/* eslint-disable import/no-cycle */
import React, { PureComponent, Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
// import { compose } from 'redux' // use connect with blogs layout; if connect & withRouter use compose
import Loader from 'components/Loader';
import Backdrop from 'components/Backdrop';
// eslint-disable-next-line import/no-cycle
import Header from './LayoutComponents/Header';
import Footer from './LayoutComponents/Footer';
import BlogsLayout from './BlogsLayout';

// import WOW from 'wowjs'

const Layouts = {
  blogs: BlogsLayout,
};

class Index extends PureComponent {
  // shouldComponentUpdate(prevProps) {
  //   console.log('in componentShouldUpdate')
  //   console.log(prevProps)
  //   return false;
  // }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    const { prevLocation } = prevProps;
    if (location !== prevLocation) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const {
      children,
      location: { pathname },
      settings,
    } = this.props;
    console.log(pathname);

    console.log(this.props);

    const { backdrop } = settings;

    const getLayout = () => {
      if (/^\/blogs(?=\/|$)/i.test(pathname)) {
        return 'blogs';
      }
      return '';
    };
    console.log(getLayout());

    const Container = Layouts[getLayout()];
    console.log(Container);

    let ContentLayout;
    if (getLayout() !== '') {
      ContentLayout = <Container>{children}</Container>;
    } else ContentLayout = <>{children}</>;

    console.log(children);

    return (
      <>
        <Helmet titleTemplate="Zapkart" title="Zapkart" />
        <Backdrop active={backdrop} />
        <Header />
        <Suspense fallback={<Loader />}>{ContentLayout}</Suspense>
        <Footer />
      </>
    );
  }
}

export default withRouter(connect(({ settings }) => ({ settings }))(Index));
