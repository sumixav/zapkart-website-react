import React, { Component } from 'react'
import PageSection from 'components/PageSection'
import { getCategories } from 'services/category';
import { getBlogsList } from 'services/blogs';
import RecentBlog from 'pages/blog/recentBlog';
import { withRouter } from 'react-router'

class Blogs extends Component {

  state = {
    categories: [],
    blogDetails: [],
  };
  
  componentDidMount() {
    this.fetchList();
  }

  fetchList = async () => {
    const categories = await getCategories({ fields: ['name', 'slug'], parent:null });
    const blogs = await getBlogsList({ fields: ['name', 'slug']});
    
    if (categories)
      this.setState({
        categories,
      });
      if (blogs)
      this.setState({
        blogDetails:blogs
      });
  };
  
  render() {
    console.log("hhhhh",this.props);
    const { children, match } = this.props
    console.log(match);
    const {
      blogDetails
    }= this.state;
    
    return (
      <PageSection className="blogs-wrapper">
        <div className="container">
          <div className="row">
            {children}
            <div className="col-xl-2 col-lg-3">
              {/* Accordion wrapper */}
              <div
                className="accordion md-accordion right-side-wrapper"
                id="accordionEx"
                role="tablist"
                aria-multiselectable="true"
              >
                <h5 className="filter-sec-head">Categories</h5>

                {/*  Accordion card  */}
                <div className="card">
                  {/*  Card header  */}
                  <div className="card-header" role="tab" id="headingOne1">
                    <a
                      data-toggle="collapse"
                      data-parent="#accordionEx"
                      href="#collapseOne1"
                      aria-expanded="true"
                      aria-controls="collapseOne1"
                    >
                      <h5 className="mb-0">
                        Health (9) <i className="fas fa-angle-down rotate-icon" />
                      </h5>
                    </a>
                  </div>

                  {/*  Card body  */}
                  <div
                    id="collapseOne1"
                    className="collapse show"
                    role="tabpanel"
                    aria-labelledby="headingOne1"
                    data-parent="#accordionEx"
                  >
                    <div className="card-body">
                      <div className="filter-list">
                        <a href="/#" >Live well (7)</a>
                        <a href="/#" >Personal Beauty (2)</a>
                      </div>
                    </div>
                  </div>
                </div>
                {/*  Accordion card  */}
              </div>
              <div className="right-side-wrapper">
    <h5 className="filter-sec-head">Recent Posts</h5>
              {
                blogDetails.map(blog => {
                  return blog.slug !== '' ?
                  <RecentBlog
                    name={blog.name}
                    slug={blog.slug}
                    
                  >
                  </RecentBlog>:''
                })
              }
      </div>
              

              <div className="right-side-wrapper">
                <h5 className="filter-sec-head">Archives</h5>
                <a href="/#"  className="right-list">
                  May 2019
                </a>
                <a href="/#"  className="right-list">
                  March 2019
                </a>
                <a href="/#"  className="right-list">
                  February 2019
                </a>
                <a href="/#"  className="right-list">
                  November 2018
                </a>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    )
  }
}

export default withRouter(Blogs)
