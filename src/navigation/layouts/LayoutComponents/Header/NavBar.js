import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './NavBar.css';
// import {navBarActions} from 'redux/actions'

class NavBar extends Component {
  render() {
    const { navbar } = this.props;
    console.log(navbar);
    return (
      <nav>
        <ul className="menu">
          {navbar.navBarData.map(item => {
            console.log(item.key);
            return (
              <li key={item.key}>
                <Link
                  key={item.key}
                  to={{
                    pathname: item.url,
                    state: {
                      title: item.title,
                    },
                  }}
                >
                  {item.title}
                </Link>
              </li>
            );
          })}
        </ul>
      </nav>
    );
  }
}

export default connect(({ navbar }) => ({ navbar }))(NavBar);
