import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './styles.css';

class ProfileMenu extends Component {
  render() {
    const { img, menu } = this.props;
    const { profileMenuData } = menu;
    const { dispatch } = this.props;

    // const profileMenuFiltered = profileMenuData.filter(item => item.url !== '/logout')

    // return (
    //   <div className="profile-menu">
    //     {profileMenuData.map(item => (
    //       <div className="menu-grid">
    //         <div className="menuitem__icon">
    //           <i className={item.icon} />
    //         </div>
    //         <div className="menuitem__title">{item.title}</div>
    //       </div>
    //     ))}
    //   </div>
    // );

    return (
      // <div className="btn-group">
      <div className="dropdown">
        <button
          type="button"
          className="btn primary-bg-color login-btn"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <img src={img} alt="" className="lazy top-icons" />
        </button>
        <div className="dropdown-menu">
          {profileMenuData.map(item => {
            if (item.divider) return <div key={Math.random()} className="dropdown-divider" />;
            if (item.url) {
              return (
                <Link to={item.url}>
                  {' '}
                  <div role="presentation" className="menu-grid" aria-hidden="true">
                    <div className="menuitem__icon">
                      <i className={item.icon} />
                    </div>
                    <div className="menuitem__title">{item.title}</div>
                  </div>
                </Link>
              );
            }
            if (item.action) {
              return (
                <Link to={item.url}>
                  {' '}
                  <div
                    role="presentation"
                    onClick={() => dispatch({ type: item.action })}
                    className="menu-grid"
                    aria-hidden="true"
                  >
                    <div className="menuitem__icon">
                      <i className={item.icon} />
                    </div>
                    <div className="menuitem__title">{item.title}</div>
                  </div>
                </Link>
              );
            }
            return '';
            // console.log(item.key);
            // return (
            //   <Link key={item.key} className="dropdown-item" to={`${item.url}`}>
            //     {item.icon && (
            //       <span key={item.key}>
            //         <i key={item.key} className={item.icon} aria-hidden="true" />
            //       </span>
            //     )}{' '}
            //     <span>{item.title}</span>
            //   </Link>
            // );
          })}
        </div>
      </div>
    );
  }
}

export default connect(({ menu }) => ({ menu }))(ProfileMenu);
