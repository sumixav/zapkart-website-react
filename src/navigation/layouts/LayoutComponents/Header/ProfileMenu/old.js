import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class ProfileMenu extends Component {
  render() {
    const { img, menu } = this.props
    const { profileMenuData } = menu
    const { dispatch } = this.props

    // const profileMenuFiltered = profileMenuData.filter(item => item.url !== '/logout')

    return (
      <div className="btn-group">
        <button
          type="button"
          className="btn primary-bg-color login-btn"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <img src={img} alt="" className="lazy top-icons" />
        </button>
        <div className="dropdown-menu">
          {profileMenuData.map(item => {
            if (item.divider) return <div key={Math.random()} className="dropdown-divider" />
            if (item.action) {
              console.log(item.key)
              return (
                <button
                  className="dropdown-item"
                  key={item.key}
                  type="button"
                  onClick={() => dispatch({ type: item.action })}
                >
                  <span key={item.key}>
                    <i key={item.key} className={item.icon} aria-hidden="true" />
                  </span>
                  <span>{item.title}</span>
                </button>
              )
            }
            console.log(item.key)
            return (
              <Link key={item.key} className="dropdown-item" to={`${item.url}`}>
                {item.icon && (
                  <span key={item.key}>
                    <i key={item.key} className={item.icon} aria-hidden="true" />
                  </span>
                )}{' '}
                <span>{item.title}</span>
              </Link>
            )
          })}
        </div>
      </div>
    )
  }
}

export default connect(({ menu }) => ({ menu }))(ProfileMenu)
