import React, { useState, useRef, useEffect } from 'react';
import { generateCategoryTree } from '_utils';
import classnames from 'classnames';
import styled from 'styled-components';
import { useCategories } from 'hooks/api';
import { Link } from 'react-router-dom';
import findIndex from 'lodash/findIndex';

const StyledDiv = styled.div`
  margin-bottom: 20px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-auto-flow: columns;
  &:last-child {
    margin-bottom: 0px;
  }
  &:grid-column(1) {
    background-color: red;
  }
  /* column-count: 4; 
  @media (max-width: 991px) {
    column-count: 2;
  } */
`;

const NavBar = ({ hasSideMenu }) => {
  const [nodes, setNodes] = useState([]);
  const colItemsCount = useRef();
  colItemsCount.current = 0;

  const handleCaret = e => {
    const { dataset } = e.currentTarget;
    const { nodeId: selectedNodeId } = dataset;
    console.log(selectedNodeId);
    const index = findIndex(nodes, i => i.id === selectedNodeId);
    console.log('index', index);
    if (index >= 0) nodes[findIndex].isOpen = !(nodes[findIndex].isOpen && true);

    setNodes(nodes);
  };

  const { data } = useCategories();

  useEffect(() => {
    if (data && data.data) setNodes(generateCategoryTree(data.data));
  }, [data]);

  return (
    <nav className={classnames({ 'has-side-menu': hasSideMenu })}>
      {/** container not necessary */}
      <div className="container">
        <ul className="menu-main">
          {nodes
            .filter(i => i.parent === 0)
            .map(i => {
              return (
                <li>
                  <Link className="main-heading" to={`/products/category/${i.id}`}>
                    {i.title}
                  </Link>
                  <span
                    data-nodeId={i.id}
                    className="toggle-caret"
                    tabIndex={0}
                    role="button"
                    onKeyDown={handleCaret}
                    onClick={handleCaret}
                  >
                    {i.isOpen && <i className="fas fa-chevron-up" />}
                    {!i.isOpen && <i className="fas fa-chevron-down" />}
                  </span>
                  {i.children && (
                    <div className={classnames('menu-sub', { active: i.isOpen })}>
                      {i.children.map(sub1 => {
                        return (
                          <StyledDiv>
                            <div>
                              <Link to={`/products/category/${sub1.id}`}>
                                <h3 className="category-heading">{sub1.title}</h3>
                              </Link>
                              {sub1.children && (
                                <ul>
                                  {sub1.children.map(sub2 => (
                                    <ul>
                                      <li>
                                        <Link to={`/products/category/${sub2.id}`}>
                                          {sub2.title}
                                        </Link>
                                      </li>
                                    </ul>
                                  ))}
                                </ul>
                              )}
                            </div>
                          </StyledDiv>
                        );
                      })}
                    </div>
                  )}
                </li>
              );
            })}
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
