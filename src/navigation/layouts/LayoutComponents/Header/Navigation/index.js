import React, { useState } from 'react';
import classNames from 'classnames';
import NavBar from './NavBar';
import PrescriptionUpload from './PrescriptionUpload';


const Navigation = () => {
  const [isSideMenu, toggleSideMenu] = useState(false);

  const handleSideMenu = () => toggleSideMenu(prev => !prev);
  return (
    <>
      <div className="nav-wrapper primary-bg-color wow fadeInUp">
        <div className="container">
          <div className="nav-container">
            <div className="nav-area">
              <span
                className={classNames('toggle-main', { active: isSideMenu })}
                // className="toggle-main"
                role="button"
                tabIndex={0}
                onKeyDown={handleSideMenu}
                onClick={handleSideMenu}
              >
                {/* role="presentation"
               className={`toggle-main ${isToggleMenu ? 'active' : ''}`}
               onClick={this.setToggleMenu} */}

                {!isSideMenu && <i className="fas fa-bars ease1s" />}
                {isSideMenu && <i className="fas fa-times ease1s" />}
              </span>

              <NavBar hasSideMenu={isSideMenu} />
            </div>
            <PrescriptionUpload />
          </div>
        </div>
      </div>
    </>
  );
};

export default Navigation;
