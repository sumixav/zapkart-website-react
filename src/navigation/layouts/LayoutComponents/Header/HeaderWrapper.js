import React, { Component } from 'react'
import BreadCrumbs from 'components/BreadCrumbs'
import { withRouter } from 'react-router-dom'
// eslint-disable-next-line import/no-cycle
import { getHeaderState } from 'navigation/router'
import { Spring } from 'react-spring/renderprops'

const springFrom = { marginTop: '20px', opacity: 0 };
const springTo = { marginTop: '0px', opacity: 1 }
const springConfig = { delay: 500 };

// headerState: {
//   title: 'Login',
//   bread: [
//     {
//       url: '/home',
//       text: 'Home',
//     },
//     {
//       text: 'Login',
//     },
//   ],
// },
class HeaderWrapper extends Component {
  state = {
    headerState: null,
  }

  componentDidMount() {
    const { location, headerState : headerStateFromProps } = this.props
    const { pathname } = location
    console.log(this.props)
    const headerState = headerStateFromProps || getHeaderState(pathname)
    if (headerState) {
      this.setState({
        headerState,
      })
    }
    console.log(headerState)
  }

  // shouldComponentUpdate(nextProps) {
  //   const { location } = nextProps
  //   const { pathname } = location
  //   return isRouteWithHeader(pathname)
  // }

  static getDerivedStateFromProps(props, state) {
    console.log(props, state)
    const { location, headerState : headerStateFromProps } = props
    const { pathname } = location
    return { headerState: headerStateFromProps || getHeaderState(pathname) }
  }

  render() {
    const { headerState } = this.state;

    console.log(headerState)
    if (headerState) {
      return (
        <Spring from={springFrom} to={springTo} config={springConfig}>
          {props => (
            <div style={props}>
              <div className="page-header-wrapper">
                <div className="container">
                  <div className="row">
                    <div className="col-lg-6 col-md-5">
                      <h1 className="page-heading">{headerState.title}</h1>
                    </div>
                    {headerState.bread && (
                      <div className="col-lg-6 col-md-7">
                        <BreadCrumbs bread={headerState.bread} />
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          )}
        </Spring>
      )
    }
    return null
  }
}



export default withRouter(HeaderWrapper)
