import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import ProfileMenu from './ProfileMenu'

const mapStateToProps = ({ user, menu }) => ({
  isLogged : user.isLogged,
  menu
})

class ProfileAvatar extends Component {
  render() {
    const { isLogged, menu } = this.props
    console.log(isLogged)
    console.log(menu)

    if (isLogged) return <ProfileMenu img="/resources/images/kickill-user.svg" />

    return (
      <Link to="/user/login" className="btn primary-bg-color login-btn">
        <img src="/resources/images/kickill-user.svg" alt="" className="lazy top-icons" />
      </Link>
    )
  }
}

export default connect(mapStateToProps)(ProfileAvatar)
