import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import WOW from 'wowjs'; // main
// import {WOW} from 'wowjs/dist/wow';
// import NavBar from './NavBar';
// eslint-disable-next-line import/no-cycle
import HeaderWrapper from './HeaderWrapper';
import ProfileAvatar from './ProfileAvatar';
import CartIcon from './CartIcon';
import Navigation from './Navigation';

class Header extends PureComponent {
  state = {
    isToggleMenu: false,
  };
  // componentDidMount() {
  //   new WOW.WOW({
  //     live: false,
  //   }).init()
  // }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      const wow = new WOW.WOW({
        live: false,
      });
      wow.init();
    }
  }

  // componentDidUpdate() {
  //       if (typeof window !== 'undefined') {
  //     const wow = new WOW.WOW({
  //       live: false,
  //     })
  //     wow.init()
  //   }
  // }

  setToggleMenu = () => {
    this.setState(prev => ({
      ...prev,
      isToggleMenu: !prev.isToggleMenu,
    }));
  };

  render() {
    // const { isToggleMenu } = this.state;
    return (
      <>
        <header>
          <div className="top-strip-wrapper secondary-bg-color">
            <div className="container">
              <div className="top-strip">
                <div className="logo wow fadeInDown">
                  <Link to="/home">
                    {/* <LazyLoad> */}
                    <img src="/resources/images/kickill-logo.png" alt="" />
                    {/* </LazyLoad> */}
                  </Link>
                </div>
                <div className="search-wrapper wow fadeInDown">
                  <form action="">
                    <input type="text" placeholder="Search Products..." />
                    <button type="button" className="search-btn primary-bg-color">
                      <img
                        src="/resources/images/kickill-search.svg"
                        className="top-icons"
                        alt=""
                      />
                    </button>
                  </form>
                </div>
                <div className="top-button wow fadeInDown">
                  <Link to="/" className="btn primary-bg-color location-btn">
                    <span className="loc-img">
                      {/* <LazyLoad> */}
                      <img
                        src="/resources/images/kickill-location.svg"
                        alt=""
                        className="top-icons"
                      />
                    </span>{' '}
                    <span className="loc">Ernakulam</span>
                  </Link>
                  <Link to="/" className="btn primary-bg-color truck-btn">
                    <img src="/resources/images/kickill-truck.svg" alt="" className="top-icons" />
                  </Link>
                  <ProfileAvatar />
                  <CartIcon />
                </div>
              </div>
            </div>
          </div>
          <Navigation />
          <HeaderWrapper />
        </header>
      </>
    );
  }
}

export default Header;
