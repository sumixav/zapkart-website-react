import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const FooterMenu = ({ title, list }) => {
  console.log(list);
  return (
    <div className="col-lg-3 col-md-6 col-sm-6 col-6 footer-menu-wrapper">
      <div className="footer-menu">
        <h3 className="footer-head">{title}</h3>
        <ul className="footer-list">
          {list.map(item => {
            const { footerStatus = true } = item;
            return (
              <li key={item.key}>
                {footerStatus ? (
                  <Link
                    to={{
                      pathname: item.link || '/',
                      state: {
                        title: item.title,
                      },
                    }}
                  >
                    {item.title}
                  </Link>
                ) : (
                  item.title
                )}
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

FooterMenu.propTypes = {
  title: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
};

export default FooterMenu;
