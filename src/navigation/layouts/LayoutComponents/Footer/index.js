/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getInformations } from 'services/informations';
import { getBrands } from 'services/brands';
// eslint-disable-next-line import/no-cycle
import { getCategories } from 'services/category';
import FooterMenu from './FooterMenu';
import data from './data.json';

class Footer extends Component {
  state = {
    footerMenuData: data.data,
    brands: [],
    informations: [],
    categories: [],
    quickLinks: [],
  };

  componentDidMount() {
    this.fetchList();
  }

  fetchList = async () => {
    console.log('fetching brands');
    const brands = await getBrands({ fields: ['name', 'slug'] });
    const informations = await getInformations();
    const categories = await getCategories({ fields: ['name', 'slug'], parent: null });
    console.log(brands);
    if (brands)
      this.setState({
        brands,
      });
    if (informations)
      this.setState({
        informations,
      });
    if (categories)
      this.setState({
        categories,
      });
  };

  render() {
    let { footerMenuData, brands, informations, quickLinks, categories } = this.state;

    brands = brands.map(i => ({ title: i.name, link: `/products/brand/${i._id}`, key: i.slug }));
    informations = informations.map(i => ({
      title: i.name,
      link: `/informations/${i.slug}`,
      key: i.slug,
      footerStatus: i.footerStatus,
    }));
    categories = categories.map(i => ({
      title: i.name,
      link: `/products/category/${i._id}`,
      key: i.slug,
    }));

    quickLinks = [
      { title: 'Blogs', link: '/blogs', key: 'blogs' },
      { title: 'Contact Us', key: 'contact-us', link: '/contact-us' },
    ];
    console.log(brands);
    footerMenuData = [
      { title: 'Quick Links', key: 'quick-links', children: quickLinks },
      { title: 'Informations', key: 'informations', children: informations },
      // ...footerMenuData,
      { title: 'Categories', key: 'categories', children: categories },
      { title: 'Brands', key: 'brands', children: brands },
    ];

    const footerMenu = footerMenuData.map(item => (
      <FooterMenu key={item.key || item._id} title={item.title} list={item.children} />
    ));

    return (
      <footer>
        <div className="container">
          <div className="footer-wrapper">
            <div className="footer-menu-list">
              <div className="row">{footerMenu}</div>
            </div>
            <div className="newsletter-wrapper">
              {/* <!-- BOF Newsletter Html --> */}
              <div className="newsletter-area">
                <h6 className="head-two-boders">
                  <span>Sign up for our Newsletter</span>
                </h6>
                <p>
                  Get all the best deals, sales and offers from the best online shopping store. Sign
                  up now !
                </p>
                <div className="newsletter-input-wrapper">
                  <form action="">
                    <input
                      type="text"
                      className="newsletter-input"
                      placeholder="Enter Your Email..."
                    />
                    <button type="button" className="newsletter-btn">
                      Subscribe
                    </button>
                  </form>
                </div>
              </div>
              {/* <!-- EOF Newsletter Html --> */}

              {/* <!-- BOF Footer Social Media --> */}
              <div className="footer-social-wrapper">
                <h6 className="head-two-boders">
                  <span className="fs14">Sign up for our Newsletter</span>
                </h6>
                <div className="footer-social">
                  <Link to="/" target="_blank">
                    <i className="fab fa-instagram" />
                  </Link>
                  <Link to="/" target="_blank">
                    <i className="fab fa-facebook-f" />
                  </Link>
                  <Link to="/" target="_blank">
                    <i className="fab fa-twitter" />
                  </Link>
                  <Link to="/" target="_blank">
                    <i className="fab fa-pinterest-p" />
                  </Link>
                </div>
              </div>
              {/* <!-- EOF Footer Social Media --> */}

              <div className="download-app-wrapper">
                <h6 className="head-two-boders">
                  <span className="fs14">Sign up for our Newsletter</span>
                </h6>
                <div className="download-app">
                  <Link to="/" target="_blank">
                    <img
                      // loading="lazy"
                      className="lazy"
                      src="/resources/images/kickill-android-app.png"
                      alt=""
                    />
                  </Link>
                  <Link to="/" target="_blank">
                    <img className="lazy" src="/resources/images/kickill-ios-app.png" alt="" />
                  </Link>
                </div>
              </div>
            </div>
            <div className="footer-about-wrapper">
              <div className="footer-about">
                <h5 className="footer-head fs14">
                  Zapkart: Purchase Medicines & Drugs Online in India
                </h5>
                <p>
                  Zapkart is the easiest way to shop prescription medicines, Allopathic medicines,
                  Homeopathic pills, Ayurvedic medicines and health products. Zapkart app helps you
                  to find the right medicine. Now, search from a wide range of prescription
                  medicines, Allopathic medicines, Homeopathic pills, Ayurvedic medicines and health
                  products.
                </p>
                <p>
                  Get quick and seamless delivery of your orders as well as easy returns and refunds
                  only at Zapkart shopping app.
                </p>
              </div>

              <div className="payment-wrappper">
                <h5 className="footer-head fs14">Payment Methods</h5>
                <div className="payment-list">
                  <img
                    className="lazy"
                    src="/resources/images/payments/kickill-payment-1.png"
                    alt=""
                  />
                  <img
                    className="lazy"
                    src="/resources/images/payments/kickill-payment-2.png"
                    alt=""
                  />
                  <img
                    className="lazy"
                    src="/resources/images/payments/kickill-payment-3.png"
                    alt=""
                  />
                  <img
                    className="lazy"
                    src="/resources/images/payments/kickill-payment-4.png"
                    alt=""
                  />
                  <img
                    className="lazy"
                    src="/resources/images/payments/kickill-payment-5.png"
                    alt=""
                  />
                  <img
                    className="lazy"
                    src="/resources/images/payments/kickill-payment-6.png"
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="copyright-wrapper">
          <div className="container">
            <div className="copy-content">
              <div className="copyright">© 2019 ZAPKART. ALL RIGHTS RESERVED.</div>
              <div className="copy-links">
                <Link to="/">PRIVACY POLICY</Link> |<Link to="/">Terms and Conditions</Link>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
