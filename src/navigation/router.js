import React, { Suspense } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Loader from 'components/Loader';
import { ConnectedRouter } from 'connected-react-router';
import IndexLayout from './layouts';

const loadable = loader => React.lazy(loader);

// const NotFoundPage = () => {
//   console.log('in not found')
//   return 'Not found'
// }

// const Loader = () => {
//   console.log('in loader')
//   return 'Loading'
// }

export const routes = [
  {
    path: '/products',
    component: loadable(() => import('modules/product/list')),
    exact: true,
  },
  {
    path: '/products/:type/:id',
    component: loadable(() => import('modules/product/list')),
    exact: true,
  },
  {
    path: '/products/:type',
    component: loadable(() => import('modules/product/list')),
    exact: true,
  },
  {
    path: '/product/:slug',
    component: loadable(() => import('modules/product/details')),
    exact: true,
  },
  {
    path: '/home',
    component: loadable(() => import('modules/home')),
    exact: true,
  },
  {
    path: '/user/login',
    component: loadable(() => import('modules/user/login')),
    exact: true,
    headerState: {
      title: 'Login',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Login',
        },
      ],
    },
  },
  {
    path: '/user/register',
    component: loadable(() => import('modules/user/register')),
    exact: true,
    headerState: {
      title: 'Register',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Register',
        },
      ],
    },
  },
  {
    path: '/user/forgot-password',
    component: loadable(() => import('modules/user/forgot-password')),
    exact: true,
    headerState: {
      title: 'Forgot Password',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Forgot Password',
        },
      ],
    },
  },
  {
    path: '/user/user-profile',
    component: loadable(() => import('modules/user/profile')),
    exact: true,
    headerState: {
      title: 'User Profile',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'User Profile',
        },
      ],
    },
  },
  {
    path: '/cart',
    component: loadable(() => import('modules/cart')),
    exact: true,
    headerState: {
      title: 'Cart',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Cart',
        },
      ],
    },
  },
  {
    path: '/contact-us',
    component: loadable(() => import('modules/contact-us')),
    exact: true,
    headerState: {
      title: 'Contact Us',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Contact Us',
        },
      ],
    },
  },
  {
    path: '/blogs',
    component: loadable(() => import('modules/blogs/list')),
    exact: true,
    headerState: {
      title: 'Blogs',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Blogs',
        },
      ],
    },
  },
  {
    path: '/blogs/:id',
    component: loadable(() => import('modules/blogs/details')),
    exact: true,
    headerState: {
      title: 'Blogs',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Blogs',
          url: '/blogs',
        },
        {
          text: 'Blog details',
        },
      ],
    },
  },

  {
    path: '/blogs/list/all',
    component: loadable(() => import('modules/blogs/list')),
    exact: true,
    headerState: {
      title: 'Blogs',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'blogs',
        },
      ],
    },
  },
  {
    path: '/checkout',
    component: loadable(() => import('modules/user/checkout')),
    exact: true,
    headerState: {
      title: 'Checkout',
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: 'Checkout',
        },
      ],
    },
  },

  {
    path: '/informations/:slug',
    component: loadable(() => import('modules/informations')),
    exact: true,
  },
];

export const getHeaderState = url => {
  console.log('in getHeaderState');
  const route = routes.find(item => item.path === url);
  console.log(route);
  if (route && route.headerState) {
    console.log(route.headerState);
    return route.headerState;
  }
  return null;
};

class RouterJ extends React.Component {
  render() {
    const { history } = this.props;
    return (
      <Suspense fallback={<Loader />}>
        <ConnectedRouter history={history}>
          <IndexLayout>
            <Switch>
              <Route exact path="/" render={() => <Redirect to="/home" />} />
              {routes.map(route => {
                return (
                  <Route
                    path={route.path}
                    component={route.component}
                    key={route.path}
                    exact={route.exact}
                  />
                );
              })}
              <Route component={() => <div>Not Found</div>} />
            </Switch>
          </IndexLayout>
        </ConnectedRouter>
      </Suspense>
    );
  }
}

export default RouterJ;
