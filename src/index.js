import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { logger } from 'redux-logger'
import { routerMiddleware } from 'connected-react-router'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import {
  // createHashHistory,
  createBrowserHistory,
} from 'history'
// import createHistory from 'history/createBrowserHistory'
import reducers from 'redux/reducers'
import sagas from 'redux/sagas'
import Router from 'navigation/router'
import './global.scss'
import * as serviceWorker from './serviceWorker'

// const history = createHashHistory()
const history = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()
const routeMiddleware = routerMiddleware(history)
const middlewares = [sagaMiddleware, routeMiddleware]

if (process.env.NODE_ENV === 'development' && true) {
  middlewares.push(logger)
}

const store = createStore(reducers(history), compose(applyMiddleware(...middlewares)))
sagaMiddleware.run(sagas)

ReactDOM.render(
  <Provider store={store}>
    <Router history={history} />
  </Provider>,
  document.getElementById('root'),
)

serviceWorker.register()
export { store, history }


