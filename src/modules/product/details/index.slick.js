import React, { PureComponent } from 'react';
import PageSection from 'components/PageSection';
import * as Button from 'components/Button';
import Slider from 'react-slick';
import Tabs, { TabsPanel } from 'components/TabsOld';
import ProductDetail from '../components/ProductDetail';
import ProductPricing from '../components/ProductPricing';
import SimilarProducts from '../components/SimilarProducts';
import AddReview from '../components/AddReview';
import Review from '../components/Review';
import FrequentlyBought from '../components/FrequentlyBought';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import data from './data.json';
import './styles.css';

const { reviews } = data;

const { AddButton } = Button;

const freqBought = [
  {
    id: '123',
    name: 'Becadexamin Soft Gelatin Capsule',
    pricing: {
      listPrice: 3450,
      salePrice: 3000,
    },
    image: '/resources/images/products/kickill-product-6.png',
  },
  {
    id: '124',
    name: 'Becadexamin Soft Gelatin Tablet',
    pricing: {
      listPrice: 3450,
      salePrice: 2000,
    },
    image: '/resources/images/products/kickill-product-6.png',
  },
];

const tabs = [
  {
    title: 'Description',
    content: (
      <>
        <p>Healthvit Vitamin B12 2000mcg tablet contains vitamins B12 and glycine. </p>
        <p>
          Key benefits of Healthvit Vitamin B12 2000mcg tablet contains: Used to treat certain
          anaemias.{' '}
        </p>
        <p>Vitamin B12 plays an important role in helping the body make red blood cells. </p>
      </>
    ),
  },
  {
    title: 'Key Benefits',
    content: 'key benefits',
  },
  {
    title: 'Directions for use/dosage',
    content: (
      <>
        <p>
          As a dietary supplement for adults, take 1 or 2 tablets daily preferably at mealtime or as
          directed by health practitioner.
        </p>
      </>
    ),
  },
  {
    title: 'Safety Informations/precaution',
    content: ' safety info',
  },
  {
    title: 'Other Informations',
    content: (
      <>
        <p>Not returnable Read policy</p>
        <p>Frequent searches leading to this page</p>
        <p>
          HealthVit Vitamin B12 2000mcg Tablet Price, Buy HealthVit Vitamin B12 2000mcg Tablet
          Online,
        </p>
      </>
    ),
  },
  {
    title: 'Reviews',
    content: reviews.map(item => <Review id={item.id} key={item.id} review={item} />),
  },
];

class ProductDetailPage extends PureComponent {
  state = {
    images: [
      { id: '1', image: '/resources/images/kickill-product-detail-image.jpg' },
      { id: '2', image: '/resources/images/kickill-product-detail-image.jpg' },
      { id: '3', image: '/resources/images/kickill-product-detail-image.jpg' },
      { id: '4', image: '/resources/images/kickill-product-detail-image.jpg' },
      { id: '5', image: '/resources/images/kickill-product-detail-image.jpg' },
      { id: '6', image: '/resources/images/kickill-product-detail-image.jpg' },
    ],

    selectedQty: 0,
  };

  getData = () => {
    console.log('hi');
  };

  componentDidMount = () => {
    this.getData();
  };

  // static getDerivedStateFromProps(prevProps) {
  //   const { product } = prevProps
  //   console.log(product)
  //   const { minOrderQty, maxOrderQty } = product.product
  //   const qtys = []
  //   let i = minOrderQty
  //   qtys.push(i)
  //   i += 1
  //   while (i <= maxOrderQty) {
  //     qtys.push(i)
  //     i += 1
  //   }
  //   return {
  //     quantities: qtys,
  //     selectedQty: minOrderQty,
  //   }
  // }

  handleBuyProduct = () => {
    console.log('in buy products');
  };

  // componentDidUpdate = (prevProps, prevState) =>{
  //   console.log(prevProps, this.props)
  //   console.log('prevState', prevState)
  //   const {product} = this.props
  //   console.log(product)

  // }

  handleQtyChange = e => {
    this.setState({
      selectedQty: e.target.value,
    });
  };

  handleBuyFrequently = () => {
    console.log('clicked to buy frequently bought');
  };

  render() {
    const { selectedQty, images } = this.state;
    console.log(this.props);

    // console.log('product', product);
    // console.log(product.images);
    let index = 0;

    // const { images } = product;
    let imagesa = [];
    if (images) {
      imagesa = images.map(item => {
        const a = { id: index, image: item.url, thumb: item.thumbUrl };
        index += 1;
        return a;
      });
    }
    console.log('imagesa', imagesa);
    const quantities = [];
    let minOrderQty = 4;
    let maxOrderQty = 6;
    if (minOrderQty && maxOrderQty) {
      while (minOrderQty <= maxOrderQty) {
        quantities.push(minOrderQty);
        minOrderQty += 1;
      }
    }
    console.log(quantities);

    const carouselSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
    };
    return (
      <>
        <PageSection className="blogs-wrapper">
          <div className="container">
            <div className="products-detail-3">
              <Slider {...carouselSettings}>
                {images.map(i => (
                  <div>
                    <img style={{ width: '100%', height: '100%' }} src={i.image} alt="abc" />
                  </div>
                ))}
              </Slider>

              {/* <div className="products-details-wrapper"> */}

              <FrequentlyBought products={freqBought} onBuyFrequently={this.handleBuyFrequently} />

              {/* </div> */}
              {/* <div className="four">
                {/* <div className="similar-products-offers">
                <div className="breadcrumbs-wrapper">
                  <ul>
                    <li>
                      <a href="/#"">Home</a>
                    </li>
                    <li>/</li>
                    <li>
                      <a href="/#"">Products</a>
                    </li>
                    <li>/</li>
                    <li>Product Details</li>
                  </ul>
                </div>
              </div> */}

              <div className="additional-offers-wrapper">
                <span className="additional-offers-head">Additional offers</span>
                <p>
                  Freecharge: 25% Cashback up to ₹100 | Valid once per user from 18 Sep to 30 Apr
                </p>
                <p>Mobikwik: 20% SuperCash up to ₹250 | Valid twice per user from 1 to 30 Apr</p>
              </div>

              {/* </div> */}
            </div>
            <div className="tabs-reviews-wrapper">
              <Tabs>
                {tabs.map(tabContent => (
                  <TabsPanel id={tabContent.title} label={tabContent.title}>
                    {tabContent.content}
                  </TabsPanel>
                ))}
              </Tabs>
              <AddReview />
            </div>
          </div>
        </PageSection>
      </>
    );
  }
}

export default ProductDetailPage;
