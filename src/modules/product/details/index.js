/* eslint-disable no-underscore-dangle */
import React, { PureComponent } from 'react';

import PageSection from 'components/PageSection';
import { AddButton } from 'components/Button';
// import Slider from 'react-slick';
import { connect } from 'react-redux';
import { cartActions } from 'redux/actions';
import _ from 'lodash';
import OwlCarousel from 'react-owl-carousel';
import { Link } from 'react-router-dom';
import toaster from 'toasted-notes';
import { STRINGS } from '_constants';
import Tabs, { TabsPanel } from 'components/TabsOld';
import BreadCrumbs from 'components/BreadCrumbs';
import Toaster from 'components/Toaster';
import Loader from 'components/Loader';
import { getProduct } from 'services/products';
import { createMarkup } from '_utils';
import ProductDetail from '../components/ProductDetail';
import ProductPricing from '../components/ProductPricing';
import SimilarProducts from '../components/SimilarProducts';
import AddReview from '../components/AddReview';
import Review from '../components/Review';
import FrequentlyBought from '../components/FrequentlyBought';
import data from './data.json';
import './styles.scss';
import VariantList from '../components/VariantList';

const carImgStyle = {
  width: '100%',
};
const checkIconStyle = {
  color: 'rgb(57, 141, 159)',
};
// const carouselSettings = {
//   dots: true,
//   infinite: true,
//   speed: 500,
//   slidesToShow: 1,
//   slidesToScroll: 1,
// };
// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css';

const { reviews } = data;

const freqBought = [
  {
    id: '123',
    name: 'Becadexamin Soft Gelatin Capsule',
    pricing: {
      listPrice: 3450,
      salePrice: 3000,
    },
    image: '/resources/images/products/kickill-product-6.png',
  },
  {
    id: '124',
    name: 'Becadexamin Soft Gelatin Tablet',
    pricing: {
      listPrice: 3450,
      salePrice: 2000,
    },
    image: '/resources/images/products/kickill-product-6.png',
  },
];

// const tabs = [
//   {
//     title: 'Description',
//     content: (
//       <>
//         <p>Healthvit Vitamin B12 2000mcg tablet contains vitamins B12 and glycine. </p>
//         <p>
//           Key benefits of Healthvit Vitamin B12 2000mcg tablet contains: Used to treat certain
//           anaemias.{' '}
//         </p>
//         <p>Vitamin B12 plays an important role in helping the body make red blood cells. </p>
//       </>
//     ),
//   },
//   {
//     title: 'Key Benefits',
//     content: 'key benefits',
//   },
//   {
//     title: 'Directions for use/dosage',
//     content: (
//       <>
//         <p>
//           As a dietary supplement for adults, take 1 or 2 tablets daily preferably at mealtime or as
//           directed by health practitioner.
//         </p>
//       </>
//     ),
//   },
//   {
//     title: 'Safety Informations/precaution',
//     content: ' safety info',
//   },
//   {
//     title: 'Other Informations',
//     content: (
//       <>
//         <p>Not returnable Read policy</p>
//         <p>Frequent searches leading to this page</p>
//         <p>
//           HealthVit Vitamin B12 2000mcg Tablet Price, Buy HealthVit Vitamin B12 2000mcg Tablet
//           Online,
//         </p>
//       </>
//     ),
//   },
//   {
//     title: 'Reviews',
//     content: reviews.map(item => <Review id={item.id} key={item.id} review={item} />),
//   },
// ];

class ProductDetailPage extends PureComponent {
  state = {
    product: null,
    loading: false,
    goToCart: false,
    // images: [
    //   { id: '1', image: '/resources/images/kickill-product-detail-image.jpg' },
    //   { id: '2', image: '/resources/images/kickill-product-detail-image.jpg' },
    //   { id: '3', image: '/resources/images/kickill-product-detail-image.jpg' },
    //   { id: '4', image: '/resources/images/kickill-product-detail-image.jpg' },
    //   { id: '5', image: '/resources/images/kickill-product-detail-image.jpg' },
    //   { id: '6', image: '/resources/images/kickill-product-detail-image.jpg' },
    // ],

    selectedQty: 0,
  };

  componentDidMount = () => {
    const { match } = this.props;
    const { params } = match;
    const { slug } = params;
    this.fetchProduct(slug);
  };

  componentDidUpdate = prevProps => {
    const { slug: prevSlug } = prevProps.match.params;
    const { match } = this.props;
    const { slug: currentSlug } = match.params;

    if (prevSlug !== currentSlug) {
      this.fetchProduct(currentSlug);
    }
  };

  fetchProduct = async slug => {
    this.setState({ loading: true });
    const dataProd = await getProduct(slug);
    console.log(dataProd);
    this.setState({ loading: false });
    if (data) return this.setState({ product: dataProd, selectedQty: dataProd.minOrderQty });
    const { history } = this.props;
    history.go(-1);
    console.log('Product not available');
    return '';
  };

  // static getDerivedStateFromProps(prevProps) {
  //   const { product } = prevProps
  //   console.log(product)
  //   const { minOrderQty, maxOrderQty } = product.product
  //   const qtys = []
  //   let i = minOrderQty
  //   qtys.push(i)
  //   i += 1
  //   while (i <= maxOrderQty) {
  //     qtys.push(i)
  //     i += 1
  //   }
  //   return {
  //     quantities: qtys,
  //     selectedQty: minOrderQty,
  //   }
  // }

  handleBuyProduct = () => {
    const { dispatch } = this.props;
    const { product, selectedQty } = this.state;
    const { pricing, images, textDescription, name, maxOrderQty } = product;
    const { listPrice, salePrice } = pricing;
    console.log('will dispatch', product);
    const updatedQty = selectedQty > maxOrderQty ? maxOrderQty : selectedQty;

    console.log(listPrice, salePrice, selectedQty, name, textDescription);

    dispatch(
      {
        type: cartActions.ADD_TO_CART,
        payload: {
          productId: product._id,
          qty: updatedQty,
          listPrice,
          salePrice,
          title: name,
          description: textDescription,
          img: images[0].url,
          thumbImg: images[0].thumbnail,
          // , title, description, img, thumbImg, listPrice, salePrice
        },
      },
      // toaster.notify(STRINGS.added_to_cart, { position: 'top-right' }),
      toaster.notify(
        ({ onClose }) => (
          <>
            <Toaster
              onClose={onClose}
              text={STRINGS.added_to_cart}
              icon={<i style={checkIconStyle} className="fas fa-check-circle" />}
            />
          </>
        ),
        {
          position: 'top-right',
          // duration: null,
        },
      ),
      this.setState({
        goToCart: true,
      }),
    );
  };

  // componentDidUpdate = (prevProps, prevState) =>{
  //   console.log(prevProps, this.props)
  //   console.log('prevState', prevState)
  //   const {product} = this.props
  //   console.log(product)

  // }

  handleQtyChange = e => {
    this.setState({
      selectedQty: e.target.value,
    });
  };

  handleBuyFrequently = () => {
    console.log('clicked to buy frequently bought');
  };

  render() {
    const { selectedQty, product, loading, goToCart } = this.state;
    console.log(this.props);

    if (loading) return <Loader />;

    if (!product) return null;

    const { images, attributes, productExtraInfo } = product;
    //     keyBenefits
    // directionsForUse
    // safetyInfo
    // otherInfo
    const tabs = [
      {
        title: 'Description',
        content: productExtraInfo && (productExtraInfo.description !== '')
          ? (
            <div className="tab-content">
              <div dangerouslySetInnerHTML={createMarkup(productExtraInfo ? productExtraInfo.description : '')} />
            </div>
          ) : null

      },
      {
        title: 'Key Benefits',
        content: productExtraInfo && (productExtraInfo.keyBenefits !== '')
        ? (
          <div className="tab-content">
            <div dangerouslySetInnerHTML={createMarkup(productExtraInfo ? productExtraInfo.keyBenefits : '')} />
          </div>
        ) : null
      },
      {
        title: 'Directions for use/dosage',
        content: productExtraInfo && (productExtraInfo.directionsForUse !== '')
        ? (
          <div className="tab-content">
            <div dangerouslySetInnerHTML={createMarkup(productExtraInfo ? productExtraInfo.directionsForUse : '')} />
          </div>
        ) : null
      },
      {
        title: 'Safety Informations/precaution',
        content: productExtraInfo && (productExtraInfo.safetyInfo !== '')
        ? (
          <div className="tab-content">
            <div dangerouslySetInnerHTML={createMarkup(productExtraInfo ? productExtraInfo.safetyInfo : '')} />
          </div>
        ) : null
      },
      {
        title: 'Other Informations',
        content: productExtraInfo && (productExtraInfo.otherInfo !== '')
        ? (
          <div className="tab-content">
            <div dangerouslySetInnerHTML={createMarkup(productExtraInfo ? productExtraInfo.otherInfo : '')} />
          </div>
        ) : null
        // content: (
        //   <>
        //     <p>Not returnable Read policy</p>
        //     <p>Frequent searches leading to this page</p>
        //     <p>
        //       HealthVit Vitamin B12 2000mcg Tablet Price, Buy HealthVit Vitamin B12 2000mcg Tablet
        //       Online,
        //     </p>
        //   </>
        // ),
      },
      {
        title: 'Reviews',
        content: reviews.map(item => <Review id={item.id} key={item.id} review={item} />),
      },
    ];

    const batchTypeAttr = _.find(attributes, a => {
      console.log(a);
      return a.attributeGroup.attribute_group_code === 'type';
    });
    const batchQtyAttr = _.find(attributes, a => {
      return a.attributeGroup.attribute_group_code === 'batch-qty';
    });

    console.log(batchTypeAttr, batchQtyAttr);
    const batchType = batchTypeAttr ? batchTypeAttr.value.value : '';
    const batchQty = batchQtyAttr ? batchQtyAttr.value.value : '';

    console.log(batchTypeAttr, batchQtyAttr);
    console.log(batchQty, product.medicineType);
    const quantities = [];
    let { minOrderQty } = product;
    const { maxOrderQty } = product;
    if (minOrderQty && maxOrderQty) {
      while (minOrderQty <= maxOrderQty) {
        quantities.push(minOrderQty);
        minOrderQty += 1;
      }
    }
    console.log(quantities);

    let bread = [];

    bread = [
      {
        url: '/home',
        text: 'Home',
      },
      {
        text: product.name,
        url: `/products/${product._id}`,
      },
    ];

    return (
      <>
        <PageSection className="blogs-wrapper">
          <div className="container">
            <div className="products-detail-3-sections">
              <OwlCarousel
                className="products-image-slider"
                center
                nav
                items={1}
                lazyLoad
                lazyContent={false}
                // className="owl-theme owl-carousel"
                margin={10}
              >
                {images.map(i => (
                  <div key={i._id}>
                    <img style={carImgStyle} src={`/${i.url}`} alt="" />
                  </div>
                ))}
              </OwlCarousel>
              {/* <Slider {...carouselSettings} className="products-image-slider">
                {images.map(i => (
                  <div key={i._id}>
                    <img style={carImgStyle} src={`/${i.url}`} alt="" />
                  </div>
                ))}
              </Slider> */}
              <div className="products-details-wrapper">
                <ProductDetail
                  title={product.name || ''}
                  brand={product.brand && product.brand.name ? product.brand.name : ''}
                  count={product.purchaseCount || 0}
                />

                {product.pricing && (
                  <ProductPricing
                    originalPrice={product.pricing.listPrice}
                    offerPrice={product.pricing.salePrice}
                  >
                    <div className="productdetail-qty-addtocart">
                      <div className="productdetail-qty-wrapper">
                        <select
                          value={selectedQty}
                          onChange={this.handleQtyChange}
                          className="form-control"
                        >
                          {quantities.map(item => (
                            <option key={item} value={item}>
                              {`${item} ${batchType}`}
                            </option>
                          ))}
                        </select>
                        {/* Of {`${product.qtyPerBatch || ''} ${product.medicineType}`} */}
                        Of {`${batchQty} ${product.medicineType.name}`}
                      </div>
                      <AddButton onClick={this.handleBuyProduct}>Add to cart</AddButton>
                      {goToCart && (
                        <Link to="/cart">
                          <AddButton>Go to Cart</AddButton>
                        </Link>
                      )}
                    </div>
                  </ProductPricing>
                )}
                <VariantList
                  productId={product._id}
                  isVariant={product.variantType === 'single' && product.variantCount > 0}
                />
                <FrequentlyBought
                  products={freqBought}
                  onBuyFrequently={this.handleBuyFrequently}
                />
              </div>

              <div className="similar-products-offers">
                {/* <div className="breadcrumbs-wrapper">
                  <ul>
                    <li>
                      <a href="/#"">Home</a>
                    </li>
                    <li>/</li>
                    <li>
                      <a href="/#"">Products</a>
                    </li>
                    <li>/</li>
                    <li>Product Details</li>
                  </ul>
                </div> */}
                <BreadCrumbs bread={bread} />

                <SimilarProducts
                  productId={product._id}
                  organics={product.organic}
                  productTitle={product.name || ''}
                />

                <div className="additional-offers-wrapper">
                  <span className="additional-offers-head">Additional offers</span>
                  <p>
                    Freecharge: 25% Cashback up to ₹100 | Valid once per user from 18 Sep to 30 Apr
                  </p>
                  <p>Mobikwik: 20% SuperCash up to ₹250 | Valid twice per user from 1 to 30 Apr</p>
                </div>
              </div>
            </div>
            <div className="tabs-reviews-wrapper">
              <Tabs>
                {tabs.filter(i => i.content !== null).map(tabContent => (
                  <TabsPanel id={tabContent.title} label={tabContent.title}>
                    {tabContent.content}
                  </TabsPanel>
                ))}
              </Tabs>
              <AddReview />
            </div>
          </div>
        </PageSection>
      </>
    );
  }
}

export default connect()(ProductDetailPage);
