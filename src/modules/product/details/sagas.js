import {
  all,
  put,
  call,
  // delay,
  takeEvery,
} from 'redux-saga/effects'
import { getProduct } from 'services/products'
import { productActions } from './actions'

export function* GET_PRODUCT({ payload }) {
  console.log('abcd')
  yield put({
    type: productActions.SET_STATE,
    payload: {
      loading: true,
    },
  })
  console.log(payload)
  console.log('in product get data')
  const product = yield call(getProduct, payload)
  if (product) {
    yield put({
      type: productActions.SET_STATE,
      payload: { product},
    })
  }
  yield put({
    type: productActions.SET_STATE,
    payload: { loading: false },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery('product/GET_PRODUCT', GET_PRODUCT),
    // GET_DATA(), // run once on app load to fetch menu data
  ])
}
