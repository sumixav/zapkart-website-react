import React, { useState } from 'react';
import PageSection from 'components/PageSection';
import useDidMountEffect from 'hooks/useDidMountEffect';
import { useProductsTest, useBrands, useCategories } from 'hooks/api';
// import ManageLoadingStates from 'components/ManageLoadingStates';
import Filter from './Filter';
import List from './List';
import FilterContext from './context';


const Index = props => {
  // const searchStr = '';
  console.log('PRODUCT LIST PROPS', props);
  const { match, location } = props;
  const { params } = match;
  const { type, id } = params;

  const { state } = location;

  let title = null;
  if (state && state.title) title = state.title;

  React.useEffect(() => {
    console.log('seems props have changed !');
  }, [props]);

  // filter products according to these selected options
  const [selectedOptions, setSelectedOptions] = useState({});

  // set initial selectedOptions on component mount
  useDidMountEffect(() => {
    let options = {};
    if (type && id) options = { [type]: id }; // category: "11"
    if (type && !id) options = { [type]: true }; // featured: true
    setSelectedOptions(options);
  }, [type, id]);

  console.log('selectedOptions', selectedOptions);

  // const { data, isLoading, isFetching } = useProducts(options);
  const { data, isLoading: isLoadingProds } = useProductsTest(selectedOptions);
  const { data: resBrands, isLoading: isLoadingBrands } = useBrands();
  const { data: resCats, isLoading: isLoadingCats } = useCategories();
  console.log(data, resBrands, resCats);

  const handleFiltersSorters = a => {
    Object.entries(a).forEach(([key, value]) => {
      console.log(key, value);
      const { brand } = value;
      const { category } = value;
      console.log(value);
      switch (key) {
        case 'filters':
          console.log(brand, category);
          if (brand) setSelectedOptions(prev => ({ ...prev, brand }));
          if (category) setSelectedOptions(prev => ({ ...prev, category }));
          break;
        default:
          break;
      }
    });
  };

  return (
    <PageSection>
      <div className="container">
        <div className="flex-wrapper products-wrapper">
          <FilterContext.Provider
            value={{
              brands: {
                data: resBrands && resBrands.data ? resBrands.data : [],
                loading: isLoadingBrands,
              },
              categories: {
                data: resCats && resCats.data ? resCats.data : [],
                loading: isLoadingCats,
              },
              onChange: handleFiltersSorters,
            }}
          >
            <Filter />

            <List
              loading={isLoadingProds}
              data={data && data.data ? data.data : []}
              type={type}
              title={title || null}
            />
          </FilterContext.Provider>
          {/* {isLoading && <div>Loading...</div>}
          {data &&
            (isFetching ? (
              <div>Refreshing...</div>
            ) : (
              <Filter data={data}>
                {filteredData => {
                  console.log(filteredData);
                  return <List data={filteredData} />;
                }}
              </Filter>
            ))}
          {!isLoading && !data && !isFetching && <div>No data available</div>} */}
        </div>
      </div>
    </PageSection>
  );
};

export default Index;
