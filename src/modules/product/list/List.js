/* eslint-disable no-underscore-dangle */
import React from 'react';
import Product from 'components/Product';
// import Loader from 'components/Loader';

const List = props => {
  const { data, title, loading } = props;
  console.log(props);

  // console.log(props);
  return (
    <div className="products-list-wrapper">
      <div className="product-top-ad-wrapper">
        <img src="/resources/images/kickill-product-ad1.jpg" alt="" />
      </div>

      {title && (
        <div className="products-head">
          <h1>
            <span>{title}</span>
          </h1>
          <div className="breadcrumbs-wrapper">
            <ul>
              <li>
                <a href="/#">Home</a>
              </li>
              <li>/</li>
              <li>Products</li>
            </ul>
          </div>
        </div>
      )}

      {loading && (
        <div className="products-page-list-wrapper">
          <Product loading />
          <Product loading />
          <Product loading />
          <Product loading />
          <Product loading />
        </div>
      )}
      {!loading && data.length > 0 && (
        <div className="products-page-list-wrapper">
          {data.map(i => {
            return (
              <Product
                key={i._id}
                title={i.name}
                id={i._id}
                slug={i.slug}
                img={i.images[0] && i.images[0].url}
                thumbImg={i.images[0] && i.images[0].thumbnail}
                price={i.pricing.listPrice}
                description={i.textDescription}
              />
            );
          })}
        </div>
      )}
      {/* BOF Single Product List */}
      {/* <div className="product-listing">
          <img
            data-src="/resources/images/products/kickill-product-7.png"
            alt=""
            className="product-image lazy"
          />
          <h5>Becadexamin Soft Gelatin Capsule</h5>
          <h6>bottle of 30 soft gelatin capsules</h6>
          <h3>
            <i className="fas fa-rupee-sign" /> 3450/-
          </h3>
          <div className="order-btn">
            <a href="/#" className="small-btn">
              Order Now
            </a>
          </div>
        </div> */}
      {/* BOF Single Product List */}

      {!loading && data.length === 0 && <div className="no-data">No data to show</div>}
    </div>
  );
};

export default List;
