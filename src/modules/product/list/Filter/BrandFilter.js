/* eslint-disable no-underscore-dangle */
import React, { useCallback } from 'react';
import MultipleCheckBox from 'components/Input/MultipleCheckbox';
import FilterContext from '../context';

const BrandFilter = () => {
  const { brands, onChange } = React.useContext(FilterContext);

  const handleBrandsChange = useCallback(
    val => {
      onChange({ filters: { brand: val } });
    },
    [onChange],
  );

  console.log(brands);
  const options = brands.data && brands.data.map(i => ({ label: i.name, value: i._id }));
  return (
    <div className="filter-sec">
      <h5 className="filter-sec-head">Brands</h5>
      <div className="filter-search">
        <input type="text" className="form-control" placeholder="Search Brands" />
        <button type="button">
          <img src="/resources/images/kickill-filter-search-icon.png" alt="" />
        </button>
      </div>

      <MultipleCheckBox options={options} name="brands" onChange={handleBrandsChange} />
    </div>
  );
};

export default BrandFilter;
