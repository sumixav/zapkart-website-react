/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import Tree from 'components/Tree';
import { generateCategoryTree } from '_utils';
import FilterContext from '../context';

class CategoryFilter extends Component {
  state = {
    cats: [],
  };

  componentDidMount() {
    const { categories } = this.context;
    this.setState({ cats: categories && categories.data ? categories.data : [] });
  }

  shouldComponentUpdate() {
    console.log('shouldComponentUpdate');
    const { categories } = this.context;
    const { cats: prevcats } = this.state;

    console.log('should component update', categories !== prevcats && prevcats.data);
    return categories !== prevcats && prevcats.data;
  }

  handleClick = e => {
    // const { id } = JSON.parse(e.target.dataset.onclickparam);
    const { onChange } = this.context;
    const { id } = e.currentTarget.dataset;
    if (id) onChange({ filters: { category: id } });
  };

  handleChange = node => {
    const { onChange } = this.context;
    const { id } = node;
    console.log('a change');
    if (id) onChange({ filters: { category: id } });
  };

  render() {
    const { categories } = this.context;

    const categoryTree = generateCategoryTree(categories.data);
    console.log('categoryTree', categoryTree);
    return (
      // <FilterContext.Consumer>
      //   {({ categories, onClick }) => {

      <div className="filter-sec">
        <h5 className="filter-sec-head">Categories</h5>
        <Tree treeData={categoryTree} onSelect={this.handleChange} />
      </div>

      //   }}
      // </FilterContext.Consumer>
    );
  }
}

CategoryFilter.contextType = FilterContext;

export default CategoryFilter;
