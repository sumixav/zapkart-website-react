import React, { useState } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import Modal from 'components/Modal'

const borderStyle = {
  background: 'none',
  border: 'none',
}

// using modal pass multiple components as head body and footer

const Review = ({ review }) => {
  const [show, setShow] = useState(false)
  const { rating, reviewText, title, dateTime,
    // onClickReview 
    } = review

  const onCloseModal = () => {
    setShow(false)
  }

  const onShowModal = () => {
    setShow(true)
  }

  return (
    <>
      <Modal show={show} onClose={onCloseModal}>
        <Modal.Head onClose={onCloseModal}>
          <div style={{display:'flex'}}>
            <span
              className={classNames('ratings-area', {
                'review-green': rating >= 4,
                'review-red': rating < 2,
                'review-yellow': rating === 3 || rating === 2,
              })}
            >
              {rating}
              <i className="fas fa-star" />
            </span>

            <h5>{title}</h5>
          </div>
        </Modal.Head>
        <Modal.Body>
          <p>{reviewText}</p>
        </Modal.Body>
        <Modal.Footer>
          <button
            type="button"
            onClick={onCloseModal}
            className="btn btn-secondary"
            data-dismiss="modal"
          >
            Close
          </button>
        </Modal.Footer>
      </Modal>
      <div
        // role="presentation"
        // onKeyPress={() => setShow(true)}
        // onClick={() => setShow(true)}
        className="single-review"
      >
        <div className="review-subject">
          <span
            className={classNames('ratings-area', {
              'review-green': rating >= 4,
              'review-red': rating < 2,
              'review-yellow': rating === 3 || rating === 2,
            })}
          >
            {rating}
            <i className="fas fa-star" />
          </span>
          <button type="button" style={borderStyle} onClick={onShowModal}>
            <h5>{title}</h5>
          </button>
        </div>
        <div className="review-content">
          <p>{reviewText.length > 50 ? `${reviewText.slice(0.5)}...` : reviewText}</p>
        </div>
        <div className="date-time-wrapper">
          <span className="date-time">{dateTime}</span>
        </div>
      </div>
    </>
  )
}

Review.propTypes = {
  rating: PropTypes.number.isRequired,
  reviewText: PropTypes.string.isRequired,
  title: PropTypes.string,
  dateTime: PropTypes.string.isRequired,
  // onClickReview: PropTypes.func.isRequired,
}

Review.defaultProps = {
  title: '',
}

export default Review
