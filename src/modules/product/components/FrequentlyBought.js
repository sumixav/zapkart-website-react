import React from 'react';
import Product from 'components/Product';

const FrequentlyBought = ({ products, onBuyFrequently }) => {
  const totalPrice = products.reduce((prevValue, item) => {
    return Number(item.pricing.salePrice) + prevValue;
  }, 0);
  return (
    <div className="freq-area">
      <div className="frequently-brought-wrapper">
        <h5 className="brought-head h5-left-border">Frequently bought together</h5>
        <div className="frequently-brought-area">
          {products.map(product => (
            <Product
              deadLink
              frequent
              key={product.id}
              id={product.id}
              title={product.name}
              img={product.image}
              listPrice={product.pricing.listPrice}
              salePrice={product.pricing.salePrice}
            />
          ))}
        </div>
      </div>
      <div className="total-price-add-both">
        <div className="total-price">
          Total Price{' '}
          <strong>
            <i className="fas fa-rupee-sign" />
            {totalPrice}
          </strong>
        </div>
        <div className="add-both">
          <button type="button" onClick={onBuyFrequently} className="small-btn weight-300">
            Add BOTH To Cart
          </button>
        </div>
      </div>
    </div>
  );
};

export default FrequentlyBought;
