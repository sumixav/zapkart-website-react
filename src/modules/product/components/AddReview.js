import React, { Component } from 'react'

export default class AddReview extends Component {
  render() {
    return (
      <div className="reviews-wrapper">
        <h4 className="review-head">
          <span>Add Your Review</span>
        </h4>

        <div className="review-form-wrapper">
          <h5>Rate this product</h5>
          <div className="stars-wrapper">
            <img src="/resources/images/kickill-stars.png" alt="" />
          </div>
        </div>

        <div className="form-group">
          <textarea
            name=""
            id=""
            cols="30"
            rows="10"
            className="form-control review-field"
            placeholder="Write your review  :"
          />
        </div>

        <div className="button-main">
          <button type="button" className="btn submit-btn post-btn">
            Post Your review{' '}
            <img
              className="lazy submit-icon"
              data-src="/resources/images/kickill-thumb-icon.png"
              alt=""
            />
          </button>
        </div>
      </div>
    )
  }
}
