import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ProductDetail extends Component {
  render() {
    const { title, brand, count } = this.props;
    return (
      <div className="prod-details-main">
        <div className="product-detail-head-wrapper">
          <h4 className="h4-left-border product-detail-head">{title}</h4>
          <p className="product-detail-subhead">{brand}</p>
        </div>
        {count > 0 && (
          <div className="how-many-bought-this">
            {' '}
            {/* icon image not loading */}
            <img src="/resources/images/kickill-add-cart-icon.png" alt="" className="lazy" />{' '}
            {`${count} `}
            people bought this recently
          </div>
        )}
      </div>
    );
  }
}

ProductDetail.propTypes = {
  title: PropTypes.string.isRequired,
  brand: PropTypes.string.isRequired,
  count: PropTypes.number,
};

ProductDetail.defaultProps = {
  count: 0,
};

export default ProductDetail;
