/* eslint-disable no-underscore-dangle */
import React from 'react';
import Product from 'components/Product';
import OwlCarousel from 'react-owl-carousel';
import { useSimilarProducts } from 'hooks/api';
import PropTypes from 'prop-types';
// import useProducts from 'hooks/api/useProducts';
// import similarProducts from './data.json'

const SimilarProducts = ({ organics, productTitle, productId }) => {
  // const { data, isLoading } = useProducts({ ids: productIds });

  const { isLoading, data } = useSimilarProducts(organics);
  console.log(organics);
  console.log(isLoading, data);
  console.log('SimilarProducts');
  if (organics.length === 0) return null;
  if (data && data.data && data.data.length > 0) {
    const { data: similarProducts } = data;
    return (
      <div className="similar-products-wrapper">
        <h4 className="similar-products-offers-head">{`Products similar to ${productTitle}`}</h4>
        <OwlCarousel className="owl-carousel owl-theme similar-product-slider">
          {similarProducts
            .filter(i => i._id !== productId)
            .map(item => (
              <div className="item" key={item.key}>
                <Product
                  small
                  key={item._id}
                  slug={item.slug}
                  id={item._id}
                  title={item.name}
                  description={item.textDescription}
                  price={item.pricing.salePrice}
                  img={item.images[0] ? `/${item.images[0].url}` : ''}
                />
              </div>
            ))}
        </OwlCarousel>
        {/* {isLoading && <div>Loading ... </div>}
      {data && (
        <OwlCarousel className="owl-carousel owl-theme similar-product-slider">
          {data.map(item => (
            <div className="item" key={item.key}>
              <Product
                small
                key={item.key}
                title={item.title}
                description={item.description}
                price={item.price}
                img={item.img}
              />
            </div>
          ))}
        </OwlCarousel>
      )} */}
      </div>
    );
  }
  return null;
};

SimilarProducts.propTypes = {
  organics: PropTypes.array,
  productId: PropTypes.string.isRequired,
};

SimilarProducts.defaultProps = {
  organics: [],
};

export default SimilarProducts;
