import React from 'react'
import Product from 'components/Product'
import OwlCarousel from 'react-owl-carousel'

const similarProducts = [
  {
    key: '1',
    title: 'Becadexamin Soft Gelatin Capsule',
    description: 'bottle of 30 soft gelatin capsules',
    price: '3450',
    img: '/resources/images/products/kickill-product-7.png',
  },
  {
    key: '2',
    img: '/resources/images/products/kickill-product-2.png',
    title: 'Becadexamin Soft Gelatin Capsule',
    description: 'bottle of 30 soft gelatin capsules',
    price: '3450',
  },
  {
    key: '3',
    img: '/resources/images/products/kickill-product-2.png',
    title: 'Becadexamin Soft Gelatin Capsule',
    description: 'bottle of 30 soft gelatin capsules',
    price: '3450',
  },
  {
    key: '4',
    img: '/resources/images/products/kickill-product-2.png',
    title: 'Becadexamin Soft Gelatin Capsule',
    description: 'bottle of 30 soft gelatin capsules',
    price: '3450',
  },
]
const SimilarProducts = ({ productTitle }) => {
  console.log('SimilarProducts')
  return (
    <div className="similar-products-wrapper">
      <h4 className="similar-products-offers-head">{`Products similar to ${productTitle}`}</h4>
      <OwlCarousel className="owl-carousel owl-theme similar-product-slider">
        {similarProducts.map(item => (
          <div className="item" key={item.key}>
            <Product
              small
              key={item.key}
              id={item.key}
              title={item.title}
              description={item.description}
              price={item.price}
              img={item.img}
            />
          </div>
        ))}
      </OwlCarousel>
    </div>
  )
}

export default SimilarProducts
