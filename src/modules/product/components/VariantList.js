/* eslint-disable no-underscore-dangle */
import React from 'react';
import useFetching from 'hooks/useFetching';
import { CATALOG_API } from '_constants';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const VariantList = ({ productId, isVariant }) => {
  console.log(productId, isVariant);
  const [{ response, loading }] = useFetching(
    `${CATALOG_API.getVariants}/${productId}?status=active`,
  );
  console.log(loading, response);

  if (!isVariant) {
    console.log('not variant');
    return null;
  }
  // if (!response) return null;
  if (response) {
    if (response.data && response.data.length === 0) return null;
  }
  if (response && response.data) {
    const renderVariant = response.data.map(i => (
      <Link key={i.slug} to={`/product/${i.slug}`} className="item">
        {i.attributes.map(m => {
          console.log(m);

          return (
            <>
              <div className="attributeGroup">{m.attributeGroup.name}</div>
              <div className="attrValue">{m.value.value}</div>
            </>
          );
        })}
      </Link>
    ));
    return (
      <div className="variants-names-list">
        <div className="frequently-brought-wrapper">
          <h5 className="brought-head h5-left-border">Variants</h5>
          <div className="variants-names-list-area">{renderVariant}</div>
        </div>
      </div>
    );
  }

  return null;
};

VariantList.propTypes = {
  productId: PropTypes.string.isRequired,
  isVariant: PropTypes.bool,
};

VariantList.defaultProps = {
  isVariant: false,
};

export default VariantList;
