import React from 'react'
import PropTypes from 'prop-types'

const ProductPricing = ({ originalPrice, offerPrice, children }) => {
  return (
    <div className="product-pricing-wrapper">
      <div className="price-area">
        <h2 className="original-price">
          <i className="fas fa-rupee-sign" /> {originalPrice}
        </h2>
        <h2 className="offer-price">
          <i className="fas fa-rupee-sign" /> {offerPrice}
        </h2>
      </div>
      {children}
    </div>
  )
}

ProductPricing.propTypes = {
  originalPrice: PropTypes.number,
  offerPrice: PropTypes.number,
}

ProductPricing.defaultProps = {
  originalPrice: 0,
  offerPrice: 0,
}

export default ProductPricing
