/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Formik, Field, Form } from 'formik';
import { connect } from 'react-redux';
import { userActions } from 'redux/actions';
import { CustomInputComponent } from 'components/Input';

const RegisterForm = props => {
  const { dispatch, user } = props;
  const { loading, registerSuccess, registerError } = user;
  console.log(registerSuccess, registerError);
  console.log('register');
  if (registerSuccess) {
    return <Redirect to="/user/login" />;
  }
  return (
    <div>
      <Formik
        initialValues={{
          email: '',
          password: '',
          firstName: '',
          lastName: '',
          phoneNo: '',
          confirmPassword: '',
        }}
        validate={values => {
          const errors = {};
          const keys = Object.entries(values);
          keys.forEach(([key, value]) => {
            if (!value) errors[key] = 'Required';
          });

          if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
            errors.email = 'Invalid email address';
          }
          const passRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
          if (values.password && !values.password.match(passRegEx)) {
            errors.password =
              'Password must be between 6 to 20 characters with at least one numeric digit, one uppercase and one lowercase letter';
          } else if (
            values.password &&
            values.password.match(passRegEx) &&
            values.password !== values.confirmPassword
          ) {
            errors.confirmPassword = 'Passwords do not match';
          }
          return errors;
        }}
        onSubmit={(
          values,
          // { setSubmitting }
        ) => {
          console.log('in submit register');
          const valuesJson = JSON.stringify(values, null, 2);
          console.log(valuesJson);
          dispatch({
            type: userActions.REGISTER,
            payload: values,
          });
          // setSubmitting(isSubmitForm)
          // setTimeout(() => {
          //   console.log(JSON.stringify(valuesJson, null, 2))
          //   setSubmitting(false)
          // }, 1000)
        }}
        onBlur={e => console.log(e)}
      >
        {() => (
          // { isSubmitting }
          <Form className="form-area">
            <div className="row">
              <Field
                type="text"
                name="firstName"
                component={CustomInputComponent}
                placeholder="First Name :"
              />
              <Field
                type="text"
                name="lastName"
                component={CustomInputComponent}
                placeholder="Last Name :"
              />
              <Field
                type="email"
                name="email"
                component={CustomInputComponent}
                placeholder="Email ID :"
              />
              <Field
                type="text"
                name="phoneNo"
                component={CustomInputComponent}
                placeholder="Phone Number :"
              />
              <Field
                type="password"
                name="password"
                component={CustomInputComponent}
                placeholder="Password :"
              />
              <Field
                type="password"
                component={CustomInputComponent}
                name="confirmPassword"
                placeholder="Confirm Password :"
              />

              <div className="col-lg-12 col-md-12">
                <div className={`error-field-notify ${!registerError ? 'hidden' : ''}`}>
                  Error registering
                </div>
              </div>

              {registerSuccess && (
                <div className="col-lg-12 col-md-12">Successfully registered!</div>
              )}
              <div className="col-lg-12 col-md-12 log-forgot">
                <button type="submit" className="btn big-login-btn" disabled={loading}>
                  Register
                  <img className="lazy" src="/resources/images/kickill-user-img-white.png" alt="" />
                </button>
              </div>
              <div className="col-lg-12 col-md-12 log-forgot mt-50">
                <span>
                  Already a member ?{'  '}
                  <Link to="/user/login" className="btn big-login-btn capitalize">
                    Login Now
                  </Link>
                </span>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default connect(({ user }) => ({ user }))(RegisterForm);
