/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import { Link } from 'react-router-dom'
import { Form, Formik, Field } from 'formik'
import { connect } from 'react-redux'
// import Input from 'components/Input'
import { forgotPasswordActions } from 'redux/actions'

const LoginForm = ({ dispatch, forgotPassword }) => {
  const { error } = forgotPassword
  return (
    <div>
      <Formik
        initialValues={{ email: '', password: '' }}
        validate={values => {
          const errors = {}
          if (!values.email) {
            errors.email = 'Required'
          } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
            errors.email = 'Invalid email address'
          }

          return errors
        }}
        onSubmit={values => {
          console.log(values)
          const { email } = values
          dispatch({
            type: forgotPasswordActions.FORGOT_PASSWORD,
            payload: email,
          })
        }}
      >
        {() => (
          <Form className="form-area">
            <Field
              type="email"
              name="email"
              component={CustomInputComponent}
              className="form-control"
              placeholder="Email address"
            />

            <div className="log-forgot flex space-between align-center">
              <button type="submit" className="btn big-login-btn">
                Continue{' '}
                <img className="lazy" src="/resources/images/kickill-right-long-arrow.svg" alt="" />
              </button>
              <span>
                Back to{' '}
                <Link to="/user/login" className="btn big-login-btn ml-10 capitalize">
                  Login
                </Link>
              </span>
            </div>
          </Form>
        )}
      </Formik>
      <br />
      {error && (
        <div className="error-field">
          Email does not exist in our records. Please enter a valid email and try again
        </div>
      )}
    </div>

    // <div className="form-area">
    //   <form action="">
    //     <div className="form-group">
    //       <input type="text" className="form-control" placeholder="Username :" />
    //     </div>
    //     <div className="form-group">
    //       <input type="password" className="form-control" placeholder="Password :" />
    //     </div>
    //     <div className="log-forgot flex space-between align-center">
    //       <button type="button" className="btn big-login-btn">
    //         Login{' '}
    //         <img
    //           className="lazy"
    //           src="/resources/images/kickill-user-img-white.png"
    //           alt=""
    //         />
    //       </button>
    //       <Link to="/" className="forgot">
    //         Forgot your Password?
    //       </Link>
    //     </div>
    //   </form>
    // </div>
  )
}

const CustomInputComponent = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => (
  <div className="form-group">
    <input className="form-control" {...field} {...props} />
    {touched[field.name] && errors[field.name] && (
      <div className="error-field">{errors[field.name]}</div>
    )}
  </div>
)

// const withStyleField = props => {
//   return (
//     <div className="form-group">
//       <Field {...props} />
//     </div>
//   )
// }

export default connect(({ forgotPassword }) => ({ forgotPassword }))(LoginForm)
