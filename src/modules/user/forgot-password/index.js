import React, { Component } from 'react'
import Hero from 'components/Hero'
import Banner from 'components/Banner'
import { Link } from 'react-router-dom'
import PageSection from 'components/PageSection'
import ForgotPasswordForm from './Form'

class ForgotPassword extends Component {
  // handleFormSubmit = (values) => {
  
  // }

  render() {
    return (
      <PageSection>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="login-area-wrapper">
                <div className="login-area">
                  <div className="login-head-wrapper">
                    <h2 className="login-head">
                      <Link to="/user/forgot-password">
                        <span>Forgot your</span> Password
                      </Link>
                    </h2>
                    <h5 className="login-para">
                      To request a new password, please enter your most current registered email on
                      Zapkart
                    </h5>
                  </div>

                  <ForgotPasswordForm />

                  <div className="social-signup">
                    <h3 className="signup-with">Or Login with</h3>
                    <div className="social-signup-btns">
                      <button type="button" className="social-btn facebook-btn">
                        {' '}
                        <span className="social-icon">
                          <i className="fab fa-facebook-f" />
                        </span>{' '}
                        Facebook
                      </button>
                      <button type="button" className="social-btn google-btn">
                        {' '}
                        <span className="social-icon">
                          <i className="fab fa-google" />
                        </span>{' '}
                        Google
                      </button>
                    </div>
                  </div>

                  <div className="newuser-register all-center">
                    <a href="/#" className="medium-btn bordered-btn">
                      New User?
                    </a>
                    <a href="/#" className="medium-btn bold register-btn">
                      Register now
                    </a>
                  </div>
                </div>
                <Hero image="/resources/images/kickill-login-bg.jpg">
                  <Banner
                    title="zapkart"
                    subtitle="The Online Pharmacy"
                    slogan="WE Ensure the Lowest and Best Price for each MEDICINES"
                  />
                </Hero>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    )
  }
}

export default ForgotPassword
