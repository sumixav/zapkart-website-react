import React from 'react';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import { LoginSchema } from '_utils/Schemas';
import Input from 'components/Input';
import FormItem from 'components/FormItem';
import { userActions } from 'redux/actions';
// import Button from 'components/Button';

const MyForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,

    // handleReset
  } = props;

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(errors);
  return (
    <>
      <FormItem label="Email">
        <Input
          placeholder="Email"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          name="email"
          errors={errors.email && touched.email ? errors.email : ''}
        />
      </FormItem>
      <FormItem label="Password">
        <Input
          // icon="envelope"
          placeholder="Password"
          type="password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          name="password"
          errors={errors.password && touched.password ? errors.password : ''}
        />
      </FormItem>
      <div className="log-forgot flex space-between align-center">
        <button onClick={handleSubmit} type="button" className="btn big-login-btn">
          Login <img className="lazy" src="/resources/images/kickill-user-img-white.png" alt="" />
        </button>
        <a href="forgot.php" className="forgot">
          Forgot your Password?
        </a>
      </div>

      <div className="social-signup mt-30">
        <h3 className="signup-with text-center">Or Login with</h3>
        <div className="social-signup-btns">
          <button type="button" className="social-btn facebook-btn">
            {' '}
            <span className="social-icon">
              <i className="fab fa-facebook-f" />
            </span>{' '}
            Facebook
          </button>
          <button type="button" className="social-btn google-btn">
            {' '}
            <span className="social-icon">
              <i className="fab fa-google" />
            </span>{' '}
            Google
          </button>
        </div>
      </div>
    </>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: ({ email = '', password = '' }) => ({
    email,
    password,
  }),

  validationSchema: LoginSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(props);
    // props.onSubmit(values);
    const { dispatch } = props;
    // const { isLogged, loginError, isLogging } = props;
    const { email, password } = values;
    dispatch({
      type: userActions.LOGIN,
      payload: {
        email,
        password,
      },
    });

    setTimeout(() => {
      // alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  },

  displayName: 'BasicForm',
})(MyForm);

export default connect(({ user }) => ({ user }))(MyEnhancedForm);
