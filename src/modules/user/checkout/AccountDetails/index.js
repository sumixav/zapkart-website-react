import React from 'react';
import RegisterForm from './RegisterForm';

import LoginForm from './LoginForm';
// import Button from 'components/Button';

const AccountDetails = () => {
  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')

  return (
    <div className="row">
      <div className="col-lg-6">
        <h5 className="h4-left-border">Register Account</h5>
        <RegisterForm />
      </div>
      <div className="col-lg-6">
        <h5 className="h4-left-border">I have a Zapkart Account & Password</h5>
        <LoginForm />
      </div>
    </div>
  );
};

export default AccountDetails;
