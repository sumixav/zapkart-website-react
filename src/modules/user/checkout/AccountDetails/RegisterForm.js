import React from 'react';
import { withFormik } from 'formik';
import { RegisterSchema } from '_utils/Schemas';
import { connect } from 'react-redux';
import Input from 'components/Input';
import FormItem from 'components/FormItem';
import { userActions } from '_constants';
// import Button from 'components/Button';

const MyForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    // handleReset
  } = props;

  console.log(props);

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(errors);
  return (
    <>
      <FormItem label="First Name">
        <Input
          placeholder="First Name"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.firstName}
          name="firstName"
          errors={errors.firstName && touched.firstName ? errors.firstName : ''}
        />
      </FormItem>
      <FormItem label="Last Name">
        <Input
          placeholder="Last Name"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.lastName}
          name="lastName"
          errors={errors.lastName && touched.lastName ? errors.lastName : ''}
        />
      </FormItem>
      <FormItem label="Email">
        <Input
          placeholder="Email"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.email}
          name="email"
          errors={errors.email && touched.email ? errors.email : ''}
        />
      </FormItem>
      <FormItem label="Mobile number">
        <Input
          placeholder="Mobile number"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.mobileNo}
          name="mobileNo"
          errors={errors.mobileNo && touched.mobileNo ? errors.mobileNo : ''}
        />
      </FormItem>
      <FormItem label="Password">
        <Input
          type="password"
          placeholder="Password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.password}
          name="password"
          errors={errors.password && touched.password ? errors.password : ''}
        />
      </FormItem>
      <FormItem label="Confirm password">
        <Input
          type="password"
          placeholder="Confirm password"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.confirmPassword}
          name="confirmPassword"
          errors={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : ''}
        />
      </FormItem>
      <FormItem>
        <div className="common-check flex">
          <span>
            <input
              type="checkbox"
              name="signUpOffers"
              onChange={handleChange}
              onBlur={handleBlur}
              checked={values.signUpOffers}
            />{' '}
            Signup for offers.
          </span>
          <span>
            <a href="/#" className="normal-link">
              Privacy Statements
            </a>
          </span>
        </div>
      </FormItem>
      <div className="form-group">
        <button type="button" onClick={handleSubmit} className="commmon-btn">
          Save & Continue
        </button>
      </div>
    </>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: ({
    firstName = '',
    lastName = '',
    email = '',
    mobileNo = '',
    signUpOffers = false,
    id = '',
    password = '',
    confirmPassword = '',
  }) => ({
    firstName,
    lastName,
    email,
    mobileNo,
    signUpOffers,
    id,
    confirmPassword,
    password,
  }),

  validationSchema: RegisterSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(props);
    const { dispatch } = props;
    const { firstName, lastName, mobileNo: phoneNo, email, password } = values;
    console.log(values);
    // props.onSubmit(values);
    dispatch({
      type: userActions.REGISTER,
      payload: {
        firstName,
        lastName,
        phoneNo,
        email,
        password,
      },
    });
    setTimeout(() => {
      // alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  },

  displayName: 'BasicForm',
})(MyForm);

export default connect()(MyEnhancedForm);
