import React from 'react';
import { withFormik } from 'formik';
import { CheckoutShipAddrSchema } from '_utils/Schemas';
import Input, { TextArea } from 'components/Input';
import FormItem from 'components/FormItem';

const MyForm = props => {
  const {
    values,
    touched,
    errors,
    handleChange,
    handleSubmit,
    handleBlur,
    // handleReset
  } = props;

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(errors);
  return (
    <div className="row">
      <div className="col-lg-12">
        <h5 className="h4-left-border">Shipping Information</h5>
      </div>

      <FormItem label="First Name" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.firstName}
          name="firstName"
          errors={errors.firstName && touched.firstName ? errors.firstName : ''}
        />
      </FormItem>
      <FormItem label="Last Name" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.lastName}
          name="lastName"
          errors={errors.lastName && touched.lastName ? errors.lastName : ''}
        />
      </FormItem>
      <FormItem label="Address 1" className="col-lg-6">
        <TextArea
          name="addressOne"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.addressOne}
          errors={errors.addressOne && touched.addressOne ? errors.addressOne : ''}
        />
      </FormItem>
      <FormItem label="Address Two" className="col-lg-6">
        <TextArea
          name="addressTwo"
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.addressTwo}
          errors={errors.addressTwo && touched.addressTwo ? errors.addressTwo : ''}
        />
      </FormItem>
      <FormItem label="Country" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.country}
          name="country"
          errors={errors.country && touched.country ? errors.country : ''}
        />
      </FormItem>
      <FormItem label="Province / State" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.state}
          name="state"
          errors={errors.state && touched.state ? errors.state : ''}
        />
      </FormItem>
      <FormItem label="City" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.city}
          name="city"
          errors={errors.city && touched.city ? errors.city : ''}
        />
      </FormItem>
      <FormItem label="Postal Code / ZIP" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.pincode}
          name="pincode"
          errors={errors.pincode && touched.pincode ? errors.pincode : ''}
        />
      </FormItem>
      <FormItem label="Phone" className="col-lg-6">
        <Input
          onChange={handleChange}
          onBlur={handleBlur}
          value={values.phoneNo}
          name="phoneNo"
          errors={errors.phoneNo && touched.phoneNo ? errors.phoneNo : ''}
        />
      </FormItem>
      <FormItem className="col-lg-6">
        <div className="confirm-msg">
          {/* <label htmlFor="a" className="custom-checkbox"> */}
          <Input
            type="checkbox"
            checked={values.isDifferentAddr}
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.isDifferentAddr}
            name="isDifferentAddr"
          />
          Billing: Different From Shipping Address
          {/* </label> */}
        </div>
      </FormItem>
      <div className="col-lg-12">
        <button type="button" onClick={handleSubmit} className="commmon-btn">
          Save & Continue
        </button>
      </div>
    </div>
  );
};

const MyEnhancedForm = withFormik({
  mapPropsToValues: ({
    firstName = '',
    lastName = '',
    addressTwo = '',
    addressOne = '',
    state = '',
    city = '',
    country = '',
    pincode = '',
    phoneNo = '',
    isDifferentAddr = false,
  }) => ({
    firstName,
    lastName,
    addressTwo,
    addressOne,
    state,
    city,
    country,
    pincode,
    phoneNo,
    isDifferentAddr,
  }),

  validationSchema: CheckoutShipAddrSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(props);
    props.onSubmit(values);
    setTimeout(() => {
      // alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  },

  displayName: 'BasicForm',
})(MyForm);

export default MyEnhancedForm;
