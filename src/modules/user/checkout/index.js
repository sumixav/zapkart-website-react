import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Steps from 'components/Steps';
import AccountDetails from './AccountDetails';
import ShippingAddress from './ShippingAddress';

class Checkout extends PureComponent {
  state = {
    // eslint-disable-next-line react/destructuring-assignment
    activeIndex: this.props.user.id !== '' ? 1 : 0,
    steps: [
      {
        id: 'accountDetails',
        // isValidated: true,
        // isDisabled: this.props.user && this.props.authorized,
        isCompleted: false,
        canGoBack: false, // not used
        fields: {
          email: '',
          password: '',
          mobileNo: '',
          signUpOffers: true,
        },
      },
      {
        id: 'shippingAddr',
        // isValidated: true,
        isCompleted: true,
        canGoBack: true,
        fields: {
          firstName: '',
          lastName: '',
          addressTwo: '',
          addressOne: '',
          state: '',
          city: '',
          country: '',
          pincode: '',
          phoneNo: '',
          isDifferentAddr: false,
        },
      },
      {
        id: 'payment',
        // isValidated: false,
        isCompleted: false,
        canGoBack: true,
      },
    ],
  };

  componentDidUpdate(prevProps) {
    const { user } = this.props;
    console.log(user);
    if (user.id === '') this.setActiveIndex(0);
    const {user: prevUser} = prevProps;
    if (user.id !== '' && (prevUser.id !== user.id)) this.setActiveIndex(1)
    
  }

  setActiveIndex = i => {
    this.setState({ activeIndex: i });
  };

  onAccountDetailsSubmit = values => {
    console.log('in account details submit', values);

    this.setState(prevState => {
      const { steps } = prevState;
      const { fields } = steps[0];
      console.log(fields);
      const otherSteps = steps.filter((item, index) => index !== 0);
      console.log(otherSteps);
      Object.entries(values).forEach(([key, value]) => {
        console.log(key, value);
        console.log(key, fields[key]);
        fields[key] = value;
      });
      console.log(fields);
      steps[0].fields = fields;
      steps[0].isCompleted = true;
      steps[0].canGoBack = false;
      console.log('new steps', steps);
      const nextIndex = this.getNextIndex();
      return {
        ...prevState,
        steps: [steps[0], ...otherSteps],
        activeIndex: nextIndex,
      };
    });
  };

  getNextIndex = () => {
    const { activeIndex, steps } = this.state;
    const stepsLength = steps.length;
    let nextIndex = activeIndex;
    if (activeIndex < stepsLength) nextIndex = activeIndex + 1;
    return nextIndex;
  };

  onShippingAddrSubmit = values => {
    console.log('in shipping addr', values);
    this.setState(prevState => {
      const { steps } = prevState;
      const { fields } = steps[1];
      console.log(fields);
      // const otherSteps = steps.filter((item, index) => index !== 0);
      // console.log(otherSteps);
      Object.entries(values).forEach(([key, value]) => {
        console.log(key, value);
        console.log(key, fields[key]);
        // if (Object.prototype.hasOwnProperty(key) && value) fields[key] = value;
        fields[key] = value;
      });
      console.log(fields);
      steps[1].fields = fields;
      steps[1].isCompleted = true;
      steps[1].canGoBack = true;
      console.log('new steps', steps);
      const nextIndex = this.getNextIndex();
      return {
        ...prevState,
        steps: [steps[0], steps[1], steps[2]],
        activeIndex: nextIndex,
      };
    });
  };

  render() {
    const { steps: stepsState, activeIndex } = this.state;
    console.log('steps', stepsState);
    const { user } = this.props;
    const { fields: fieldsA } = stepsState[0] || '';
    const { fields: fieldsB } = stepsState[1] || '';
    // const { fields: fieldsC } = stepsState[2] || '';
    // const isValidatedValues = stepsState.map(item => item.isValidated);
    const steps = [
      {
        menuItem: (
          <>
            <span className="step-number">Step 1</span>
            <h4 className="h4-left-border">Account details</h4>
          </>
        ),
        id: 'accountDetails',
        content: (
          <AccountDetails
            {...fieldsA}
            onSubmit={this.onAccountDetailsSubmit}
            disabled={user && user.id}
          />
        ),
        // isValidated: isValidatedValues[0],
        isCompleted: stepsState[0].isCompleted || false,
        canGoBack: stepsState[0].canGoBack || false,
      },
      {
        menuItem: (
          <>
            <span className="step-number">Step 2</span>
            <h4 className="h4-left-border">Shipping Address</h4>
          </>
        ),
        id: 'shippingAddr',
        content: <ShippingAddress {...fieldsB} onSubmit={this.onShippingAddrSubmit} />,
        // isValidated: isValidatedValues[1],
        isCompleted: stepsState[1].isCompleted || false,
        canGoBack: stepsState[1].canGoBack || false,
      },
      {
        menuItem: (
          <>
            <span className="step-number">Step 3</span>
            <h4 className="h4-left-border">Payment Options</h4>
          </>
        ),
        id: 'payment',
        content: 'Payment Gateway',
        // isValidated: isValidatedValues[2],
        isCompleted: stepsState[2].isCompleted || false,
        canGoBack: stepsState[2].canGoBack || false,
      },
    ];

    return (
      <section className="inner-page-wrapper">
        <div className="inner-container">
          <Steps activeIndex={activeIndex}>
            {steps.map(step => {
              console.log(step.isCompleted, step.canGoBack);
              return (
                <Steps.Panel
                  // isValidated={step.isValidated}
                  // isCompleted={step.isCompleted}
                  // canGoBack={step.canGoBack}
                  menuItem={step.menuItem}
                  id={step.id}
                  key={step.id}
                >
                  {step.content}
                </Steps.Panel>
              );
            })}
          </Steps>
        </div>
      </section>
    );
  }
}

export default connect(({ user }) => ({ user }))(Checkout);
