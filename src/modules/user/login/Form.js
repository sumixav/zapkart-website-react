/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import { Link } from 'react-router-dom'
import { Form, Formik, Field } from 'formik'
import { connect } from 'react-redux'
import { userActions } from 'redux/actions'

const LoginForm = ({ dispatch, user }) => {
  const { isLogged, loginError, isLogging } = user
  console.log(isLogged, loginError, isLogging)
  return (
    <div>
      <Formik
        initialValues={{ email: '', password: '' }}
        validate={values => {
          const errors = {}
          if (!values.email) {
            errors.email = 'Required'
          } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
            errors.email = 'Invalid email address'
          }
          if (!values.password) {
            errors.password = 'Required'
          }
          return errors
        }}
        onSubmit={(values, { setSubmitting }) => {
          console.log('submitting form')
          setSubmitting(true)
          dispatch({
            type: userActions.LOGIN,
            payload: {
              email: values.email,
              password: values.password,
            },
          })
          if (!isLogging) setSubmitting(false)
          // setTimeout(() => {
          //   console.log('submitted form')
          //   console.log(JSON.stringify(values, null, 2))
          //   setSubmitting(false)
          // }, 1000)
        }}
      >
        {() => (
          <Form className="form-area">
            <Field
              type="email"
              name="email"
              component={CustomInputComponent}
              className="form-control"
              placeholder="Username :"
            />
            <Field
              type="password"
              component={CustomInputComponent}
              name="password"
              className="form-control"
              placeholder="Password :"
            />
            <div className="log-forgot flex space-between align-center">
              {/* <button type="submit" className="btn big-login-btn" disabled={isSubmitting}> */}
              <button type="submit" className="btn big-login-btn" disabled={isLogging}>
                Login{' '}
                <img className="lazy" src="/resources/images/kickill-user-img-white.png" alt="" />
              </button>
              <Link to="/user/forgot-password" className="forgot">
                Forgot your Password?
              </Link>
            </div>
          </Form>
        )}
      </Formik>
      {loginError && (
        <div className="error-field-notify">
          Error logging in. Please check your username and password and try again
        </div>
      )}
      {/* {isLogged && <div>Logged in! Redirecting back to home</div>}
      {isLogged &&
        setTimeout(() => {
          return <Redirect to="/home" />
        }, 30000)} */}
    </div>

    // <div className="form-area">
    //   <form action="">
    //     <div className="form-group">
    //       <input type="text" className="form-control" placeholder="Username :" />
    //     </div>
    //     <div className="form-group">
    //       <input type="password" className="form-control" placeholder="Password :" />
    //     </div>
    //     <div className="log-forgot flex space-between align-center">
    //       <button type="button" className="btn big-login-btn">
    //         Login{' '}
    //         <img
    //           className="lazy"
    //           src="/resources/images/kickill-user-img-white.png"
    //           alt=""
    //         />
    //       </button>
    //       <Link to="/" className="forgot">
    //         Forgot your Password?
    //       </Link>
    //     </div>
    //   </form>
    // </div>
  )
}

const CustomInputComponent = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => (
  <div className="form-group">
    <input className="form-control" {...field} {...props} />
    {touched[field.name] && errors[field.name] && (
      <div className="error-field">{errors[field.name]}</div>
    )}
  </div>
)

// const withStyleField = props => {
//   return (
//     <div className="form-group">
//       <Field {...props} />
//     </div>
//   )
// }

export default connect(({ user }) => ({ user }))(LoginForm)
