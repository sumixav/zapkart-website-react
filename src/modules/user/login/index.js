import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import Hero from 'components/Hero'
import Banner from 'components/Banner'
import { Link, Redirect } from 'react-router-dom'
import PageSection from 'components/PageSection'
import { connect } from 'react-redux'
import LoginForm from './Form'
import { userActions } from 'redux/actions';
import FacebookLoginWithButton from 'components/SocialMedia/facebook/facebook-with-button'
import { GoogleLogin } from 'react-google-login';
const mapStateToProps = ({user}) => ({user})



class Login extends Component {
  
  render() {
    const { dispatch } = this.props;

    const responseGoogle = (response) => {
      console.log(response.profileObj);
      dispatch({
        type: userActions.GBLOGIN,
        payload: {
          email: response.profileObj.email,
          name: response.profileObj.name,
          id: response.profileObj.googleId,
        },
      })
    }

    const responseFacebook = (response) => {
      console.log(response);
      dispatch({
        type: userActions.FBLOGIN,
        payload: {
          email: response.email,
          name: response.name,
          id: response.id,
        },
      })
    };
    const { user } = this.props
    console.log(user)
    if (user.isLogged) return (<Redirect to='/home' />)
    return (
      <>
        <Helmet title="Login" />
        <PageSection className="bg-grey-lite">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="login-area-wrapper">
                  <div className="login-area">
                    <div className="login-head-wrapper">
                      <h2 className="login-head">
                        <span>Login to</span> Zapkart
                      </h2>
                      <h5 className="login-para">
                        Lorem ipsum dolor sit a ut labore et dolore magna aliqua. Quis ipsum
                        suspendisse ultrices gravida.{' '}
                      </h5>
                    </div>
                    <LoginForm />
                    <div className="social-signup">
                      <h3 className="signup-with">Or Login with</h3>
                      <div className="social-signup-btns">
                       
                        <FacebookLoginWithButton
            appId="195007881710099"
            fields="name,email,picture"
            callback={responseFacebook}
            icon="fa-facebook"
          />
          
                        {/* <button type="button" className="social-btn google-btn">
                          <span className="social-icon">
                            <i className="fab fa-google" />
                          </span>
                          Google
                        </button> */}
                        <GoogleLogin
    clientId="891830764310-e0qh1p7236786tpvarco5ktqfvbr1v20.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />
                      </div>
                    </div>

                    <div className="newuser-register all-center">
                      <Link to="/user/register" className="medium-btn bordered-btn">
                        New User?
                      </Link>
                      <Link to="/user/register" className="medium-btn bold register-btn">
                        Register now
                      </Link>
                    </div>
                  </div>
                  <Hero image="/resources/images/kickill-login-bg.jpg">
                    <Banner
                      title="Zapkart"
                      subtitle="The Online Pharmacy"
                      slogan="WE Ensure the Lowest and Best Price for each MEDICINES"
                    />
                  </Hero>
                </div>
              </div>
            </div>
          </div>
        </PageSection>
      </>
    )
  }
}

export default connect(mapStateToProps)(Login)
