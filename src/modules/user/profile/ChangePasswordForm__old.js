import React from 'react'
import { withFormik } from 'formik'
import { CurrentPasswordSchema, NewPasswordSchema } from '_utils/Schemas'
import Input from 'components/Input'
import FormItem from 'components/FormItem'
import Row from 'components/Row'
import * as Button from 'components/Button'
import { Transition } from 'react-spring/renderprops'

// const [activeIndex, setActiveIndex] = useState('1')

const CurrentPasswordForm = ({
  // active,
  handleChange,
  handleBlur,
  handleSubmit,
  values,
  errors,
  touched,
  isSubmitting,
}) => {
  console.log(isSubmitting)
  // if (!active) return null
  return (
    <>
      <Row>
        <FormItem className="col-xl-6 col-lg-6">
          <Input
            placeholder="Current Password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.currentPassword}
            name="currentPassword"
            errors={errors.currentPassword && touched.currentPassword ? errors.currentPassword : ''}
          />
        </FormItem>
      </Row>
      <FormItem>
        <Button.SubmitButton onClick={handleSubmit} loading={isSubmitting}>
          Next
        </Button.SubmitButton>
      </FormItem>
    </>
  )
}

const NewPasswordForm = ({
  // active,
  handleChange,
  handleBlur,
  handleSubmit,
  values,
  errors,
  touched,
  isSubmitting,
}) => {
  console.log(isSubmitting)
  // if (!active) return null
  return (
    <>
      <Row>
        <FormItem className="col-xl-6 col-lg-6">
          <Input
            placeholder="NewPassword"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.newPassword}
            name="newPassword"
            errors={errors.newPassword && touched.newPassword ? errors.newPassword : ''}
          />
        </FormItem>
        <FormItem className="col-xl-6 col-lg-6">
          <Input
            placeholder="ConfirmPassword"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.confirmPassword}
            name="confirmPassword"
            errors={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : ''}
          />
        </FormItem>
      </Row>
      <FormItem>
        <Button.SubmitButton onClick={handleSubmit} loading={isSubmitting}>
          Save
        </Button.SubmitButton>
      </FormItem>
    </>
  )
}

class MyForm extends React.Component {
  state = {
    activeIndex: '1',
  }

  handleSubmitCur = a => {
    console.log('yo', a)
    setTimeout(() => {
      this.setState({ activeIndex: '2' })
      // setSubmitting(false)
    }, 1000)
  }

  handleSubmitNew = a => {
    console.log('yo new pass', a)
  }

  render() {
    const { activeIndex } = this.state
    return (
      <>
        <div className="information-head-wrapper">
          <h4 className="information-head">
            Change Password{' '}
            <img src="/resources/images/kickill-help-icon.png" alt="" className="lazy" />
          </h4>
          {/* <button type="button" className="extra-small-btn">
          <img className="lazy edit-icon" src="/resources/images/kickill-down-arrow.svg" alt="" />
        </button> */}
        </div>

        <Transition
          items={activeIndex === '1'}
          from={{ transform: 'translate3d(0px,0px,0)' }}
          enter={{ transform: 'translate3d(0px,0px,0)' }}
          leave={{ transform: 'translate3d(-500px,0,0)' }}
          config={{ duration: 500 }}
        >
          {isPass =>
            isPass
              ? props => (
                <div style={props}>
                  <CurrentPassFormik
                    active={activeIndex === '1'}
                    onSubmit={a => this.handleSubmitCur(a)}
                  />
                </div>
                )
              : ''}
        </Transition>
        <Transition
          items={activeIndex === '2'}
          from={{ transform: 'translate3d(2000px,0px,0)' }}
          enter={{ transform: 'translate3d(0px,0px,0)' }}
          config={{ duration: 500 }}
        >
          {isPass =>
            isPass
              ? props => (
                <div style={props}>
                  <NewPassFormik
                    active={activeIndex === '2'}
                    onSubmit={a => this.handleSubmitNew(a)}
                  />
                </div>
                )
              : ''}
        </Transition>
      </>
    )
  }
}

export const CurrentPassFormik = withFormik({
  mapPropsToValues: () => ({
    currentPassword: '',
  }),

  validationSchema: CurrentPasswordSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(values)
    setSubmitting(true)
    props.onSubmit(values, () => setSubmitting(false))
    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false)
    // }, 1000)
    // setStepOne(false)
    // setStepTwo(true)
  },

  displayName: 'CurrentPasswordForm',
})(CurrentPasswordForm)

export const NewPassFormik = withFormik({
  mapPropsToValues: () => ({
    newPassword: '',
    confirmPassword: '',
  }),

  validationSchema: NewPasswordSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    setSubmitting(true)
    console.log(values)
    props.onSubmit(values, () => setSubmitting(false))
    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false)
    //   // setActiveIndex('1')
    // }, 1000)
    // setStepOne(false)
    // setStepTwo(true)
  },

  displayName: 'NewPasswordForm',
})(NewPasswordForm)

export default MyForm
