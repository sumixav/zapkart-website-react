import React from 'react'

const UserAddress = ({ addressType, name, phoneNo, address,id, pincode, onEditAddress }) => {
 console.log(id)
  return (
    <div className="address-wrapper">
      <div className="address-location">{addressType}</div>
      <button type="button" className="address-edit-icon" onClick={() => onEditAddress(id)}>
        <img
          src="/resources/images/kickill-edit-icons-grey.svg"
          alt=""
          className="lazy edit-icon"
        />
      </button>
      <h4 className="address-name">{`${name} ${phoneNo}`}</h4>
      <p>{`${address} ${pincode}`}</p>
    </div>
  )
}

export default UserAddress
