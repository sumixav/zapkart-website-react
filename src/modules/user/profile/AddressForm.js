import React from 'react'
import { withFormik } from 'formik'
import { UserAddressSchema } from '_utils/Schemas'
import Input from 'components/Input'
import FormItem from 'components/FormItem'

const MyForm = props => {
  const { values, touched, errors, handleChange, handleSubmit, handleBlur, handleReset } = props

  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(errors)
  return (
    <div
      style={{
        padding: '1rem',
      }}
    >
      <form>
        <FormItem label="Address Type">
          <Input
            placeholder="Address Name"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.addressType}
            name="addressType"
            errors={errors.addressType && touched.addressType ? errors.addressType : ''}
          />
        </FormItem>
        <FormItem label="Name">
          <Input
            placeholder="Name"
            onChange={handleChange}
            onBlur={handleBlur}
            name="name"
            value={values.name}
            errors={errors.name && touched.name ? errors.name : ''}
          />
        </FormItem>
        <FormItem label="Phone No">
          <Input
            placeholder="Phone No"
            onChange={handleChange}
            onBlur={handleBlur}
            name="phoneNo"
            value={values.phoneNo}
            errors={errors.phoneNo && touched.phoneNo ? errors.phoneNo : ''}
          />
        </FormItem>
        <FormItem label="Address">
          <Input
            placeholder="Address"
            onChange={handleChange}
            onBlur={handleBlur}
            name="address"
            value={values.address}
            errors={errors.address && touched.address ? errors.address : ''}
          />
        </FormItem>
        <FormItem label="Pincode">
          <Input
            placeholder="Pincode"
            onChange={handleChange}
            onBlur={handleBlur}
            name="pincode"
            value={values.pincode}
            errors={errors.pincode && touched.pincode ? errors.pincode : ''}
          />
        </FormItem>
        <div className="modal-footer">
          <button type="submit" onClick={handleSubmit} className="btn save-btn">
            Save changes
          </button>
          <button
            type="button"
            onClick={() => handleReset()}
            className="btn btn-secondary"
            data-dismiss="modal"
          >
            Reset
          </button>
        </div>
      </form>
    </div>
  )
}

const MyEnhancedForm = withFormik({
  mapPropsToValues: ({ addressType = '', name = '', phoneNo = '', pincode = '', address = '', id = '' }) => ({
    addressType,
    name,
    phoneNo,
    pincode,
    address,
    id
  }),

  validationSchema: UserAddressSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(props)
    props.onSubmit(values)
    setTimeout(() => {
      // alert(JSON.stringify(values, null, 2));
      setSubmitting(false)
    }, 1000)
  },

  displayName: 'BasicForm',
})(MyForm)

export default MyEnhancedForm
