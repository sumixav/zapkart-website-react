import React, { Component } from 'react'
import PageSection from 'components/PageSection'
import Modal from 'components/Modal'
import UserAddress from './components/UserAddress'
import EditProfileForm from './EditProfileForm'
import AddressForm from './AddressForm'
// import ChangePasswordForm from './ChangePasswordForm'
import ChangePassword from './ChangePassword'
import { addressData as addressJSON } from './data.json'

class UserProfile extends Component {
  state = {
    addressData: [],
    isAddrModalOpen: false,
    currentEditItem: null,
  }

  componentDidMount() {
    this.setState({
      addressData: addressJSON,
    })
  }

  handleEditAddress = id => {
    console.log('clicked to edit address', id)
    const { addressData } = this.state
    let currentEditItem = {}
    if (id) {
      // edit current
      currentEditItem = addressData.find(item => item.id === id)
    } else {
      currentEditItem = {
        id: '',
        addressType: '',
        name: '',
        phoneNo: '',
        address: '',
        pincode: '',
      }
    }
    console.log(currentEditItem)
    this.setState({
      isAddrModalOpen: true,
      currentEditItem,
    })
  }

  handleCloseAddrModal = () => {
    console.log('closing modal')
    this.setState({ isAddrModalOpen: false })
  }

  handleSaveAddressForm = a => {
    console.log('saving address form', a)

    this.setState(prevState => {
      const updatedAddrs = [...prevState.addressData]

      const newItem = updatedAddrs.indexOf(item => item.id === a.id) === -1
      console.log(newItem)
      let ad = updatedAddrs.find(item => item.id === a.id)
      if (!ad) ad = {}
      Object.entries(a).map(([key, value]) => {
        ad[key] = value
        return ''
      })
      console.log(ad)
      if (newItem) {
        ad.id = updatedAddrs.length + 1
        updatedAddrs.push(ad)
      }
      return {
        addressData: updatedAddrs,
        isAddrModalOpen: false,
      }
    })

    // this.setState({ isAddrModalOpen: false })
  }

  handleSubmitProfile = values => {
    console.log('handleSubmitProfile', values)
    // return new Promise(resolve => setTimeout(() => resolve, 4000))
  }

  render() {
    const { addressData, isAddrModalOpen, currentEditItem } = this.state
    const addresses = addressData.map(item => (
      <UserAddress
        id={item.id}
        addressType={item.addressType}
        name={item.name}
        phoneNo={item.phoneNo}
        address={item.address}
        pincode={item.pincode}
        onEditAddress={this.handleEditAddress}
        key={item.id}
      />
    ))

    return (
      <PageSection className="bg-grey-lite">
        <Modal
          noFooter
          title="Edit Address"
          show={isAddrModalOpen}
          onClose={this.handleCloseAddrModal}
        >
          {isAddrModalOpen && (
            <AddressForm
              id={currentEditItem.id}
              addressType={currentEditItem.addressType}
              phoneNo={currentEditItem.phoneNo}
              name={currentEditItem.name}
              address={currentEditItem.address}
              pincode={currentEditItem.pincode}
              onSubmit={this.handleSaveAddressForm}
            />
          )}
        </Modal>
        <div className="inner-container">
          <div className="row">
            <div className="col-lg-6">
              <div className="information-wrapper" style={{ marginBottom: '.5rem' }}>
                <EditProfileForm data="s" onSubmit={this.handleSubmitProfile} />
              </div>
              <div className="information-wrapper">
                <ChangePassword />
              </div>
            </div>

            <div className="col-lg-6">
              <div className="information-wrapper">
                <div className="all-address-area">
                  <div className="information-head-wrapper">
                    <h4 className="information-head">Manage Addresses</h4>
                    <button
                      type="button"
                      onClick={() => this.handleEditAddress()}
                      className="extra-small-btn"
                    >
                      <img
                        className="lazy edit-icon"
                        src="/resources/images/kickill-location-white.png"
                        alt=""
                      />{' '}
                      Add New Address
                    </button>
                  </div>
                  <div className="content">{addresses}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    )
  }
}

export default UserProfile
