import React, { useState } from 'react'
import { withFormik } from 'formik'
import { UserProfileSchema } from '_utils/Schemas'
import Input, { RadioButtona } from 'components/Input'
import FormItem from 'components/FormItem'
import Row from 'components/Row'

const MyForm = props => {
  const { values, touched, errors, handleChange, handleBlur, handleSubmit, handleReset, isSubmitting } = props
  const [isEdit, setEdit] = useState(false)
  console.log(props)
  // console.log(errors.firstName && touched.firstName ? errors.firstName : '')
  console.log(errors)
  return (
    <>
      <div className="information-head-wrapper">
        <h4 className="information-head">Personal Information</h4>
        <button
          type="button"
          onClick={() => {
            setEdit(prevEdit => !prevEdit)
            handleReset()
          }}
          className="extra-small-btn"
        >
          <img className="lazy edit-icon" src="/resources/images/kickill-edit-icons.svg" alt="" />
          Edit Information
        </button>
      </div>
      <form onSubmit={handleSubmit}>
        <Row>
          <FormItem className="col-xl-6 col-lg-6">
            <Input
              placeholder="First Name"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.firstName}
              name="firstName"
              errors={errors.firstName && touched.firstName ? errors.firstName : ''}
              disabled={!isEdit}
            />
          </FormItem>
          <FormItem className="col-xl-6 col-lg-6">
            <Input
              placeholder="Last Name"
              onChange={handleChange}
              onBlur={handleBlur}
              name="lastName"
              value={values.lastName}
              errors={errors.lastName && touched.lastName ? errors.lastName : ''}
              disabled={!isEdit}
            />
          </FormItem>
          <FormItem className="col-xl-12 col-lg-12">
            <div className="gender">
              <span>Your Gender</span>
              <RadioButtona
                disabled={!isEdit}
                label="Male"
                type="radio"
                name="gender"
                value="male"
                defaultChecked={values.gender === 'male'}
                onChange={handleChange}
              />
              <RadioButtona
                disabled={!isEdit}
                label="Female"
                type="radio"
                name="gender"
                value="female"
                defaultChecked={values.gender === 'female'}
                onChange={handleChange}
              />
            </div>
          </FormItem>
          <FormItem className="col-xl-6 col-lg-6">
            <Input
              placeholder="Email Address"
              label="Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              name="email"
              value={values.email}
              errors={errors.email && touched.email ? errors.email : ''}
              disabled={!isEdit}
            />
          </FormItem>
          <FormItem className="col-xl-6 col-lg-6">
            <Input
              placeholder="Mobile"
              name="phoneNo"
              label="Mobile"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phoneNo}
              disabled={!isEdit}
              errors={errors.phoneNo && touched.phoneNo ? errors.phoneNo : ''}
            />
          </FormItem>
        </Row>
      </form>
      {isEdit && (
        <button type="button" onClick={handleSubmit} loading={isSubmitting} className="save-btn">
          Save
        </button>
      )}
    </>
  )
}

const MyEnhancedForm = withFormik({
  mapPropsToValues: () => ({
    firstName: 'Sumi',
    lastName: 'Xavier',
    email: 'sumi@gmail.com',
    gender: 'male',
    phoneNo: '919998787678',
  }),

  validationSchema: UserProfileSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    setSubmitting(true)
    props.onSubmit(values, () => setSubmitting(false))
    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(true)
    // }, 1000)
  },

  displayName: 'BasicForm',
})(MyForm)

export default MyEnhancedForm
