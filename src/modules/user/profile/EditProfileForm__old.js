/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react'
// import { Link } from 'react-router-dom'
import { Form, Field, Formik, withFormik } from 'formik'
import Input from 'components/Input'
import { Transition } from 'react-spring/renderprops'
import {validateUserProfile} from '_utils/validation'

const EditProfileForm = () =>
  // { dispatch, isSubmitForm }
  {
    const [isEdit, setIsEdit] = useState(false)
    const [isPassword, setIsPassword] = useState(false)


    return (
      <div>
        <Formik
          initialValues={{
            firstName: 'Sumi',
            lastName: 'Xaver',
            gender: 'female',
            phoneNo: '9998987898',
            email: 'sumi@gmail.com',
            currentPassword: '',
            newPassword: '',
            confirmPassword: '',
          }}
          validate={values => validateUserProfile(values, isEdit, isPassword)}
          onSubmit={(
            values,
            // { resetForm }
          ) => {
            console.log('hi there')
     
            if (!isPassword) {
              delete values.currentPassword
              delete values.newPassword
              delete values.confirmPassword
            }
            if (!isEdit) {
              delete values.firstName
              delete values.lastName
              delete values.gender
              delete values.phoneNo
              delete values.email
            }
            if (isEdit || isPassword) {
              const valuesJson = JSON.stringify(values, null, 2) // for password call api protected route passing token
              console.log(valuesJson)
            } else {
              console.log('nothing edited')
            }

            // dispatch({
            //   type: 'KL',
            //   payload: valuesJson,
            // })
            // setSubmitting(isSubmitForm)
            // setTimeout(() => {
            //   console.log(JSON.stringify(valuesJson, null, 2))
            //   setSubmitting(false)
            // }, 1000)
          }}
          onChange={e => console.log(e)}
        >
          {({ handleReset }) => (
            <Form>
              <div className="information-wrapper">
                <div className="information-area">
                  <div className="information-head-wrapper">
                    <h4 className="information-head">Personal Information</h4>
                    <button
                      onClick={() => {
                        setIsEdit(previdEdit => !previdEdit)
                        handleReset()
                      }}
                      type="button"
                      className="extra-small-btn"
                    >
                      <img
                        className="lazy edit-icon"
                        src="/resources/images/kickill-edit-icons.svg"
                        alt=""
                      />{' '}
                      Edit Informations
                    </button>
                  </div>

                  <div className="row">
                    <Field
                      type="text"
                      disabled={!isEdit}
                      name="firstName"
                      component={Input}
                      className="form-control"
                      placeholder="First Name :"
                    />
                    <Field
                      type="text"
                      disabled={!isEdit}
                      name="lastName"
                      component={Input}
                      className="form-control"
                      placeholder="Last Name :"
                    />
                    <Field
                      disabled={!isEdit}
                      type="email"
                      name="email"
                      component={Input}
                      className="form-control"
                      placeholder="Email ID :"
                    />
                    <Field
                      disabled={!isEdit}
                      type="text"
                      name="phoneNo"
                      component={Input}
                      className="form-control"
                      placeholder="Phone Number :"
                    />
                  </div>
                </div>
                <div className="password-wrapper">
                  <div className="information-head-wrapper">
                    <h4 className="information-head">
                      Change Password{' '}
                      <img src="/resources/images/kickill-help-icon.png" alt="" className="lazy" />
                    </h4>
                    <button
                      type="button"
                      onClick={() => setIsPassword(prevValue => !prevValue)}
                      className="extra-small-btn"
                    >
                      {isPassword ? 'Cancel' : 'Edit'}
                      <img
                        className="lazy edit-icon"
                        src="/resources/images/kickill-down-arrow.svg"
                        alt=""
                      />
                    </button>
                  </div>

                  <Transition
                    items={isPassword}
                    from={{ transform: 'translate3d(0,-50px,0)', opacity: 0 }}
                    enter={{ transform: 'translate3d(0,0,0)', opacity: 1 }}
                    leave={{ opacity: 0 }}
                    config={{ duration: 200 }}
                  >
                    {isPass =>
                      isPass
                        ? props => (
                          <div style={props}>
                            <div className="row">
                              <Field
                                type="password"
                                name="currentPassword"
                                component={Input}
                                className="form-control"
                                placeholder="Current password :"
                              />
                              <Field
                                type="password"
                                name="newPassword"
                                component={Input}
                                className="form-control"
                                placeholder="Password :"
                              />
                              <Field
                                type="password"
                                component={Input}
                                name="confirmPassword"
                                className="form-control"
                                placeholder="Confirm Password :"
                              />
                            </div>
                          </div>
                          )
                        : ''}
                  </Transition>
                </div>
                <div className="col-xl-6 col-lg-6 form-group">
                  <label htmlFor="save" className="form-label d-none d-lg-block">
                    &nbsp;
                  </label>
                  <button type="submit" className="save-btn">
                    Save
                  </button>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    )
  }

export const a = props => {
  const { values, touched, errors, handleChange, handleBlur, handleSubmit } = props
  return (
    <div className="information-wrapper">
      <form onSubmit={handleSubmit}>
        <div className="information-area">
          <div className="information-head-wrapper">
            <h4 className="information-head">Personal Information</h4>
            <div className="extra-small-btn">
              <img
                className="lazy edit-icon"
                src="/resources/images/kickill-edit-icons.svg"
                alt=""
              />{' '}
              Edit Informations
            </div>
          </div>

          <div className="row">
            <div className="col-xl-6 col-lg-6 form-group">
              <input
                type="text"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.firstName}
                name="firstName"
              />
              {errors.firstName && touched.firstName && <div id="feedback">{errors.firstName}</div>}
            </div>
            <div className="col-xl-6 col-lg-6 form-group">
              <input
                type="text"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.LastName}
                name="lastName"
              />
              {errors.lastName && touched.lastName && <div id="feedback">{errors.lastName}</div>}
            </div>
            <div className="col-xl-12 col-lg-12 form-group">
              <div className="gender">
                <span>Your Gender</span>
                <label htmlFor="radio" className="form-check-label">
                  Male
                  <span className="checkmark" />
                </label>
                <input
                  type="radio"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.gender}
                  name="gender"
                  checked={values.gender}
                />
                <label htmlFor="radio" className="form-check-label">
                  Female
                  <span className="checkmark" />
                  <input
                    type="radio"
                    name="gender"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.gender}
                    checked={values.gender}
                  />
                </label>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 form-group">
              <label htmlFor="Email" className="form-label">
                Email Address
              </label>
              <input
                type="text"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                name="email"
              />
              {errors.email && touched.email && <div id="feedback">{errors.email}</div>}
            </div>
            <div className="col-xl-6 col-lg-6 form-group">
              <label htmlFor="Mobile" className="form-label">
                Mobile Number
              </label>
              <input
                type="text"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phoneNo}
                name="phoneNo"
              />
              {errors.phoneNo && touched.phoneNo && <div id="feedback">{errors.phoneNo}</div>}
            </div>
          </div>
        </div>
        <div className="password-wrapper">
          <div className="information-head-wrapper">
            <h4 className="information-head">
              Change Password{' '}
              <img src="/resources/images/kickill-help-icon.png" alt="" className="lazy" />
            </h4>
            <div className="extra-small-btn">
              Cancel{' '}
              <img
                className="lazy edit-icon"
                src="/resources/images/kickill-down-arrow.svg"
                alt=""
              />
            </div>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6 form-group">
              <label htmlFor="Email" className="form-label">
                Type Current Password
              </label>
              <input
                type="password"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
                name="password"
              />
              {errors.password && touched.password && <div id="feedback">{errors.password}</div>}
            </div>
            <div className="col-xl-6 col-lg-6 form-group">
              <label htmlFor="password" className="form-label">
                Type New Password
              </label>
              <input
                type="password"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.newPassword}
                name="newPassword"
              />
              {errors.newPassword && touched.newPassword && (
                <div id="feedback">{errors.newPassword}</div>
              )}
            </div>
            <div className="col-xl-6 col-lg-6 form-group">
              <label htmlFor="Email" className="form-label">
                Retype New Password
              </label>
              <input
                type="password"
                className="form-control"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.newPasswordConfirm}
                name="newPasswordConfirm"
              />
              {errors.newPasswordConfirm && touched.newPasswordConfirm && (
                <div id="feedback">{errors.newPasswordConfirm}</div>
              )}
            </div>
            <div className="col-xl-6 col-lg-6 form-group">
              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
              <label htmlFor="save" className="form-label d-none d-lg-block">
                &nbsp;
              </label>
              <button type="submit" className="save-btn">
                Save
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  )
}

export const MyEnhancedForm = withFormik({
  mapPropsToValues: () => ({
    firstName: '',
    lastName: '',
    gender: 'male',
    password: '',
    newPassword: '',
    newPasswordCofirm: '',
  }),

  // Custom sync validation
  validate: values => {
    const errors = {}

    if (!values.name) {
      errors.name = 'Required'
    }

    return errors
  },

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      console.log(values)
      setSubmitting(false)
    }, 1000)
  },

  displayName: 'BasicForm',
})(a)

export default EditProfileForm
