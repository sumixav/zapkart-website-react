import React from 'react'
import { withFormik } from 'formik'
import { CurrentPasswordSchema} from '_utils/Schemas'
import Input from 'components/Input'
import FormItem from 'components/FormItem'
import Row from 'components/Row'
import * as Button from 'components/Button'

const CurrentPasswordForm = ({
  // active,
  handleChange,
  handleBlur,
  handleSubmit,
  values,
  errors,
  touched,
  isSubmitting,
}) => {
  console.log(isSubmitting)
  // if (!active) return null
  return (
    <>
      <Row>
        <FormItem className="col-xl-6 col-lg-6">
          <Input
            placeholder="Current Password"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.currentPassword}
            name="currentPassword"
            errors={errors.currentPassword && touched.currentPassword ? errors.currentPassword : ''}
          />
        </FormItem>
      </Row>
      <FormItem>
        <Button.SubmitButton onClick={handleSubmit} loading={isSubmitting}>
          Next
        </Button.SubmitButton>
      </FormItem>
    </>
  )
}

const CurrentPassFormik = withFormik({
  mapPropsToValues: () => ({
    currentPassword: '',
  }),

  validationSchema: CurrentPasswordSchema,

  handleSubmit: (values, { props, setSubmitting }) => {
    console.log(values)
    setSubmitting(true)
    props.onSubmit(values, () => setSubmitting(false))
    // setTimeout(() => {
    //   // alert(JSON.stringify(values, null, 2));
    //   setSubmitting(false)
    // }, 1000)
    // setStepOne(false)
    // setStepTwo(true)
  },

  displayName: 'CurrentPasswordForm',
})(CurrentPasswordForm)

export default CurrentPassFormik
