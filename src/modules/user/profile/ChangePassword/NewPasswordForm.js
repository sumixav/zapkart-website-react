import React from 'react'
import { withFormik } from 'formik'
import {  NewPasswordSchema } from '_utils/Schemas'
import Input from 'components/Input'
import FormItem from 'components/FormItem'
import Row from 'components/Row'
import * as Button from 'components/Button'


const NewPasswordForm = ({
    // active,
    handleChange,
    handleBlur,
    handleSubmit,
    values,
    errors,
    touched,
    isSubmitting,
  }) => {
    console.log(isSubmitting)
    // if (!active) return null
    return (
      <>
        <Row>
          <FormItem className="col-xl-6 col-lg-6">
            <Input
              placeholder="NewPassword"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.newPassword}
              name="newPassword"
              errors={errors.newPassword && touched.newPassword ? errors.newPassword : ''}
            />
          </FormItem>
          <FormItem className="col-xl-6 col-lg-6">
            <Input
              placeholder="ConfirmPassword"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.confirmPassword}
              name="confirmPassword"
              errors={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : ''}
            />
          </FormItem>
        </Row>
        <FormItem>
          <Button.SubmitButton onClick={handleSubmit} loading={isSubmitting}>
            Save
          </Button.SubmitButton>
        </FormItem>
      </>
    )
  }

  export const NewPassFormik = withFormik({
    mapPropsToValues: () => ({
      newPassword: '',
      confirmPassword: '',
    }),
  
    validationSchema: NewPasswordSchema,
  
    handleSubmit: (values, { props, setSubmitting }) => {
      setSubmitting(true)
      console.log(values)
      props.onSubmit(values, () => setSubmitting(false))
      // setTimeout(() => {
      //   // alert(JSON.stringify(values, null, 2));
      //   setSubmitting(false)
      //   // setActiveIndex('1')
      // }, 1000)
      // setStepOne(false)
      // setStepTwo(true)
    },
  
    displayName: 'NewPasswordForm',
  })(NewPasswordForm)

  export default NewPassFormik