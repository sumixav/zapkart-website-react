import React from 'react';
import { useGetInformation } from 'hooks/api';
import Loader from 'components/Loader';
import { createMarkup } from '_utils';
import HeaderWrapper from 'navigation/layouts/LayoutComponents/Header/HeaderWrapper';

const Index = props => {
  const { match } = props;
  const { params } = match;
  const { slug } = params;

  const { isLoading, data } = useGetInformation(slug);

  if (isLoading) return <Loader />;
  if (data && data.data) {
    const { data: infoData } = data;
    const headerState = {
      title: infoData.name,
      bread: [
        {
          url: '/home',
          text: 'Home',
        },
        {
          text: infoData.name,
        },
      ],
    };
    return (
      <>
        <HeaderWrapper headerState={headerState} />
        <div className="inner-page-wrapper">
          <div className="container">
            <div dangerouslySetInnerHTML={createMarkup(infoData.htmlContent)} />
          </div>
        </div>
      </>
    );
  }
  return null;
};

export default Index;
