import React, { Component } from 'react'
import PageSection from 'components/PageSection'

class ContactUs extends Component {
  render() {
    return (
      <PageSection>
        <div className="container">
          <div className="flex-wrapper contact-us-wrapper">
            <div className="contact-details-wrapper">
              <div className="contact-head">
                <h3>Having trouble using the kickill.com vouchers?</h3>
                <p>
                  E-mail support at supporvt@kickill.com <strong>or</strong>{' '}
                  <span>call (91) 9656011778 (during our business hours, Mon-Fri, 9am-6pm)</span>
                </p>
              </div>
              <div className="ownersec">
                <h3 className="ownersec-head">Are you a Business Owner?</h3>
                <p className="ownersec-para">
                  Check out our (Link) page and you will see how easy it is to start promoting your
                  business!
                </p>
              </div>

              <div className="contact-details">
                <div className="address-area">
                  <img src="/resources/images/kickill-location.png" className="lazy" alt="" />
                  <h4>Address:</h4>
                  <p>
                    Door no XVI/223 and 223A at Sabadeep, M.C.Road, Near Issac Maria Theatre,
                    Muvattupuzha.
                  </p>
                </div>
                <div className="phone-area">
                  <img src="/resources/images/kickill-phone.png" className="lazy" alt="" />
                  <p>
                    Land Phone No.: <a href="tel:04852836010">0485 283 60 10</a>
                  </p>
                  <p>
                    Mobile No.: <a href="tel:+919656011778">+91 96 56 011 778</a>
                  </p>
                </div>
              </div>

              <div className="contact-email">
                <p>
                  <span>
                    E-mail: <a href="mailto:kickillonline@gmail.com">kickillonline@gmail.com</a>{' '}
                  </span>{' '}
                  <span>Mobile App: kickill</span>
                  <img src="/resources/images/kickill-mail.png" className="lazy" alt="" />
                </p>
              </div>
            </div>

            <div className="contact-form-wrapper">
              <div className="contact-form">
                <div className="form-head">
                  <h4>
                    <img
                      className="lazy form-icon"
                      src="/resources/images/contact-head-iconmail.svg"
                      alt=""
                    />{' '}
                    {`Do you have any query don't hesitate to contact us.`}
                  </h4>
                </div>
                <form action="">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="form-group">
                        <input type="text" className="form-control" placeholder="First Name :" />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <input type="text" className="form-control" placeholder="Last Name :" />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <input type="text" className="form-control" placeholder="Email :" />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <input type="text" className="form-control" placeholder="Phone Number :" />
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <div className="form-group">
                        <textarea
                          name=""
                          id=""
                          cols="30"
                          rows="10"
                          className="form-control"
                          placeholder="Message :"
                        />
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <button type="submit" className="btn submit-btn">
                        Submit{' '}
                        <img
                          className="lazy submit-icon"
                          src="/resources/images/kickil-paper-plane.svg"
                          alt=""
                        />
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    )
  }
}

export default ContactUs
