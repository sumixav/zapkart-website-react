/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Skeleton from 'react-loading-skeleton';
import HomeBanner from 'components/HomeBanner';
import Product from 'components/Product';
import Blog from 'modules/blogs/components/Blog';
import { getProducts } from 'services/products';
import { getBlogsList } from 'services/blogs';
import { getBannersList } from 'services/banners';
import AdBanner from './components/AdBanner';
import ServicesInfo from './components/ServicesInfo';
import InfoBanner from './components/InfoBanner';
import InfoSteps from './components/InfoSteps';
import { homeActions } from './actions';
import CarouselProducts from './components/CarouselProducts';

const mapStateToProps = ({ home }) => {
  const {
    ads,
    adsLoading,
    adsError,
    infoBanner,
    infoLoading,
    infoBannerError,
    // popularProducts,
    // popularProdsLoading,
  } = home;
  console.log(ads, adsLoading, adsError);
  return {
    ads,
    adsError,
    adsLoading,
    infoBanner,
    infoLoading,
    infoBannerError,
    // popularProducts,
    // popularProdsLoading,
  };
};

const adImages = [
  {
    id: '1',
    url: '/resources/images/ads/kickill-ad-1.jpg',
  },
  {
    id: '2',
    url: '/resources/images/ads/kickill-ad-2.jpg',
  },
  {
    id: '3',
    url: '/resources/images/ads/kickill-ad-3.jpg',
  },
  {
    id: '4',
    url: '/resources/images/ads/kickill-ad-4.jpg',
  },
];

const infoSteps = [
  {
    id: '1',
    image: '/resources/images/steps/kickill-step-1.png',
    title: 'Search for your Medicines',
  },
  {
    id: '2',
    image: 'resources/images//steps/kickill-step-2.png',
    title: 'Add to Cart & Pay Or Pay Later',
  },
  {
    id: '3',
    image: 'resources/images//steps/kickill-step-3.png',
    title: 'Attach Prescriptions Wherever Needed',
  },
  {
    id: '4',
    image: 'resources/images//steps/kickill-step-4.png',
    title: 'Receive Order From Trusted Pharmacy',
  },
];

const cates = [
  {
    id: '5e4a3ed8815d3e7565b4fa26',
    slug: 'fitness-and-development',
  },
  {
    id: '5e4a3e1f815d3e7565b4fa1d',
    slug: 'winter-section',
  },
];

class Home extends Component {
  state = {
    popularProdsLoading: false,
    popularProducts: [],
    catTwoProdsLoading: false,
    featProdsLoading: false,
    featuredProducts: [],
    catTwoProds: [],
    blogDetails: [],
    bannerDetails: [],
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: homeActions.TEST_SAGA,
    });
    dispatch({
      type: homeActions.GET_ADS,
    });
    dispatch({
      type: homeActions.GET_INFO_BANNER_REQUEST,
    });
    dispatch({
      type: homeActions.GET_POPULAR_PRODUCTS,
    });
    // dispatch({
    //   type: blogActions.GET_DATA,
    // });

    // from services

    this.fetchProducts(
      {
        category: cates[0].id,
        fields: ['attributes', 'textDescription', 'pricing', 'images', 'slug', 'name'],
      },
      '1',
    ); // popular-health-products
    this.fetchProducts(
      {
        category: cates[1].id,
        fields: ['attributes', 'textDescription', 'pricing', 'images', 'slug', 'name'],
      },
      '2',
    ); // skin-care
    this.fetchProducts(
      {
        featured: true,
        fields: ['attributes', 'textDescription', 'pricing', 'images', 'slug', 'name'],
      },
      '3',
    ); // featured

    // this.fetchProducts({ category: cates[0].id }, '1'); // popular-health-products
    // this.fetchProducts({ category: cates[1].id }, '2'); // skin-care
    // this.fetchProducts({ featured: true }, '3'); // featured
    this.fetchBlog();
    this.fetchBanner();
  }

  fetchBlog = async () => {
    const blogs = await getBlogsList();
    if (blogs && blogs.length > 0) {
      this.setState({ blogDetails: blogs });
    }
  };

  fetchBanner = async () => {
    const banners = await getBannersList();
    if (banners && banners.length > 0) {
      this.setState({ bannerDetails: banners });
    }
  };

  fetchProducts = async (options, key) => {
    let loadingState = key === '1' ? 'popularProdsLoading' : 'catTwoProdsLoading';
    if (key === '3') loadingState = 'featProdsLoading';
    let prodState = key === '1' ? 'popularProducts' : 'catTwoProds';
    if (key === '3') prodState = 'featuredProducts';
    this.setState({ [loadingState]: true });
    const products = await getProducts(options);
    this.setState({ [loadingState]: false });
    if (products && products.length > 0) {
      this.setState({ [prodState]: products });
    }
  };

  onAdClick = id => {
    console.log('clicked on this ad', id);
  };

  render() {
    console.log(this.props);

    const {
      ads,
      adsLoading,
      // adsError,
      infoLoading,
      // infoBannerError,
      infoBanner,
      // popularProducts,
      // popularProdsLoading,
    } = this.props;

    const {
      popularProducts,
      popularProdsLoading,
      featProdsLoading,
      featuredProducts,
      catTwoProdsLoading,
      catTwoProds,
      blogDetails,
      bannerDetails,
    } = this.state;

    console.log('etreyrty', bannerDetails);
    console.log('popularProdsLoading', popularProdsLoading);
    console.log('popularProducts', popularProducts);
    // const title = 'Becadexamin Soft Gelatin Capsule';
    // const description = 'bottle of 30 soft gelatin capsules';
    // const price = '345';
    // const img = '/resources/images/products/kickill-product-7.png';

    return (
      <>
        <HomeBanner banner={bannerDetails} />
        <section className="bg-grey-lite">
          <div className="container">
            <div className="small-ad-wrapper">
              {adsLoading && ads.length === 0 && (
                <>
                  <AdBanner loading />
                  <AdBanner loading />
                </>
              )}
              {/* {adsError && <div className="ads-error">Error loading ads</div>} */}
              {ads &&
                ads.map(ad => (
                  <AdBanner
                    id={ad.id}
                    key={ad.id}
                    title={ad.title}
                    subtitle={ad.subtitle}
                    image={ad.image}
                    onClick={() => this.onAdClick(ad.id)}
                  />
                ))}
            </div>
          </div>
        </section>

        <section className="product-listing-wrapper wow fadeInUp">
          {featProdsLoading && (
            <>
              <div className="section-heading loading">
                {' '}
                <h2>
                  <Skeleton />
                </h2>
              </div>

              <div className="products-page-list-wrapper-loading">
                <Product key="1" loading />
                <Product key="2" loading />
                <Product key="3" loading />
                <Product key="4" loading />
                <Product key="5" loading />
                <Product key="6" loading />
                <Product key="7" loading />
              </div>
            </>
          )}
          <CarouselProducts
            products={featuredProducts}
            title="Featured Products"
            viewAllLink="/products/featured"
          >
            {featuredProducts &&
              featuredProducts.map(product => (
                <Product
                  item
                  slug={product.slug}
                  key={product.key}
                  id={product._id}
                  title={product.name}
                  description={product.textDescription}
                  price={product.pricing.salePrice}
                  img={product.images[0].thumbnail}
                />
              ))}
          </CarouselProducts>
        </section>

        <section className="bg-grey-lite">
          <div className="container">
            <div className="med-info-wrapper">
              {/* {infoLoading && infoBanner.length <= 0 && ( */}
              {infoLoading && (
                <>
                  <InfoBanner loading />
                  <InfoBanner loading />
                </>
              )}
              {/* {infoBannerError && <div>Error loading info...</div>} */}
              {infoBanner.length > 0 &&
                !infoLoading &&
                infoBanner.map(item => (
                  <InfoBanner
                    key={item.id}
                    title={item.title}
                    subtitle={item.subtitle}
                    image={item.image}
                  />
                ))}
            </div>
          </div>
        </section>

        <section className="product-listing-wrapper wow fadeInUp">
          {popularProdsLoading && (
            <>
              <div className="section-heading">
                <h2>
                  <span className="head">
                    <Skeleton />
                  </span>
                </h2>
              </div>
              <div className="products-page-list-wrapper-loading">
                <Product key="1" loading />
                <Product key="2" loading />
                <Product key="3" loading />
                <Product key="4" loading />
                <Product key="5" loading />
                <Product key="6" loading />
                <Product key="7" loading />
              </div>
            </>
          )}

          <CarouselProducts
            products={popularProducts}
            title="Fitness & Development"
            viewAllLink={`/products/category/${cates[0].id}`}
          >
            {popularProducts.map(product => (
              <Product
                item
                key={product.key}
                slug={product.slug}
                title={product.name}
                description={product.textDescription}
                price={product.pricing.salePrice}
                img={product.images[0].thumbnail}
              />
            ))}
          </CarouselProducts>
        </section>

        <section className="bg-grey-lite wow fadeInUp">
          <div className="container">
            <div className="four-ads-wrapper">
              {adImages.map(item => (
                <div key={item.id} className="ad-area wow fadeInUp">
                  <Link to="/">
                    <img className="lazy" src={item.url} alt="" />
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </section>

        {/* <Product title={title} description={description} price={price} img={img} /> */}

        <section className="wow fadeInUp">
          <InfoSteps steps={infoSteps} />
        </section>
        <section className="product-listing-wrapper bg-grey-lite skin-care wow fadeInUp">
          {catTwoProdsLoading && (
            <>
              <div className="section-heading">
                <h2>
                  <span className="head">
                    <Skeleton />
                  </span>
                </h2>
              </div>
              <div className="products-page-list-wrapper-loading">
                <Product key="1" loading />
                <Product key="2" loading />
                <Product key="3" loading />
                <Product key="4" loading />
                <Product key="5" loading />
                <Product key="6" loading />
                <Product key="7" loading />
              </div>
            </>
          )}
          <CarouselProducts
            title="Winter Section"
            viewAllLink={`/products/category/${cates[1].id}`}
          >
            {catTwoProds.map(product => (
              <Product
                slug={product.slug}
                item
                id={product._id}
                key={product.key}
                title={product.name}
                description={product.textDescription}
                price={product.pricing.salePrice}
                img={product.images[0].thumbnail}
              />
            ))}
          </CarouselProducts>
        </section>

        <section className="wow fadeInUp">
          <ServicesInfo />
        </section>

        <section className="product-listing-wrapper bg-grey-lite skin-care wow fadeInUp">
          {popularProdsLoading && (
            <div className="products-page-list-wrapper-loading">
              <Product key="1" />
              <Product key="2" />
              <Product key="3" />
            </div>
          )}
          <CarouselProducts
            blogs={blogDetails}
            title="Latest Articles"
            viewAllLink="/blogs/list/all"
          >
            {blogDetails.map(blog => (
              <Blog
                date={new Date(blog.createdAt).toDateString('d/m/y')}
                subtitle={blog.shortDescription}
                url={blog.url}
                slug={blog.slug}
                image={blog.images[0] && blog.images[0].thumbnail}
                item
              />
            ))}
          </CarouselProducts>
        </section>
      </>
    );
  }
}

export default connect(mapStateToProps)(Home);
