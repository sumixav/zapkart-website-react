import { all, put, call, delay, takeEvery } from 'redux-saga/effects';
import { getPopularProducts, getHomeAds, getInfoBanner } from 'services/home';
import { homeActions } from './actions';

export function* GET_ADS() {
  console.log('in home saga GET_ADS')
  yield put({
    type: homeActions.ADS_REQUEST,
  });
  yield delay(5000)
  const ads = yield call(getHomeAds);
  console.log(ads)
  if (ads) {
    yield put({
      type: homeActions.FETCH_ADS_SUCCESS,
      payload: ads
    })
  }
  else {
    yield put({
      type: homeActions.FETCH_ADS_ERROR,
      payload: {
        message: 'unable to fetch ads'
      }
    })
  }
  // return ads
}

export function* GET_POPULAR_PRODUCTS() {
  console.log('in home saga GET_POPULAR_PRODUCTS')
  yield put({
    type: homeActions.GET_POPULAR_PRODUCTS_REQUEST,
  });
  yield delay(4000)
  const popularProducts = yield call(getPopularProducts);
  console.log(popularProducts)
  if (popularProducts) {
    yield put({
      type: homeActions.GET_POPULAR_PRODUCTS_SUCCESS,
      payload: popularProducts
    })
  }
  else {
    yield put({
      type: homeActions.GET_POPULAR_PRODUCTS_ERROR,
      payload: {
        message: 'unable to fetch popular products'
      }
    })
  }

}
export function* GET_INFO_BANNERS() {
  console.log('in home saga GET_INFO_BANNER')
  yield delay(4000)
  const infoBanner = yield call(getInfoBanner);
  console.log(infoBanner)
  if (infoBanner) {
    yield put({
      type: homeActions.GET_INFO_BANNER_SUCCESS,
      payload: infoBanner
    })
  }
  else {
    yield put({
      type: homeActions.GET_INFO_BANNER_ERROR,
      payload: {
        message: 'unable to fetch info banners'
      }
    })
  }

}

export function TEST_SAGA() {
  console.log('in test saga')
}

export default function* rootSaga() {
  yield all([
    takeEvery(homeActions.GET_ADS, GET_ADS),
    takeEvery(homeActions.GET_POPULAR_PRODUCTS, GET_POPULAR_PRODUCTS),
    takeEvery(homeActions.GET_INFO_BANNER_REQUEST, GET_INFO_BANNERS),
    TEST_SAGA()
  ]);
}
