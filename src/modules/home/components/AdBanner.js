import React from 'react';
// import Button from 'components/Button';
import Skeleton from 'react-loading-skeleton';

const AdBanner = ({ image, title, subtitle, onClickAd, loading }) => {
  if (loading)
    return (
      <div className="small-ad wow">
        <div className="small-ad-image">
          <Skeleton width={100} height={58} />
        </div>
        <div className="small-ad-content">
          <h5>
            <Skeleton />
          </h5>
          <p>
            <Skeleton />
          </p>
        </div>
        <div className="order-btn">
          <div className="small-btn" style={{ color: 'transparent' }}>
            Order Now
          </div>
        </div>
      </div>
    );
  return (
    // <div className="small-ad wow fadeInUp">
    <div className="small-ad wow">
      <div className="small-ad-image">{image && <img className="lazy" src={image} alt="" />}</div>
      <div className="small-ad-content">
        <h5>{title}</h5>
        <p>{subtitle}</p>
      </div>
      {title && (
        <div className="order-btn">
          <button type="button" className="small-btn" onClick={onClickAd}>
            Order Now
          </button>
        </div>
      )}
    </div>
  );
};

export default AdBanner;
