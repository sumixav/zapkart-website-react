import React from 'react';

const info = [
  {
    id: '1',
    icon: 'resources/images/services/kickill-services-1.png',
    title: 'Every Medicine is Quality Tested<',
    subtitle: 'Before It Reaches You',
  },
  {
    id: '1',
    icon: 'resources/images/services/kickill-services-2.png',
    title: 'Best Deals and Discounts',
    subtitle: 'Zapkart will provide you',
  },
  {
    id: '1',
    icon: 'resources/images/services/kickill-services-3.png',
    title: 'Safety Pay',
    subtitle: '100% MoneyBack Guarantee & 7 Days Return Policy',
  },
  {
    id: '1',
    icon: 'resources/images/services/kickill-services-4.png',
    title: '100% Secure Payments',
    subtitle: 'Guaranteed 100% protection',
  },
  {
    id: '1',
    icon: 'resources/images/services/kickill-services-5.png',
    title: 'Cash On Delivery',
    subtitle: 'Pay when you get it delivered',
  },
];

const ServicesInfo = () => {
  return (
    <div className="container">
      <div className="services-wrapper">
        {info.map(item => (
          <div className="service">
            <div className="service-img">
              <img className="lazy" src={item.icon} alt="" />
            </div>
            <div className="service-cont">
              <h5>{item.title}</h5>
              <p>{item.subtitle}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ServicesInfo;
