import React from 'react';
import PropTypes from 'prop-types';

const InfoSteps = ({ steps }) => {
  return (
    <div className="container">
      <div className="steps-wrapper">
        {steps.map(item => (
          <div className="steps wow fadeInUp">
            <div className="step-img">
              <img className="lazy" src={item.image} alt="" />{' '}
            </div>
            <div className="step-content">{item.title}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

InfoSteps.propTypes = {
  steps: PropTypes.shape({
    image: PropTypes.string.isRequired, // try 'url-prop-type' package
    title: PropTypes.string.isRequired,
  }).isRequired,
};

export default InfoSteps;
