import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Skeleton from 'react-loading-skeleton';
const linkStyle = { color: '#333' };

const Blog = props  => {
  const { date, title, subtitle, url,slug, src_list } = props;

  const getLinked = children => {
    return (
      <Link to={`/blogs/${slug}`} style={linkStyle}>
        {children}
      </Link>
    );
  };
  let { image = '' } = props;
  if (image && !image.startsWith('/')) image = `/${image}`;
  return (
    <div className="blogs-main">
      <div className="blog">
        <div className="blog-img">
        {getLinked(
        <>
          {image ?<img className="lazy" src={image} alt={src_list} />: <Skeleton />}
          <span className="blog-date">{date}</span>
          </>
      )}
        </div>
        
        <div className="blog-content">
          <h4>{title}</h4>
          <p>{subtitle}</p>
          <div className="more-btn">
            <Link to={url} className="small-btn">
              Read More
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

Blog.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  // date: PropTypes.instanceOf(Date).isRequired,
  date: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default Blog;
