import React from 'react'

const BlogCommentForm = ({onSubmit}) => {
  return (
    <div className="blog-review">
      <h5>Leave a Reply</h5>
      <div className="review-form">
        <div className="row">
          <div className="col-xl-6 col-lg-6">
            <div className="form-group">
              <input type="text" className="form-control" placeholder="Name :" />
            </div>
            <div className="form-group">
              <input type="text" className="form-control" placeholder="Email :" />
            </div>
          </div>

          <div className="col-xl-6 col-lg-6">
            <textarea
              name=""
              id=""
              cols="30"
              rows="10"
              className="form-control"
              placeholder="Enter your comment"
            />
          </div>

          <div className="col-xl-12 col-lg-12">
            <div className="post-btn-wrapper">
              <button type="button" onClick={onSubmit} className="small-btn">
                Post Your Comments
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default BlogCommentForm
