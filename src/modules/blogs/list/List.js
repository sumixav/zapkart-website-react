/* eslint-disable no-underscore-dangle */
import React from 'react';
import Blog from 'modules/blogs/components/Blog';

const List = props => {
  // console.log(props);
  const { data, title } = props;
  console.log(props);
  return (
    <div className="products-list-wrapper">
      <div className="product-top-ad-wrapper">
        <img src="/resources/images/kickill-product-ad1.jpg" alt="" />
      </div>

      {title && (
        <div className="products-head">
          <h1>
            <span>{title}</span>
          </h1>
          <div className="breadcrumbs-wrapper">
            <ul>
              <li>
                <a href="/#">Home</a>
              </li>
              <li>/</li>
              <li>Blog</li>
            </ul>
          </div>
        </div>
      )}

      <div className="products-page-list-wrapper">
        {data.map(i => {
          return (
            <Blog
            date={new Date(i.createdAt).toDateString("d/m/y")}
            subtitle={i.shortDescription}
            url={i.url}
            slug={i.slug}
            image={i.images[0] && i.images[0].thumbnail}
            src_list = {i.imgtext_data && i.imgtext_data}
            item
          />
          );
        })}
      </div>
    </div>
  );
};

export default List;
