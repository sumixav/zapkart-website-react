import React from 'react';
import PageSection from 'components/PageSection';
import { useBlogs } from 'hooks/api';
import ManageLoadingStates from 'components/ManageLoadingStates';
import Filter from 'modules/product/list/Filter';
import List from './List';

const Index = props => {
  // const searchStr = '';
  console.log('PRODUCT LIST PROPS', props);
  const { match, location } = props;
  const { params } = match;
  const { type, id } = params;

  const { state } = location;

  let title = null;
  if (state && state.title) title = state.title;

  React.useEffect(() => {
    console.log('title changes', title);
  }, [title]);

  let options = {};
  if (type && id) options = { [type]: id };
  if (type && !id) options = { [type]: true };
  console.log('product fetch options', options);
  const { data, isLoading, isFetching } = useBlogs(options);
  console.log(data, isLoading, isFetching);

  const renderList = React.useCallback(
    filteredData => {
      return <List data={filteredData} type={type} title={title || null} />;
    },
    [title, type],
  );

  return (
    <PageSection>
      <div className="container">
        <div className="flex-wrapper products-wrapper">
          <ManageLoadingStates
            isLoading={isLoading}
            isFetching={isFetching}
            data={data ? data.data : null}
          >
            <Filter data={data ? data.data : null} render={renderList} />
          </ManageLoadingStates>
          {/* {isLoading && <div>Loading...</div>}
          {data &&
            (isFetching ? (
              <div>Refreshing...</div>
            ) : (
              <Filter data={data}>
                {filteredData => {
                  console.log(filteredData);
                  return <List data={filteredData} />;
                }}
              </Filter>
            ))}
          {!isLoading && !data && !isFetching && <div>No data available</div>} */}
        </div>
      </div>
    </PageSection>
  );
};

export default Index;
