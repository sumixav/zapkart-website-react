import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReactHtmlParser from 'react-html-parser'
import { blogDetailsActions as actions } from './actions'
import BlogCommentForm from '../components/BlogCommentForm'

class BlogDetails extends Component {
  componentDidMount() {
    const {
      match: {
        params: { id: idStr },
      },
      dispatch,
    } = this.props
    // const { params } = match
    console.log(typeof idStr)
    dispatch({
      type: actions.GET_DATA,
      payload: {
        id: idStr,
      },
    })
  }

  handleSubmitForm = () => {
    console.log('will submit comment')
  }

  render() {
    const { blog } = this.props
    console.log(blog)
    const images = blog.images && blog.images[0] && blog.images[0].thumbnail
    return (
      <div className="col-xl-9 col-lg-9">
        <div className="row">
          {/* Single Blog */}
          <div className="col-xl-12 blog-details-page">
            <div className="blogs-main">
              <div className="blog">
                <div
                  className="blog-img"
                  style={{
                    backgroundSize: `cover`,
                    backgroundRepeat: `no-repeat`,
                    backgroundPosition: `50% 60%`,
                    backgroundImage: `url(/${images})`,
                  }}
                >
                  <span className="blog-date">{new Date(blog.createdAt).toDateString("d/m/y")}</span>
                </div>
                <div className="blog-content">
                  <h2 className="blog-detail-head">{blog.name}</h2>
                  {/* <h4 className="blog-detail-sub-head">
                    <strong>Tags: </strong>hairloss ,ayurvedha ,treatments ,remedy ,homeremedy
                    ,causes
                  </h4> */}
                  {ReactHtmlParser(blog.htmlContent)}
                </div>

                {/* <div className="blog-review-form-wrapper"> */}
                <BlogCommentForm onSubmit={this.handleSubmitForm} />
              </div>
            </div>
          </div>
          {/* Single Blog */}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ blog: blogDetails }) =>
  // ownProps
  {
    const { blog, fetchError, loading } = blogDetails
    return {
      blog,
      fetchError,
      loading,
    }
  }

export default connect(mapStateToProps)(BlogDetails)
