/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
// import { Transition } from 'react-spring/renderprops';
import PageSection from 'components/PageSection';
import _ from 'lodash';
import { getProductsFromIds } from 'services/products';
import { cartActions } from 'redux/actions';
import Loader from 'components/Loader';
import CartItem from './components/CartItem';
import CartPricing from './components/CartPricing';
import EmptyCart from './components/EmptyCart';
// import data from './data.json';

class Cart extends Component {
  state = {
    // products: data.products,
    products: [],
    total: 0,
    discount: 0,
    shippingCost: 0,
    cartAmount: 0,
    loading: false,
  };

  componentDidMount() {
    this.updateProducts();

    // const total = data.products.reduce((sum, item) => sum + item.originalPrice * item.qty, 0);
    const { products } = this.state;
    if (products && products.length > 0) {
      console.log('products', products);
      const total = products.reduce((sum, item) => sum + item.pricing.listPrice * item.qty, 0);
      const discount = products.reduce(
        (sum, item) => sum + (item.pricing.listPrice - item.pricing.salePrice) * item.qty,
        0,
      );
      const shippingCost = 75;
      const cartAmount = total + shippingCost - discount;
      this.setState({
        total,
        discount,
        shippingCost,
        cartAmount,
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate');
    console.log('prevProps', prevProps);
    console.log('prevState', prevState);
    const { cart: prevCart } = prevProps;
    const { cart: currentCart } = this.props;
    if (prevCart !== currentCart) this.updateProducts(); // if path change too, update (TO DO)
  }

  //   componentDidUpdate(prevProps, prevState) {
  //     console.log(prevProps, prevState)
  //     const { products : prevProducts } = prevState
  //     const { products } = this.state
  //     console.log(prevProducts, products)
  //     if (prevProducts !== products) this.getPricing()
  //   }
  updateProducts = async () => {
    const { cart } = this.props;
    console.log('in updateProducts', cart);
    const cartProds = cart.products;
    if (cartProds.length === 0) {
      this.setState({ products: [] });
      return;
    }
    const ids = cartProds.map(i => i.id);
    // this.setState({
    //   loading: true,
    // });
    const fetchedProds = await getProductsFromIds(ids);
    if (fetchedProds && fetchedProds.length > 0)
      this.setState({
        products: fetchedProds,
      });
    // this.setState({
    //   loading: false,
    // });

    // const { cart } = this.props;
    if (cart && cart.products) {
      fetchedProds.forEach(i => {
        const cartProdIndex = cart.products.findIndex(a => a.id === i._id);
        if (cartProdIndex !== -1) i.qty = cart.products[cartProdIndex].qty;
        i.id = i._id;
        i.salePrice = i.pricing.salePrice;
        i.listPrice = i.pricing.listPrice;
      });
      this.setState({ products: fetchedProds });
    }
  };

  onRemove = id => {
    console.log('in onRemove', id);
    // console.log(id);
    const { dispatch } = this.props;

    dispatch({
      type: cartActions.REMOVE_FROM_CART,
      payload: { productId: id },
    });
    // this.updateProducts();
    // this.setState(
    //   prevState => {
    //     const { products } = prevState;
    //     const filteredProducts = products.filter(item => item.id !== id);
    //     return {
    //       products: filteredProducts,
    //     };
    //   },
    //   () => this.getPricing(),
    // );
  };

  onIncQuantity = id => {
    const { dispatch } = this.props;
    const { products } = this.state;
    const selectedProc = _.find(products, { _id: id });
    if (selectedProc)
      dispatch({
        type: cartActions.INC_QTY,
        payload: {
          productId: id,
          maxOrderQty: selectedProc.maxOrderQty,
        },
      });
  };
  // onIncQuantity = id => {
  //   this.setState(
  //     prevState => {
  //       const updatedProducts = [...prevState.products];
  //       const product = updatedProducts.find(item => item.id === id);
  //       if (product.qty < product.maxOrderQrt) product.qty += 1;
  //       return {
  //         products: updatedProducts,
  //       };
  //     },
  //     () => this.getPricing(),
  //   );
  // };

  onDecQuantity = id => {
    const { dispatch } = this.props;
    const { products } = this.state;
    const selectedProc = _.find(products, { _id: id });
    if (selectedProc)
      dispatch({
        type: cartActions.DEC_QTY,
        payload: {
          productId: id,
          minOrderQty: selectedProc.minOrderQty,
        },
      });
  };
  // onDecQuantity = id => {
  //   this.setState(
  //     prevState => {
  //       const updatedProducts = [...prevState.products];
  //       const product = updatedProducts.find(item => item.id === id);
  //       if (product.qty > 1) product.qty -= 1;
  //       return {
  //         products: updatedProducts,
  //       };
  //     },
  //     () => this.getPricing(),
  //   );
  // };

  getPricing = () => {
    const { products: a } = this.state;
    console.log(a);
    this.setState(prevState => {
      console.log(prevState.products);
      const { total, discount, shippingCost, products } = prevState;
      console.log(total, discount, shippingCost);
      const totalM = products.reduce((sum, item) => sum + item.originalPrice * item.qty, 0);
      const discountM = products.reduce(
        (sum, item) => sum + (item.originalPrice - item.offerPrice) * item.qty,
        0,
      );
      const shippingCostM = products.length <= 0 ? 0 : shippingCost;
      const cartAmountM = totalM + shippingCostM - discountM;
      console.log(totalM, discountM, cartAmountM);
      return {
        total: totalM,
        discount: discountM,
        cartAmount: cartAmountM,
        shippingCost: shippingCostM,
      };
    });
  };

  render() {
    const { total, cartAmount, shippingCost, discount, removing, products, loading } = this.state;
    console.log(products);
    // const cartProducts = products.map(item => (
    //   <CartItem
    //     id={item.id}
    //     title={item.title}
    //     image={item.image}
    //     description={item.description}
    //     offerPrice={item.offerPrice}
    //     originalPrice={item.originalPrice}
    //     qty={item.qty}
    //     key={item.id}
    //     onRemove={this.onRemove}
    //     onIncQuantity={this.onIncQuantity}
    //     onDecQuantity={this.onDecQuantity}
    //     remove={removing}
    //   />
    // ))

    if (loading) return <Loader />;

    return (
      <PageSection className="blogs-wrapper">
        <div className="inner-container">
          {products && products.length > 0 && (
            <div className="row space-evenly">
              <div className="cart-items-wrapper">
                {products.map(item => (
                  <CartItem
                    id={item.id}
                    key={item.id}
                    slug={item.slug}
                    title={item.name}
                    image={item.images ? item.images[0].url : null}
                    description={item.textDescription}
                    offerPrice={item.salePrice}
                    originalPrice={item.listPrice}
                    qty={item.qty}
                    onRemove={this.onRemove}
                    onIncQuantity={this.onIncQuantity}
                    onDecQuantity={this.onDecQuantity}
                    remove={removing}
                  />
                ))}
              </div>

              <div className="cart-price-details">
                <div className="delivery-checking">
                  <p>Your delivery location</p>
                  <div className="pin-check">
                    <input type="text" placeholder="680519" className="pin-check-input" />
                  </div>
                  <div className="change-loc">
                    <a href="/#">Change Location?</a>
                  </div>
                </div>

                <CartPricing
                  total={total}
                  discount={discount}
                  shippingCost={shippingCost}
                  cartAmount={cartAmount}
                />

                <div className="continue-place-order">
                  <Link to="/home" className="big-btn continue-btn ease1s">
                    <i className="fas fa-chevron-left" /> Continue Shopping
                  </Link>
                  <Link
                    to="/checkout"
                    type="button"
                    className="big-btn placeorder-btn ease1s"
                    disabled={products.length <= 0}
                  >
                    Place Order <i className="fas fa-chevron-right" />
                  </Link>
                </div>
              </div>
            </div>
          )}
          {products && products.length === 0 && <EmptyCart />}
        </div>
      </PageSection>
    );
  }
}

export default connect(({ cart }) => ({ cart }))(Cart);
