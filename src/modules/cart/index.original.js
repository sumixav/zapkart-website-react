import React, { Component } from 'react';
// import { Transition } from 'react-spring/renderprops';
import PageSection from 'components/PageSection';
import CartItem from './components/CartItem';
import CartPricing from './components/CartPricing';
import data from './data.json';

export default class Cart extends Component {
  state = {
    products: data.products,
    total: 0,
    discount: 0,
    shippingCost: 0,
    cartAmount: 0,
  };

  componentDidMount() {
    const total = data.products.reduce((sum, item) => sum + item.originalPrice * item.qty, 0);
    const discount = data.products.reduce(
      (sum, item) => sum + (item.originalPrice - item.offerPrice) * item.qty,
      0,
    );
    const shippingCost = 75;
    const cartAmount = total + shippingCost - discount;
    this.setState({
      total,
      discount,
      shippingCost,
      cartAmount,
    });
  }

  //   componentDidUpdate(prevProps, prevState) {
  //     console.log(prevProps, prevState)
  //     const { products : prevProducts } = prevState
  //     const { products } = this.state
  //     console.log(prevProducts, products)
  //     if (prevProducts !== products) this.getPricing()
  //   }

  onRemove = id => {
    console.log('in onRemove');
    console.log(id);

    this.setState(
      prevState => {
        const { products } = prevState;
        const filteredProducts = products.filter(item => item.id !== id);
        return {
          products: filteredProducts,
        };
      },
      () => this.getPricing(),
    );
  };

  onIncQuantity = id => {
    this.setState(
      prevState => {
        const updatedProducts = [...prevState.products];
        const product = updatedProducts.find(item => item.id === id);
        if (product.qty < product.maxPermittedQty) product.qty += 1;
        return {
          products: updatedProducts,
        };
      },
      () => this.getPricing(),
    );
  };

  onDecQuantity = id => {
    this.setState(
      prevState => {
        const updatedProducts = [...prevState.products];
        const product = updatedProducts.find(item => item.id === id);
        if (product.qty > 1) product.qty -= 1;
        return {
          products: updatedProducts,
        };
      },
      () => this.getPricing(),
    );
  };

  getPricing = () => {
    const { products: a } = this.state;
    console.log(a);
    this.setState(prevState => {
      console.log(prevState.products);
      const { total, discount, shippingCost, products } = prevState;
      console.log(total, discount, shippingCost);
      const totalM = products.reduce((sum, item) => sum + item.originalPrice * item.qty, 0);
      const discountM = products.reduce(
        (sum, item) => sum + (item.originalPrice - item.offerPrice) * item.qty,
        0,
      );
      const shippingCostM = products.length <= 0 ? 0 : shippingCost;
      const cartAmountM = totalM + shippingCostM - discountM;
      console.log(totalM, discountM, cartAmountM);
      return {
        total: totalM,
        discount: discountM,
        cartAmount: cartAmountM,
        shippingCost: shippingCostM,
      };
    });
  };

  render() {
    const { products, total, cartAmount, shippingCost, discount, removing } = this.state;
    console.log(products.length);
    // const cartProducts = products.map(item => (
    //   <CartItem
    //     id={item.id}
    //     title={item.title}
    //     image={item.image}
    //     description={item.description}
    //     offerPrice={item.offerPrice}
    //     originalPrice={item.originalPrice}
    //     qty={item.qty}
    //     key={item.id}
    //     onRemove={this.onRemove}
    //     onIncQuantity={this.onIncQuantity}
    //     onDecQuantity={this.onDecQuantity}
    //     remove={removing}
    //   />
    // ))
    return (
      <PageSection className="blogs-wrapper">
        <div className="inner-container">
          <div className="row space-between">
            <div className="cart-items-wrapper">
              {products &&
                products.length > 0 &&
                products.map(item => (
                  <CartItem
                    id={item.id}
                    title={item.title}
                    image={item.image}
                    description={item.description}
                    offerPrice={item.offerPrice}
                    originalPrice={item.originalPrice}
                    qty={item.qty}
                    key={item.id}
                    onRemove={this.onRemove}
                    onIncQuantity={this.onIncQuantity}
                    onDecQuantity={this.onDecQuantity}
                    remove={removing}
                  />
                ))}
            </div>

            <div className="cart-price-details">
              <div className="delivery-checking">
                <p>Your delivery location</p>
                <div className="pin-check">
                  <input type="text" placeholder="680519" className="pin-check-input" />
                </div>
                <div className="change-loc">
                  <a href="/#"">Change Location?</a>
                </div>
              </div>

              <CartPricing
                total={total}
                discount={discount}
                shippingCost={shippingCost}
                cartAmount={cartAmount}
              />

              <div className="continue-place-order">
                <a href="/#"" className="big-btn continue-btn ease1s">
                  <i className="fas fa-chevron-left" /> Continue Shopping
                </a>
                <button
                  type="button"
                  className="big-btn placeorder-btn ease1s"
                  disabled={products.length <= 0}
                >
                  Place Order <i className="fas fa-chevron-right" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    );
  }
}
