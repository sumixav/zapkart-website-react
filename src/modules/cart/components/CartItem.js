import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class CartItem extends Component {
  render() {
    const {
      image,
      title,
      slug,
      description,
      offerPrice,
      originalPrice,
      qty,
      id,
      onRemove,
      remove,
      onIncQuantity,
      onDecQuantity,
    } = this.props;
    console.log(remove);

    const percentOff = Math.round(((originalPrice - offerPrice) / originalPrice) * 100);

    return (
      <div className="cart-item">
        <Link className="cart-item-img" to={`/product/${slug}`}>
          <img src={image} alt="" className="product-image lazy" />
        </Link>
        <div className="cart-item-details">
          <Link to={`/product/${slug}`}>
            <h5 className="product-name">{title}</h5>
            <h6 className="product-discription">{description}</h6>
          </Link>
          <div className="qty-wrapper">
            <button type="button" className="qty-minus" onClick={() => onDecQuantity(id)}>
              -
            </button>
            <div className="qty-value">{qty}</div>
            <button type="button" className="qty-plus" onClick={() => onIncQuantity(id)}>
              +
            </button>
          </div>
        </div>
        <div className="cart-item-price-wrapper">
          <div className="remove-item">
            <button type="button" onClick={() => onRemove(id)} className="remove">
              <img className="lazy small-trash" src="/resources/images/kickill-trash.svg" alt="" />
              Remove
            </button>
          </div>
          <div className="cart-item-price">
            <h3 className="original-price">
              <i className="fas fa-rupee-sign" /> {offerPrice}/-
            </h3>
            {originalPrice !== offerPrice && (
              <h4 className="offer-price">
                <i className="fas fa-rupee-sign" /> {originalPrice}/-
              </h4>
            )}
          </div>
          <div className="cart-item-offer">
            {percentOff > 0 && <strong>{percentOff}% Off</strong>} 1 Offer Available
          </div>
        </div>
      </div>
    );
  }
}

CartItem.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  offerPrice: PropTypes.number,
  originalPrice: PropTypes.number,
  qty: PropTypes.number,
  onRemove: PropTypes.func.isRequired,
  onIncQuantity: PropTypes.func.isRequired,
  onDecQuantity: PropTypes.func.isRequired,
};

CartItem.defaultProps = {
  qty: 1,
  image: '',
  title: '',
  description: '',
  offerPrice: 0,
  originalPrice: 0,
};

export default CartItem;
