export const api = {
    login : '/api/backend/v1/users/login',
    getUser : '/api/users/getUser',
    signup : '/api/backend/v1/users/login',
    forgotPassword : '/api/users/forgotPassword',
    getProducts : '/api/products'
}

export const CATALOG_API = {
    login : '/api/backend/v1/users/login',
    getUser : '/api/backend/v1/users',
    signup : '/api/backend/v1/users/register',
    getProducts : '/api/catalog/v1/products',
    getVariants: '/api/catalog/v1/products/variants',
    getBrands:'/api/catalog/v1/brands',
    getInformations:'/api/catalog/v1/informations',
    getCategories:'/api/catalog/v1/category',
    getBlogs:'/api/catalog/v1/blog',
    getBanners:'/api/catalog/v1/banner',
    fblogin: '/api/backend/v1/users/fblogin',
    gblogin: '/api/backend/v1/users/gblogin',
}