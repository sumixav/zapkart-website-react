/* eslint-disable no-underscore-dangle */
import { routes } from 'navigation/router';
// import LTT from 'list-to-tree';

export const isRouteWithHeader = url => {
  //
  console.log('in findRoute', url);
  const route = routes.find(item => item.path === url);
  if (route) {
    if (route.headerState) {
      return true;
    }
  }
  return false;
};

export const lazyFunction = (f, ...args) => {
  return () => {
    return f.apply(this, args);
  };
};

export const createMarkup = htmlContent => {
  return { __html: htmlContent };
};

export const generateCategoryTree = categories => {
  const categoriesFormatted = categories.map(item => {
    return {
      title: item.name,
      value: item._id,
      key: item._id,
      id: item._id,
      parent: item.parent === null ? 0 : item.parent,
    };
  });
  // console.log(categoriesFormatted);
  const tree = unflatten(categoriesFormatted);
  // const ltt = new LTT(categoriesFormatted, {
  //   key_id: 'id',
  //   key_parent: 'parent',
  // });
  // const tree = ltt.GetTree();
  // console.log(tree);
  return tree;
};

export const unflatten = (array, parent, tree) => {
  tree = typeof tree !== 'undefined' ? tree : [];
  parent = typeof parent !== 'undefined' ? parent : { id: 0 };
  // console.log('unflatten', array)
  const children = array.filter(child => {
    return child.parent === parent.id;
  });
  console.log(children.length);
  if (children.length !== 0) {
    if (parent.id === 0) {
      tree = children;
    } else {
      parent.children = children;
    }
    children.forEach(child => {
      unflatten(array, child);
    });
  }

  return tree;
};

export const getHeaderState = url => {
  console.log('in getHeaderState');
  const route = routes.find(item => item.path === url);
  console.log(route);
  if (route && route.headerState) {
    console.log(route.headerState);
    return route.headerState;
  }
  return null;
};

export const printResponse = response => {
  console.log(response);
  console.log(response.status);
  console.log(response.statusText);
  console.log(response.data);
};

export const printError = error => {
  console.log(error.response);
  console.log(error.request);
  console.log(error.message);
};

export const isErrorResponse = () => {
  return null;
};
export const getData = () => {};
export const getErrorMessage = () => {};

export const validateUserProfile = (values, isEdit, isPassword) => {
  console.log(values);
  const errors = {};
  const keys = Object.entries(values);
  const formValues = keys.filter(
    ([key]) => key !== 'currentPassword' && key !== 'newPassword' && key !== 'confirmPassword',
  );
  const passwordValues = keys.filter(
    ([key]) => key === 'currentPassword' || key === 'newPassword' || key === 'confirmPassword',
  );

  if (isEdit) {
    formValues.forEach(([key, value]) => {
      // if (key !== 'currentPassword' && key !== 'newPassword' && key !== 'confirmPassword')
      if (!value) errors[key] = 'Required';
    });
    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
      errors.email = 'Invalid email address';
    }
  }
  if (isPassword) {
    console.log('validating password');
    const passRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    passwordValues.forEach(([key, value]) => {
      // if (key !== 'currentPassword' && key !== 'newPassword' && key !== 'confirmPassword')
      if (!value) errors[key] = 'Required';
    });
    if (values.newPassword && !values.newPassword.match(passRegEx)) {
      errors.newPassword =
        'Password must be between 6 to 20 characters with at least one numeric digit, one uppercase and one lowercase letter';
    } else if (
      values.newPassword &&
      values.newPassword.match(passRegEx) &&
      values.newPassword !== values.confirmPassword
    ) {
      errors.confirmPassword = 'Passwords do not match';
    }
  }

  return errors;
};
