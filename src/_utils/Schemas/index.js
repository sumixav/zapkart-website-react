import * as Yup from 'yup';

export const regExMobNo = /[6-9]\d{9}$/;
export const regExPincode = /^[1-9][0-9]{5}$/;
// export const regExPassword = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
const passRegEx = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

Yup.addMethod(Yup.string, 'phone', function() {
  return this.test('phone', 'Phone number is not valid', value => regExMobNo.test(value));
});

Yup.addMethod(Yup.string, 'pincode', function() {
  return this.test('pincode', 'Enter valid Pincode', value => regExPincode.test(value));
});

Yup.addMethod(Yup.string, 'password', function() {
  return this.test(
    'password',
    'Password must be between 6 to 20 characters with at least one numeric digit, one uppercase and one lowercase letter',
    value => passRegEx.test(value),
  );
});

export const UserProfileSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .max(10, 'Too Long!')
    .required('Required'),
  lastName: Yup.string()
    .min(2, 'Too Short!')
    .max(10, 'Too Long!')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  phoneNo: Yup.string()
    .required('Required!')
    .phone('Phone number is not valid'),
});

export const UserAddressSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  addressType: Yup.string().required('Required'),
  address: Yup.string().required('Required'),
  pincode: Yup.string()
    .required('Required')
    .pincode(),
  phoneNo: Yup.string()
    .required('Required!')
    .phone('Phone number is not valid'),
});

export const CurrentPasswordSchema = Yup.object().shape({
  currentPassword: Yup.string().required('Please enter current password'),
  // .password(),
});

export const NewPasswordSchema = Yup.object().shape({
  newPassword: Yup.string().required('Required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('newPassword')], 'Passwords do not match')
    .required('Required'),
});

export const AccountDetailsSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  mobileNo: Yup.string()
    .required('Required!')
    .phone('Phone number is not valid'),
  loginEmail: Yup.string().email('Invalid email'),
  loginPassword: Yup.string(),
});

export const RegisterSchema = Yup.object().shape({
  lastName: Yup.string().required('Required'),
  firstName: Yup.string().required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  mobileNo: Yup.string()
    .required('Required')
    .phone('Phone number is not valid'),
  password: Yup.string()
    .password(
      'Password must be between 6 to 20 characters with at least one numeric digit, one uppercase and one lowercase letter',
    )
    .required('Required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Required'),
});

export const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  password: Yup.string().required('Required'),
});

export const CheckoutShipAddrSchema = Yup.object().shape({
  firstName: Yup.string().required('Required'),
  lastName: Yup.string().required('Required'),
  addressTwo: Yup.string(),
  addressOne: Yup.string().required('Required'),
  state: Yup.string().required('Required'),
  city: Yup.string().required('Required'),
  country: Yup.string().required('Required'),
  pincode: Yup.string()
    .required('Required')
    .pincode(),
  phoneNo: Yup.string()
    .required('Required!')
    .phone('Phone number is not valid'),
});
