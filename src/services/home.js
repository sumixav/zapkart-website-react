export async function getHomeAds() {
  console.log ('in getHomeAds service');
  return [
    {
      id: '1',
      image: '/resources/images/kickill-small-ad-image.jpg',
      title: 'Cut your diabetes expenses by half',
      subtitle: 'Avail exclusive offers and discounts',
    },
    {
      id: '2',
      image: '/resources/images/kickill-small-ad-image.jpg',
      title: '30% Super Cashback',
      subtitle: 'Avail exclusive offers and discounts',
    },
  ];
}

export async function getInfoBanner() {
  console.log ('in getInfBanner service');
  return [
    {
      id: '1',
      image: '/resources/images/kickill-med-info.svg',
      title: 'Find comprehensive information',
      subtitle: 'About the medicines you take',
    },
    {
      id: '2',
      image: '/resources/images/kickill-med-info2.png',
      title: 'Find Generic Sustitutes',
      subtitle: 'That are affordable and safe',
    },
  ];
}

export async function getPopularProducts() {
  console.log('in getPopularProducts service');
  return [
    {
      id:"2",
      image:"/resources/images/products/kickill-product-5.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"3",
      image:"/resources/images/products/kickill-product-6.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"4",
      image:"/resources/images/products/kickill-product-7.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"5",
      image:"/resources/images/products/kickill-product-1.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"6",
      image:"/resources/images/products/kickill-product-2.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"7",
      image:"/resources/images/products/kickill-product-3.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"8",
      image:"/resources/images/products/kickill-product-4.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"9",
      image:"/resources/images/products/kickill-product-5.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"10",
      image:"/resources/images/products/kickill-product-6.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"12",
      image:"/resources/images/products/kickill-product-7.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"11",
      image:"/resources/images/products/kickill-product-1.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
    {
      id:"1",
      image:"/resources/images/products/kickill-product-4.png",
      name:"Becadexamin Soft Gelatin Capsule",
      description:"bottle of 30 soft gelatin capsules",
      price:"3450"
    },
  ]
}
