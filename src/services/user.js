// import decode from 'jwt-decode';

import toaster from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import store from 'store';
import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants/api';

export async function register(data) {
  const { email, firstName, lastName, password, phoneNo: phone } = data;
  const formData = new FormData();
  formData.append('email', email);
  formData.append('firstName', firstName);
  formData.append('lastName', lastName);
  formData.append('password', password);
  formData.append('phone', phone);
  formData.append('roleId', 2);
  const request = {
    method: 'POST',
    body: formData,
  };
  try {
    const a = await callApi(CATALOG_API.signup, request);
    if (a && a.user) return true;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message || 'Error logging in!');
  }
  return false;
}

export async function login(email, password) {
  console.log('login', email, password);
  const formData = new FormData();
  formData.append('loginId', email);
  formData.append('password', password);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const response = await callApi(CATALOG_API.login, options);
    if (response && response.token) {
      setToken(response.token);
      return true;
    }
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
  }
  return false;
}

export async function fblogin(email, id,name) {
  console.log('login', email, id,name);
  const formData = new FormData();
  formData.append('loginId', email);
  formData.append('id', id);
  formData.append('name', name);
  formData.append('roleId', 2);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const response = await callApi(CATALOG_API.fblogin, options);
    if (response && response.token) {
      setToken(response.token);
      return true;
    }
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
  }
  return false;
}

export async function gblogin(email, id,name) {
  console.log('login', email, id,name);
  const formData = new FormData();
  formData.append('loginId', email);
  formData.append('id', id);
  formData.append('name', name);
  formData.append('roleId', 2);
  const options = {
    method: 'POST',
    body: formData,
  };
  try {
    const response = await callApi(CATALOG_API.gblogin, options);
    if (response && response.token) {
      setToken(response.token);
      return true;
    }
  } catch (error) {
    console.log(error);
    toaster.notify(error.message);
  }
  return false;
}

const setToken = token => {
  store.set('token', token);
};

const getToken = () => {
  store.get('token');
};

const removeToken = () => {
  store.remove('token');
};

// const isTokenExpired = token => {
//   console.log('checking token expiry');
//   const now = Date.now().valueOf() / 1000;
//   console.log(token.exp, now);
//   if (token.exp < now) return true;
//   return false;
// };

export async function currentAccount() {
  const token = getToken();
  console.log('in currentAccount() service');
  console.log(token);
  try {
    const res = await callApi(CATALOG_API.getUser);
    if (res && res.user) return res.user;
  } catch (err) {
    console.error(err);
    toaster.notify(err.message, { duration: null });
    logout();
  }
  return null;
}

export async function logout() {
  removeToken('token');
  toaster.notify('Logged out!');
}

// export async function login(email, password) {
//   console.log(email, password)
//   const requestBody = {
//     email,
//     password
//   }
//   fetch()
//   const promise = new Promise(resolve =>
//     setTimeout(() => {
//       if (email === 'sumixavier@gmail.com' && password === '123abcdQ') {
//         resolve(true)
//       } else resolve(false)
//     }, 2000),
//   )
//   return promise
// }
