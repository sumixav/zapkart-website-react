import toaster from 'toasted-notes'
import 'toasted-notes/src/styles.css'
import fetch from '_utils/fetch';
import { CATALOG_API } from '_constants/api';


export async function getBannersList() {
  try {
    const url = `${CATALOG_API.getBanners}?status=active&feature=active`;

    const res = await fetch(url);
    console.log('HII',res)
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }
}

export async function getBanner (id) {
  try {
    const url = `${CATALOG_API.getBanners}/${id}`;

    const res = await fetch(url);
    console.log('HII',res)
    if (res && res.data) return res.data;
    return null;
  } catch (err) {
    console.log(err);
    toaster.notify(err.message);
    return null;
  }

}
