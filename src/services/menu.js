import { userActions } from 'redux/actions'

export async function getProfileMenuData() {
  return [
    {
      title: 'My profile',
      key: 'profile',
      icon: 'fa fa-user-circle',
      url: '/user/user-profile',
    },
    {
      title: 'Orders',
      key: 'orders',
      url: '/orders',
      icon: 'fa fa-paper-plane',
    },
    {
      title: 'Wishlist',
      key: 'wishlist',
      url: '/wishlist',
      icon: 'fa fa-star',
    },
    {
      title: 'Notification',
      key: 'notification',
      url: '/notifications',
      icon: 'fa fa-exclamation',
    },
    {
      divider: true,
    },
    {
      title: 'Logout',
      key: 'logout',
      action: userActions.LOGOUT,
      icon: 'fa fa-power-off',
    },
  ]
}

export async function getTopMenuData() {
  return []
}

export async function getLeftMenuData() {
  return []
}
