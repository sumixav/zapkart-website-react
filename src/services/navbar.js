/* eslint-disable no-underscore-dangle */
/* eslint-disable import/prefer-default-export */

import { getCategories } from 'services/category';

export async function getNavBarMenuData() {
  // const categories = await getCategories({ fields: ['name', 'slug'], parent:null });
  const categories = await getCategories({ fields: ['name', 'slug'] });
  if (categories && categories.length > 0) {
    return categories.map(i => ({
      title: i.name,
      key: i._id,
      url: `/products/category/${i._id}`,
    }));
  }
  return [];
  // return [
  //   {
  //     title: 'Home',
  //     key: '1',
  //     url:'/home',
  //   },
  //   {
  //     title: 'Prescription',
  //     key: '2',
  //     url: '/prescription',
  //   },
  //   {
  //     title: 'Fertility & Gynecology',
  //     key: '3',
  //     url: '/fertility&gynocology'
  //   },
  //   {
  //     title: 'Ayurvedic & Homeopathy',
  //     key: '4',
  //     url: '/ayurvedic&homeopathy'
  //   },
  //   {
  //     title: 'Medical Devices',
  //     key: '5',
  //     url: '/medical-devices',
  //   },
  //   {
  //     title: 'Non Prescription',
  //     key: '6',
  //     url: '/non-prescription'
  //   }
  // ]
}
