import decode from 'jwt-decode'
import { api } from '_constants'
import toaster from 'toasted-notes'
import 'toasted-notes/src/styles.css'
import axios from 'axios'

export async function forgotPassword(request) {
  
  console.log(request)
  axios.post(api.forgotPassword, request).then(
    response => {
      console.log(response)
      if (response.status === 200) {
        toaster.notify('Email sent!')
        return response
      }
      toaster.notify(`Error: ${response.message}`)
      throw new Error(response)
    },
    error => {
      console.log(error)
      toaster.notify(`Error: ${error.error}`)
      throw new Error(error)
    },
  )
}

export async function login(email, password) {
  console.log('login')
  const loginData = {
    email,
    password,
  }

  return fetch(api.login, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(loginData),
  })
    .then(loginResponse => {
      console.log(loginResponse)
      if (loginResponse.status === 200) return loginResponse.json()
      throw new Error(loginResponse.status)
    })
    .then(resJSON => {
      console.log(resJSON)
      setToken(resJSON.token)
      toaster.notify('You are now logged in!')
      return true
    })
    .catch(error => {
      console.log(error)
      toaster.notify(`Error logging in`)
      return false
    })
}

const setToken = token => {
  localStorage.setItem('token', token)
}

const getToken = () => {
  return localStorage.getItem('token')
}

const removeToken = () => {
  localStorage.removeItem('token')
}

const isTokenExpired = token => {
  console.log(token.exp, Date.now())
  if (token.exp < Date.now() / 1000) return true
  return false
}

export async function currentAccount() {
  const token = getToken()
  console.log('in currentAccount() service')
  console.log(token)
  if (token) {
    if (isTokenExpired(token)) {
      toaster.notify('Session expired! Please sign in again')
      logout()
    }
    const { email, userId, role, phone, name } = decode(token)
    return {
      email,
      id: userId,
      role,
      phoneNo: phone,
      name,
    }
  }
  return null
}

export async function logout() {
  toaster.notify('Logged out!')
  removeToken()
}

// export async function login(email, password) {
//   console.log(email, password)
//   const requestBody = {
//     email,
//     password
//   }
//   fetch()
//   const promise = new Promise(resolve =>
//     setTimeout(() => {
//       if (email === 'sumixavier@gmail.com' && password === '123abcdQ') {
//         resolve(true)
//       } else resolve(false)
//     }, 2000),
//   )
//   return promise
// }
