import { useQuery } from 'react-query';
import qs from 'qs';
import cleanDeep from 'clean-deep';

// import fetch from '_utils/fetch';
import callApi from '_utils/callApi';
import { CATALOG_API } from '_constants/api';

export const useProducts = (options = {}) => {
  console.log(options);
  const url = CATALOG_API.getProducts;
  let queryStr = '?status=active&sortField=priorityOrder&sortOrder=descend';
  Object.entries(options).forEach(([key, val]) => {
    queryStr += `&${key}=${val}`;
  });
  const key = `${url}${queryStr}`;
  console.log('useProducts', options, queryStr);
  return useQuery(key, () => callApi(key));
};

export const useProductsTest = (options = {}, manual = false) => {
  const url = CATALOG_API.getProducts;
  const queryOptions = {
    ...options,
    status: 'active',
    sortField: 'priorityOrder',
    sortOrder: 'descend',
  };
  const queryStr = qs.stringify(cleanDeep(queryOptions));

  const key = `${url}?${queryStr}`;
  console.log('useProducts', options, queryStr);
  return useQuery(key, () => callApi(key), { manual });
};

export const useBrands = (options = {}, manual = false) => {
  console.log(options, manual);
  const optionsQuery = qs.stringify(options);
  const url = `${CATALOG_API.getBrands}?status=active&sort=priorityOrder&fields[]=name&fields[]=slug&${optionsQuery}`;
  return useQuery(url, () => callApi(url), { manual });
};

export const useGetInformation = slug => {
  const url = `${CATALOG_API.getInformations}/${slug}`;
  return useQuery(url, () => callApi(url));
};

export const useSimilarProducts = (organics = []) => {
  let queryStr = '?status=active&sortField=priorityOrder&sortOrder=descend&';
  organics.forEach((val, i) => {
    if (i === organics.length - 1) queryStr += `organic[]=${val}`;
    else queryStr += `organic[]=${val}&`;
    // console.log(queryStr);
  });

  // localhost:3004/api/catalog/v1/products?organic[]=5e44e70a40c1707895934dfa&organic[]=5e46a2f18b3898df11d67d36&status=active&sortField=priorityOrder&sortOrder=ascend
  const url = `${CATALOG_API.getProducts}${queryStr}`;
  return useQuery(url, () => callApi(url));
};

export const useBlogs = () => {
  const url = `${CATALOG_API.getBlogs}?status=active&sort=priorityOrder&feature=active`;
  return useQuery(url, () => callApi(url));
};

export const useCategories = (options = {}, manual = false) => {
  const optionsQuery = qs.stringify(options);
  const url = `${CATALOG_API.getCategories}?status=active&sort[priorityOrder]=1${
    Object.entries(optionsQuery).length > 0 ? `&${optionsQuery}` : ''
  }`;
  return useQuery(url, () => callApi(url), { manual });
};
