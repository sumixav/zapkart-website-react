/* eslint-disable camelcase */
import { all, takeEvery, put, call } from 'redux-saga/effects'
import toaster from 'toasted-notes'
import { STRINGS } from '_constants'
import { login, register, currentAccount, logout, fblogin, gblogin } from 'services/user'
import { userActions } from './actions'
import { menuActions } from '../actions'

export function* REGISTER({ payload }) {
  console.log(payload)
  yield put({
    type: userActions.SET_STATE,
    payload: {
      loading: true,
    },
  })
  const success = yield call(register, payload)

  console.log(success)
  if (success) {
    console.log('registered')
    toaster.notify(STRINGS.register_success)
    yield put({
      type: userActions.SET_STATE,
      payload: {
        registerSuccess: true,
        registerError: false,
      },
    })
  } else {
    console.log('registration fail')
    yield put({
      type: userActions.SET_STATE,
      payload: {
        registerError: true,
        registerSuccess: false,
      },
    })
  }
  yield put({
    type: userActions.SET_STATE,
    payload: {
      loading: false,
    },
  })
}

export function* GBLOGIN({ payload }) {
  console.log('in login redux sagatttttttttttt')
  const { email, id, name } = payload
  console.log(email, id, name)
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isLogging: true,
    },
  })
  const success = yield call(gblogin, email, id, name)
  console.log(success)
  if (success) {
    console.log('logged intttttttttttttt')
    yield put({
      type: userActions.SET_STATE,
      payload: {
        isLogged: true,
        loginError: false,
        isLogging: false,
        loading: false,
      },
    })
    yield put({
      type: userActions.LOAD_CURRENT_ACCOUNT,
    })
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loginError: true,
        isLogging: false,
      },
    })
  }
}

export function* FBLOGIN({ payload }) {
  console.log('in login redux sagatttttttttttt')
  const { email, id, name } = payload
  console.log(email, id, name)
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isLogging: true,
    },
  })
  const success = yield call(fblogin, email, id, name)
  console.log(success)
  if (success) {
    console.log('logged intttttttttttttt')
    yield put({
      type: userActions.SET_STATE,
      payload: {
        isLogged: true,
        loginError: false,
        isLogging: false,
        loading: false,
      },
    })
    yield put({
      type: userActions.LOAD_CURRENT_ACCOUNT,
    })
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loginError: true,
        isLogging: false,
      },
    })
  }
}

export function* LOGIN({ payload }) {
  console.log('in login redux saga')
  const { email, password } = payload
  console.log(email, password)
  yield put({
    type: userActions.SET_STATE,
    payload: {
      isLogging: true,
    },
  })
  const success = yield call(login, email, password)
  console.log(success)
  if (success) {
    console.log('logged in')
    yield put({
      type: userActions.SET_STATE,
      payload: {
        isLogged: true,
        loginError: false,
        isLogging: false,
        loading: false,
      },
    })
    yield put({
      type: userActions.LOAD_CURRENT_ACCOUNT,
    })
  } else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loginError: true,
        isLogging: false,
      },
    })
  }
}

export function* LOAD_CURRENT_ACCOUNT() {
  console.log('loading current account')
  yield put({
    type: userActions.SET_STATE,
    payload: {
      loading: true,
    },
  })
  const response = yield call(currentAccount)
  console.log('response LOAD_CURRENT_ACCOUNT', response)
  if (response && response.id) {
    // const { email, id, role, phoneNo, name } = response
    const {
      email,
      user_type,
      firstName,
      id,
      lastName,
      phone
    } = response
    yield put({
      type: userActions.SET_STATE,
      payload: {
        email,
        role:user_type.name,
        firstName,
        id,
        lastName,
        phoneNo:phone,
        authorized: true,
        isLogged: true,
        loading: false,
        loginError: false,
      },
    })
    yield put({
      type: menuActions.GET_DATA,
    })
  }
  else {
    yield put({
      type: userActions.SET_STATE,
      payload: {
        loading: false,
        // email:'',
        // role:'',
        // firstName:'',
        // userId:'',
        // lastName:'',
        // phoneNo:'',
        authorized: false,
        isLogged: false,
        // loginError: response.message,
      },
    })
  }
}

export function* LOGOUT() {
  yield call(logout)
  yield put({
    type: userActions.SET_STATE,
    payload: {
      id: '',
      name: '',
      role: '',
      email: '',
      avatar: '',
      authorized: false,
      loading: false,
      isLogging: false,
      isLogged: false,
      loginError: false,
    },
  })
}

export default function* rootSaga() {
  yield all([
    takeEvery(userActions.LOGIN, LOGIN),
    takeEvery(userActions.REGISTER, REGISTER),
    takeEvery(userActions.LOAD_CURRENT_ACCOUNT, LOAD_CURRENT_ACCOUNT),
    takeEvery(userActions.LOGOUT, LOGOUT),
    takeEvery(userActions.FBLOGIN, FBLOGIN),
    takeEvery(userActions.GBLOGIN, GBLOGIN),
    // takeEvery(userActions.LOGOUT, LOGOUT),
    LOAD_CURRENT_ACCOUNT(), // run once on app load to check user auth
  ])
}
