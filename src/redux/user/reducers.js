import { userActions } from './actions';

const initialState = {
  id: '',
  name: '',
  role: '',
  email: '',
  avatar: '',
  authorized: false,
  isLogging: false,
  isLogged: false,
  loginError: false,
  loading: false,
  registerSuccess: false,
  registerError: false,
};

export const getUser = state => state.user;

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case userActions.SET_STATE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
