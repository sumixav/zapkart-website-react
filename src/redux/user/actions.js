/* eslint-disable import/prefer-default-export */
export const userActions = {
    REGISTER: 'user/REGISTER',
    LOGIN: 'user/LOGIN',
    LOGOUT: 'user/LOGOUT',
    SET_STATE: 'user/SET_STATE',
    LOAD_CURRENT_ACCOUNT: 'user/LOAD_CURRENT_ACCOUNT',
    FBLOGIN: 'user/FbLOGIN',
    GBLOGIN: 'user/GbLOGIN',
}
