import { forgotPasswordActions as actions } from './actions'

const initialState = {
  success: false,
  error: false,
  isSubmitting: false,
}

export default function forgotPasswordReducer(state = initialState, action) {
  switch (action.type) {
    case actions.FORGOT_PASSWORD_SUCCESS:
      return { ...state, success: action.payload }
    case actions.FORGOT_PASSWORD_ERROR:
      return { ...state, error: action.payload }
    case actions.SET_STATE:
      return { ...state, ...action.payload }

    default:
      return state
  }
}
