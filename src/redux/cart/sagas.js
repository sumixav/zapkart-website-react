import { all, takeEvery, put, select } from 'redux-saga/effects';
import toaster from 'toasted-notes';
import { getUser } from 'redux/user/reducers';
import store from 'store';
import { cartActions } from './actions';
// deal qith qty

export function* ADD_TO_CART({ payload }) {
  const user = yield select(getUser);
  console.log(user)
  console.log('ADD_TO_CART', payload);
  const { productId, qty, listPrice, salePrice, title, description, img, thumbImg } = payload;
  let prevProds = [];
  const prevProdsStore = store.get(`cart.products`);
  let newProd = { id: productId, qty, listPrice, salePrice, title, description, img, thumbImg };
  console.log('newProd', newProd);
  if (typeof prevProdsStore !== 'undefined') {
    prevProds = prevProdsStore;
    const existingProductIndex = prevProds.findIndex(a => a.id === productId);
    console.log('existingProductIndex', existingProductIndex);
    if (existingProductIndex !== -1) {
      prevProds[existingProductIndex].qty += qty;
      newProd = null;
    }
  }
  const toUpdate = newProd ? [...prevProds, { ...newProd }] : [...prevProds];
  yield store.set('cart.products', toUpdate);
  yield put({
    type: cartActions.SET_CART,
    payload: { products: toUpdate },
  });
}

export function* REMOVE_FROM_CART({ payload }) {
  console.log(payload);
  const { productId } = payload;
  const prevProdsStore = store.get(`cart.products`);
  let filteredProds = [];
  if (typeof prevProdsStore !== 'undefined') {
    filteredProds = prevProdsStore.filter(i => i.id !== productId);
  }
  yield store.set('cart.products', [...filteredProds]);

  yield put({
    type: cartActions.SET_CART,
    payload: { products: [...filteredProds] },
  });
}

export function* INC_QTY({ payload }) {
  console.log(payload);
  const { maxOrderQty, productId } = payload;
  const prods = store.get(`cart.products`);
  const index = prods.findIndex(a => a.id === productId);
  if (index !== -1) {
    const { qty } = prods[index];
    if (qty < maxOrderQty) {
      prods[index].qty += 1;
      yield store.set('cart.products', prods);
      yield put({
        type: cartActions.SET_CART,
        payload: { products: prods },
      });
    } else toaster.notify('Max limit reached');
  }
}

export function* DEC_QTY({ payload }) {
  console.log(payload);
  const { minOrderQty, productId } = payload;
  const prods = store.get(`cart.products`);
  const index = prods.findIndex(a => a.id === productId);
  if (index !== -1) {
    const { qty } = prods[index];
    if (qty > minOrderQty) {
      prods[index].qty -= 1;
      yield store.set('cart.products', prods);
      yield put({
        type: cartActions.SET_CART,
        payload: { products: prods },
      });
    }
  }
}

export default function* rootSaga() {
  yield all([
    takeEvery(cartActions.ADD_TO_CART, ADD_TO_CART),
    takeEvery(cartActions.REMOVE_FROM_CART, REMOVE_FROM_CART),
    takeEvery(cartActions.INC_QTY, INC_QTY),
    takeEvery(cartActions.DEC_QTY, DEC_QTY),
    // SETUP(),
  ]);
}
