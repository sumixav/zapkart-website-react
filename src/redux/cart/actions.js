/* eslint-disable import/prefer-default-export */
export const cartActions = {
  GET_CART: 'cart/GET_CART',
  SET_CART: 'cart/SET_CART',
  ADD_NEW_ITEM: 'cart/ADD_NEW_ITEM',
  REMOVE_ITEM: 'cart/REMOVE_ITEM',
  ADD_TO_CART: 'cart/ADD_TO_CART',
  REMOVE_FROM_CART: 'cart/REMOVE_FROM_CART',
  INC_QTY: 'cart/INC_QTY',
  DEC_QTY: 'cart/DEC_QTY',
};
