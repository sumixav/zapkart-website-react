/* eslint-disable react/button-has-type */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';

const ButtonTypes = {
  SAVE: 'save-btn',
  ADD_TO_CART: 'addto-cart-btn',
};

export const Button = ({ className, title, onClick, children, loading, isSubmit }) => {
  // console.log(loading, typeof loading)
  return (
    <button
    
      disabled={loading}
      type={isSubmit ? 'submit' : 'button'}
      className={className}
      onClick={onClick}
    >
      <span>{title}</span>
      {children}
      {loading && (
        <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true" />
      )}
    </button>
  );
};

export const SaveButton = props => {
  const { title } = props;
  return <Button {...props} className={ButtonTypes.SAVE} title={title} />;
};

export const SubmitButton = props => {
  const { isSubmit } = props;
  return <Button {...props} className={ButtonTypes.SAVE} loading={isSubmit} />;
};

export const AddButton = props => {
  const { isAdding } = props;
  return <Button {...props} className={ButtonTypes.ADD_TO_CART} loading={isAdding} />;
};

Button.propTypes = {
  // type: PropTypes.string,
  loading: PropTypes.bool,
  title: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  // type: 'addto-cart-btn',
  loading: false,
  title: '',
};

export default Button;
