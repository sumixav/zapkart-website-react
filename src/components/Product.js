import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { connect } from 'react-redux';
import Skeleton from 'react-loading-skeleton';
import PropTypes from 'prop-types';

const linkStyle = { color: '#333' };

const Product = props => {
  console.log(props);
  const {
    title,
    // description,

    // thumbImg,
    listPrice,
    salePrice,
    price,
    item,
    slug,
    loading,
    deadLink,
  } = props;
  let { frequent, img = '' } = props;
  if (img && !img.startsWith('/')) img = `/${img}`;
  frequent = Boolean(frequent);
  console.log('in  product', img);
  console.log(frequent);
  // const loadingClass = Boolean(title)
  //  <div className={`product-listing ${frequent ? 'frequently-brought-product-list' : ''}`}>

  /**
   *
   * @param {nodes} children
   * @param {boolean} isDeadLink
   * @param {string} toLink
   */
  const getLinked = children => {
    if (deadLink) return children;
    return (
      <Link to={`/product/${slug}`} style={linkStyle}>
        {children}
      </Link>
    );
  };

  // eslint-disable-next-line no-unused-vars

  const LoadingProduct = (
    <div className="product-listing">
      <div className="product-image lazy">
        <Skeleton width={120} height={109} />{' '}
      </div>
      <h5 className="product-name">
        <Skeleton width={150} />
      </h5>
      <h6 className="product-discription">
        <Skeleton width={140} />
      </h6>

      <div>
        <h3>
          <Skeleton width={50} />
        </h3>
      </div>
      {/* <div type="button" style={{ color: 'transparent' }} className="small-btn">
        Buy
      </div> */}
    </div>
  );

  const DefaultProduct = (
    <div
      className={classNames({
        'product-listing': true,
        'frequently-brought-product-list': frequent,
        // 'loading': loadingClass
      })}
    >
      {getLinked(
        <>
          {img ? <img src={img} alt="" className="product-image lazy" /> : <Skeleton />}
          <h5 className="product-name">{title || <Skeleton />}</h5>
          {/* <h6 className="product-discription">{frequent ? '' : description || <Skeleton />}</h6> */}
        </>,
      )}

      {/* <Link to={`/product/${slug}`} style={linkStyle}>
        {img ? <img src={`/${img}`} alt="" className="product-image lazy" /> : <Skeleton />}
        <h5 className="product-name">{title || <Skeleton />}</h5>
        <h6 className="product-discription">{frequent ? '' : description || <Skeleton />}</h6>
      </Link> */}
      {listPrice && salePrice ? (
        <div className="price-area">
          <h3 className="original-price">
            <i className="fas fa-rupee-sign" /> {listPrice || <Skeleton />}
          </h3>
          <h3 className="offer-price">
            <i className="fas fa-rupee-sign" /> {salePrice || <Skeleton />}
          </h3>
        </div>
      ) : (
        <>
          <h3>
            {(salePrice || listPrice || price) && <i className="fas fa-rupee-sign" />}
            {salePrice || listPrice || price || <Skeleton />}
          </h3>

          <div className="order-btn">
            {title &&
              getLinked(
                <>
                  <button type="button" className="small-btn">
                    Add to cart
                  </button>
                </>,
              )}
            {/* // <button type="button" onClick={handleBuyProduct} className="small-btn">
            //   Buy
            // </button> */}

            {/* {title && !deadLink && (
              <Link to={`/product/${slug}`} style={linkStyle}>
                <button type="button" className="small-btn">
                  Add to cart
                </button>
              </Link>
            )} */}
          </div>
        </>
      )}
    </div>
  );
  if (item) return <div className="item">{DefaultProduct}</div>;
  if (loading) return LoadingProduct;
  return DefaultProduct;
};

Product.propTypes = {
  deadLink: PropTypes.bool,
};

Product.defaultProps = {
  deadLink: false,
};

export default connect(({ user, cart }) => ({ user, cart }))(Product);
