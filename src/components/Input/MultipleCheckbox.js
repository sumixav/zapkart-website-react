import React from 'react';

class MultipleCheckbox extends React.PureComponent {
  state = {
    selectedValues: [],
  };

  componentDidUpdate(prevProps, prevState) {
    const { onChange } = this.props;
    const { selectedValues: prevSelectedValues } = prevState;
    const { selectedValues: currentSelectedValues } = this.state;
    if (prevSelectedValues !== currentSelectedValues && onChange) onChange(currentSelectedValues);
  }

  handleChange = e => {
    console.log('clicked');
    const { value } = e.target;
    this.setState(prev => {
      if (prev.selectedValues.includes(value))
        return { ...prev, selectedValues: prev.selectedValues.filter(i => i !== value) };
      return { ...prev, selectedValues: [...prev.selectedValues, value] };
    });
  };

  render() {
    const { options } = this.props;
    return options.map(i => {
      console.log(i.checked);
      return (
        <div key={i.value} className="checkbox">
          <input
            type="checkbox"
            name={i.label}
            // onClick={onChange}
            onChange={this.handleChange}
            value={i.value}
          />
          {/* <div className="form-label">{i.label}</div> */}
          <label htmlFor={i.label} className="form-label">
            <span>{i.label}</span>
          </label>
        </div>
      );
    });
  }
}

// const RadioGroup = ({ options = null, onChange }) => {
//   const [selectedValues, setSelectedValues] = useState([]);
//   useEffect(() => {
//     onChange(selectedValues);
//   }, [selectedValues, onChange]);

//   console.log(options);

//   const handleChange = e => {
//     const { value } = e.target;
//     setSelectedValues(prev => {
//       if (prev.includes(value)) return prev.filter(i => i !== value);
//       return [...prev, value];
//     });
//   };

//   return (
//     <div>
//       {options &&
//         options.map(i => {
//           console.log(i.checked);
//           return (
//             <div key={i.value}>
//               <input
//                 type="checkbox"
//                 name={i.label}
//                 // onClick={onChange}
//                 onChange={handleChange}
//                 value={i.value}
//               />
//               <div className="form-label">{i.label}</div>
//             </div>
//           );
//         })}
//     </div>
//   );
// };

export default MultipleCheckbox;
