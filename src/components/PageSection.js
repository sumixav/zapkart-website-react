import React from 'react'

const PageSection = ({ children }) => {
  return <section className="inner-page-wrapper">{children}</section>
}

export default PageSection
