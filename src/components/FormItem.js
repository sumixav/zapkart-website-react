import React from 'react'

const FormItem = props => {

  const { children, className, label } = props
  return <div className={`form-group ${className || ''}`}><span className="form-label">{label}</span>{children}</div>
}

export default FormItem
