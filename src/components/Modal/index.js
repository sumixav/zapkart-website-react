/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import classNames from 'classnames';
import ModalHead from './ModalHead';
import ModalBody from './ModalBody';
import ModalFooter from './ModalFooter';
import './styles.css';

class Modal extends Component {
  static Head = ModalHead;

  static Body = ModalBody;

  static Footer = ModalFooter;

  render() {
    const { show, onClose, children } = this.props;
    console.log('in Modal', show);
    return (
      <div
        className={classNames({
          'modal modal-show': show,
          'modal modal-none': !show,
        })}
        // ROLE WAS dialog
        role="presentation"
        onClick={onClose}
      >
        <div
          className={classNames({
            'modal-dialog': true
          })}
          role="document"
        >
          <div className="modal-content">{children}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
