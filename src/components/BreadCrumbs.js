/* eslint-disable react/require-default-props */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class BreadCrumbs extends Component {
  state = {
    // breadcrumbs: [],
  };

  render() {
    const { bread } = this.props;
    const breadFiltered = bread.filter((item, index, array) => index < array.length - 1);
    const breads = breadFiltered.map(item => {
      // console.log(item.url)
      return (
        <li key={item.url}>
          <Link key={item.url} to={`${item.url}`}>
            {item.text}
          </Link>{' '}
          <span key={`${item.url}1`}>/</span>
        </li>
      );
    });
    // console.log(bread)
    if (breads.length > 0)
      return (
        <div className="breadcrumbs-wrapper">
          <ul>
            {breads}
            <li>{bread[bread.length - 1].text}</li>
          </ul>
        </div>
      );
    return null;
  }
}

BreadCrumbs.propTypes = {
  bread: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      url: PropTypes.string,
    }),
  ),
};

export default BreadCrumbs;
