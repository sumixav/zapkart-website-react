import React from 'react';
import PropTypes from 'prop-types'

const Toaster = ({ text, icon, children, onClose }) => {
  const wrapperStyle = { transform: 'translateY(0%) scale(1)' };
  return (
    <div className="Toaster__message-wrapper" style={wrapperStyle}>
      <div data-reach-alert="true">
        <div id="5" className="Toaster__alert">
          {icon && <div className="toaster-icon">{icon}</div>}
          {text && <div className="Toaster__alert_text">{text}</div>}
          {children}
          <button onClick={onClose} className="Toaster__alert_close" type="button" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
      </div>
    </div>
  );
};

Toaster.propTypes = {
  onClose:PropTypes.func.isRequired
}

export default Toaster;
